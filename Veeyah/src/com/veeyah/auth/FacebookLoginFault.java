
package com.veeyah.auth;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FacebookLoginFault" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
public class FacebookLoginFault {

    protected String facebookLoginFault;

    /**
     * Gets the value of the facebookLoginFault property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFacebookLoginFault() {
        return facebookLoginFault;
    }

    /**
     * Sets the value of the facebookLoginFault property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacebookLoginFault(String value) {
        this.facebookLoginFault = value;
    }

}
