
package com.veeyah.auth;


/**
 * <p>Java class for VeeyahSetting complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VeeyahSetting">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SettingName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SettingValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
public class VeeyahSetting {

    protected String settingName;
    protected String settingValue;

    /**
     * Gets the value of the settingName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSettingName() {
        return settingName;
    }

    /**
     * Sets the value of the settingName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSettingName(String value) {
        this.settingName = value;
    }

    /**
     * Gets the value of the settingValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSettingValue() {
        return settingValue;
    }

    /**
     * Sets the value of the settingValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSettingValue(String value) {
        this.settingValue = value;
    }

}
