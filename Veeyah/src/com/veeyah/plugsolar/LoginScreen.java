package com.veeyah.plugsolar;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.veeyah.matchbox.GetSunJoulesResponse;
import com.veeyah.plugsolar.manager.DeviceMetricsManager;
import com.veeyah.plugsolar.manager.GetSunJoulesManager;
import com.veeyah.plugsolar.manager.RegisterDeviceManager;
import com.veeyah.plugsolar.manager.VeeyahGUI;
import com.veeyah.plugsolar.manager.WebServiceManager;
import com.veeyah.auth.RegisterDeviceResponse;
import com.veeyah.auth.RegisterType;

/**
 * @purpose This class manages a login view that requires the user to enter an email address.
 */

@SuppressLint("NewApi")
public class LoginScreen extends Activity implements VeeyahGUI {
    private final String TAG = "[ Veeyah LoginScreen "+VeeyahConstants.VERSION_NUMBER+"]";

    public MatchBoxModel mbm;
    LoginScreen self;
    private boolean acceptedTerms = false;
    protected RelativeLayout termsContainer = null;

	private String email;
    private ProgressDialog progressDialog = null;
    private CheckBox termsBox = null;
	LinearLayout loginLinearLayout;

    public void refreshView(Object data) {
        if (data == null) {
            VeeyahLogger.w(TAG, "Null data Received!");
            return;
        }

        try {
            if (GetSunJoulesResponse.class.equals(data.getClass())) {
                final Intent startMainActivity = new Intent(LoginScreen.this,
                        Home_View.class);
                if (progressDialog != null) progressDialog.dismiss();
                
                // Animate the solar panel to enter over the sun 
                ImageView celestialObject = (ImageView) findViewById(R.id.daynight);
                ImageView solarPanelImageView = (ImageView) findViewById(R.id.solarPanelImageView);
                float xStart = (float) (celestialObject.getLeft()*5.0);

                ObjectAnimator translationXAnimator = ObjectAnimator.ofFloat(solarPanelImageView, "translationX", xStart, 0f);
        		ObjectAnimator translationYAnimator = ObjectAnimator.ofFloat(solarPanelImageView, "translationY", 50f, 0f);
        		translationXAnimator.setDuration(5000);
        		translationYAnimator.setDuration(5000);
        		
        		final AnimatorSet animatorSet = new AnimatorSet();
        		animatorSet.playTogether(translationXAnimator, translationYAnimator);
        		animatorSet.addListener(new AnimatorListenerAdapter() {
        			@Override
        			public void onAnimationEnd(Animator animation) {
        			    super.onAnimationEnd(animation);
        			    startActivity(startMainActivity);
                        finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        			}
        		});
        		if (animatorSet != null) {
        			animatorSet.start();
        		}
        		solarPanelImageView.setVisibility(View.VISIBLE);
				
            } else if (RegisterDeviceResponse.class.equals(data.getClass())) {
                GetSunJoulesManager getSJ = new GetSunJoulesManager();
                getSJ.getSunJoules(mbm.getDeviceID(), 50, new WebServiceManager(), this, false);
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception redirecting to Home_View: "+e.getClass()+": "+e.getMessage());
        }

    }

    public void showError(String msg) {
        Toast.makeText(ViaSolar.getContext(),
                "Cannot reach the server.\nPlease check your data connection and try again.", Toast.LENGTH_LONG)
                .show();
    }

    private void toggleTermsCheckbox() {
        try {
            if (mbm == null) mbm = new MatchBoxModel();
            mbm.incMetric(DeviceMetricsManager.CHK_TERMS);
            if (termsBox != null) {
                if (acceptedTerms) {
                    termsBox.setChecked(false);
                    acceptedTerms = false;
                } else {
                    termsBox.setChecked(true);
                    acceptedTerms = true;
                }
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception toggling terms checkbox: " + e.getClass() + ": " + e.getMessage());
        }
    }

	// public static Context APPLICATION_CONTEXT;
	protected void onCreate(Bundle savedInstanceState) {	
		try {

            self = this;
            super.onCreate(savedInstanceState);
            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.activity_login_screen);
            ImageView solarPanelImageView = (ImageView) findViewById(R.id.solarPanelImageView);
            solarPanelImageView.setVisibility(View.GONE);

            termsBox = (CheckBox) findViewById(R.id.termsCheckbox);
            termsBox.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleTermsCheckbox();
                }
            });
            TextView termsLabel = (TextView) findViewById(R.id.termsCheckboxLabel);
            termsLabel.setText(Html.fromHtml("I agree to the <a href=\"https://veeyah.com/terms.jsp\">terms of use</a>."));
            termsLabel.setMovementMethod(LinkMovementMethod.getInstance());
            termsLabel.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleTermsCheckbox();
                }
            });
            termsContainer = (RelativeLayout) findViewById(R.id.termsContainer);

            mbm = new MatchBoxModel();
            mbm.incMetric(DeviceMetricsManager.SCR_LOGIN);
            String deviceID = mbm.getDeviceID();
            loginLinearLayout = (LinearLayout) findViewById(R.id.scv);

            if (!"".equals(deviceID) && !VeeyahConstants.DEFAULT_DEVICE_ID.equals(deviceID) && deviceID != null) {
                Intent startMainActivity = new Intent(LoginScreen.this, Home_View.class);
                startActivity(startMainActivity);
                finish();
                return;
            }

            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle(getString(R.string.app_name));
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);

            final EditText mailText = (EditText) findViewById(R.id.mailTextView);
            mailText.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View v, int keyCode, KeyEvent event)
                {
                    if (event.getAction() == KeyEvent.ACTION_DOWN)
                    {
                        switch (keyCode)
                        {
                            case KeyEvent.KEYCODE_DPAD_CENTER:
                            case KeyEvent.KEYCODE_ENTER:
                                loginButtonProcess(mailText);
                                return true;
                            default:
                                break;
                        }
                    }
                    return false;
                }
            });
            Button loginBtn = (Button) findViewById(R.id.loginBtn);
            loginBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    loginButtonProcess(mailText);
                }
            });
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception onCreate: " + e.getClass() + ": " + e.getMessage());
        }
	}

	private void loginButtonProcess(EditText mailText) {
        try {
            mbm.incMetric(DeviceMetricsManager.BTN_REGISTER);
            Editable text = mailText.getText();
            if (text != null && text.length() > 0) {
                if (!acceptedTerms) {
                    Toast.makeText(
                            ViaSolar.getContext(),
                            "Please accept the terms of use.",
                            Toast.LENGTH_LONG).show();
                    if (termsContainer != null) {
                        termsContainer.setBackgroundResource(R.drawable.red_border);
                    }
                    return;
                }
                
                if (android.util.Patterns.EMAIL_ADDRESS.matcher(text).matches()) {
                    // Check for invalid email characters (whitespace, etc)
                	progressDialog.show();
                    RegisterDeviceManager reg = new RegisterDeviceManager();
                    RegisterType deviceInfo = new RegisterType();
                    TelephonyManager tm = (TelephonyManager) ViaSolar.getContext().getSystemService(TELEPHONY_SERVICE);
                    deviceInfo.setName("");
                    deviceInfo.setEmail(text.toString());
                    String guid = tm.getDeviceId();
                    if (guid == null) guid = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                    if (guid == null) guid = "Null GUID";
                    deviceInfo.setClientGUID(guid);
                    deviceInfo.setDeviceName(android.os.Build.DEVICE);
                    deviceInfo.setDeviceType(android.os.Build.MODEL);
                    deviceInfo.setPassword("");
                    deviceInfo.setInviteCode("0KAwbgfzG00CNk6XX02qejsf");
                    deviceInfo.setBatteryCapacity(new BigInteger("" + mbm.getCapacity()));
                    reg.registerDevice(deviceInfo, new WebServiceManager(), self);
                    Toast.makeText(
                            ViaSolar.getContext(),
                            "Registering your device. . .",
                            Toast.LENGTH_LONG).show();
                } else {
                	Toast.makeText(
                            ViaSolar.getContext(),
                            "Please enter email & tap the Go button to continue",
                            Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(
                        ViaSolar.getContext(),
                        "Please enter email & tap the Go button to continue",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception registering device onClick of Go: " + e.getClass() + ": " + e.getMessage());
            Toast.makeText(
                    ViaSolar.getContext(),
                    "Sorry, there was a problem registering your device.  Please contact customersupport@veeyah.com",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onConfigurationChanged (Configuration newConfig) {
        //do nothing
        super.onConfigurationChanged(newConfig);
    }

	protected void onStart() {
		super.onStart();
	}

	protected void onStop() {
		super.onStop();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
