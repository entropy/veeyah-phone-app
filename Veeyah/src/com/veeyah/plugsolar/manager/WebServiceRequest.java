package com.veeyah.plugsolar.manager;

public class WebServiceRequest {

    /**
     * @purpose This class is a data model for server communication.
     *
     */

    private String functionID;
    private String serviceURI;
    private String msgBody;
    private WebServiceReceiver callback;

    public WebServiceRequest(String functionID, String serviceURI, String msgBody, WebServiceReceiver callback) {
        this.functionID = functionID;
        this.msgBody = msgBody;
        this.callback = callback;
        this.serviceURI = serviceURI;
    }

    public String getMessage() {
        return(msgBody);
    }

    public String getFunctionID() {
        return(functionID);
    }

    public WebServiceReceiver getReceiver() {
        return(callback);
    }

    public String getServiceURI() {
        return(serviceURI);
    }
}
