package com.veeyah.plugsolar.manager;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.veeyah.plugsolar.VeeyahConstants;
import com.veeyah.plugsolar.VeeyahLogger;
import com.veeyah.plugsolar.ViaSolar;
import com.veeyah.plugsolar.ViaSolarException;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * @purpose This class manages webservice connections with the server.
 * It has capabilities to optionally queue requests and cache results to compensate for poor network connectivity.
 *
 */

public class WebServiceManager {
    private final static String TAG = "[Veeyah WebServiceManager "+ VeeyahConstants.VERSION_NUMBER+"]";
    private AsyncTask currentTask;
    private WebServiceRequest currentRequest;
    private LinkedList<WebServiceRequest> queue;
    private HashMap<String, String> cache;

    public static String DISCONNECTED = "Not connected to data network.";

    public WebServiceManager() {
        currentTask = null;
        currentRequest = null;
        queue = new LinkedList<WebServiceRequest>();
        cache = new HashMap<String, String>();
    }

    public void makeWebServiceRequest(WebServiceRequest request, boolean useCache, boolean useQueue) {
        try {
            if (!isConnectingToInternet()) {
                request.getReceiver().onServiceFault(DISCONNECTED);
                return;
            }
            if (checkTaskAvailability()) {
                beginWebServiceRequest(request);
            } else {
                if (useCache) {
                    if (cache.containsKey(request.getFunctionID())) {
                        request.getReceiver().onServiceComplete(cache.get(request.getFunctionID()));
                        return;
                    }
                }
                if (useQueue) {
                    queue.add(request);
                }
            }
        } catch (Exception e) {
            VeeyahLogger.i(TAG, "Exception making webservice request: " + e.getClass() + ": " + e.getMessage());
        }

    }

    private void beginWebServiceRequest(WebServiceRequest req) {
        try {
            currentRequest = req;
            currentTask = new WebServiceTask().execute(currentRequest.getMessage());
        } catch (Exception e) {
            VeeyahLogger.i(TAG, "Exception beginning webservice request: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void checkNextRequest() {
        try {
            WebServiceRequest nextReq = queue.poll();
            if (nextReq != null) {
                beginWebServiceRequest(nextReq);
            } else {
                currentRequest = null;
            }
        } catch (Exception e) {
            VeeyahLogger.i(TAG, "Exception checking next webservice request: "+e.getClass()+": "+e.getMessage());
        }
    }

        class WebServiceTask extends AsyncTask<String, Integer, String> {

            protected void onProgressUpdate(Integer... progress) {
                // setProgressPercent(progress[0]);
                //TODO: add progress feedback to WebServiceReceiver interface
            }

            protected void onPostExecute(String result) {
                cache.put(currentRequest.getFunctionID(), result);
                WebServiceReceiver lastFxn = currentRequest.getReceiver();
                checkNextRequest();
                lastFxn.onServiceComplete(result);
            }

            protected String doInBackground(String... i) {
                return (doWebService(i[0].getBytes()));
            }
        }

        private String doWebService(byte[] postData) {
            try {
                URL url = new URL(currentRequest.getServiceURI()+"/");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Length",
                        Integer.toString(postData.length));
                conn.setRequestProperty("Content-Type", "text/xml");
                conn.setRequestProperty("SOAPAction",
                        "\""+currentRequest.getServiceURI()+"\"");
                conn.setUseCaches(false);
                conn.setDoOutput(true);

                OutputStream out = conn.getOutputStream();
                out.write(postData);
                out.close();
                try {
                    InputStream in = new BufferedInputStream(conn.getInputStream());
                    StringBuilder total = new StringBuilder();
                    byte[] bytes = new byte[1000];
                    int numRead;
                    while ((numRead = in.read(bytes)) >= 0) {
                        total.append(new String(bytes, 0, numRead));
                    }
                    return (total.toString());
                } finally {
                    conn.disconnect();
                }
            } catch (Exception e) {
                String errStr = "Exception contacting server for '"+currentRequest.getFunctionID()+"' at '"+currentRequest.getServiceURI()+"/': " + e.getClass() + ": " + e.getMessage();
                Log.w(TAG, errStr);
                handleError(errStr);
                return ("Exception contacting server for '"+currentRequest.getFunctionID()+"' at '"+currentRequest.getServiceURI()+"/': " + e.getClass() + ": " + e.getMessage());
            }
        }

    private void handleError(String error) {
        WebServiceReceiver lastFxn = currentRequest.getReceiver();
        checkNextRequest();
        lastFxn.onServiceFault(error);
    }

    private boolean checkTaskAvailability() {
        if (currentTask == null)
            return (true);
        if (currentTask.getStatus().equals(AsyncTask.Status.FINISHED))
            return (true);
        VeeyahLogger.i(TAG, "Asynch task is too busy.");
        return (false);
    }

    public static boolean isConnectingToInternet(){
        try {
            ConnectivityManager cm = (ConnectivityManager) ViaSolar.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo == null) throw new ViaSolarException("Network info object is null, phone probably offline.");
            return netInfo.isConnectedOrConnecting();
        } catch (Exception e) {
            VeeyahLogger.i(TAG, "Exception checking network connectivity: "+e.getClass()+": "+e.getMessage());
        }
        return false;
    }

    public static String getCurrentTimeStamp() {
        try {
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date now = new Date();
            return sdfDate.format(now);
        } catch (Exception e) {
            VeeyahLogger.i(TAG, "Exception getting current timestamp: "+e.getClass()+": "+e.getMessage());
        }
        return(System.currentTimeMillis()+"");
    }

    public static List<String> parseForElt(String elt, String body) {
        ArrayList<String> out = new ArrayList<String>();
        try {
            if (body != null && elt != null) {
                int len = elt.length();
                int sdex = body.indexOf("<"+elt+">");
                while (sdex >= 0) {
                    int edex = body.indexOf("</"+elt+">", sdex+len+2);
                    if (edex >= 0) out.add(body.substring(sdex+len+2, edex));
                    sdex = body.indexOf("<"+elt+">", edex+len+2);
                }
            }
        } catch (Exception e) {
            VeeyahLogger.i(TAG, "Exception parsing for '"+elt+"': "+e.getClass()+": "+e.getMessage());
        }
        return(out);
    }

    public static String parseForSingleElt(String elt, String body) {
        String out = "";
        try {
            if (body != null && elt != null) {
                int len = elt.length();
                int sdex = body.indexOf("<"+elt+">");
                if (sdex >= 0) {
                    int edex = body.indexOf("</"+elt+">", sdex+len+2);
                    if (edex >= 0) out = body.substring(sdex+len+2, edex);
                }
            }
        } catch (Exception e) {
            VeeyahLogger.i(TAG, "Exception parsing for '"+elt+"': "+e.getClass()+": "+e.getMessage());
        }
        return(out);
    }

}
