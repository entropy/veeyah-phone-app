package com.veeyah.plugsolar.manager;

import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.xmlpull.v1.XmlSerializer;

import android.util.Log;
import android.util.Xml;

import com.veeyah.plugsolar.MatchBoxModel;
import com.veeyah.plugsolar.SunPort;
import com.veeyah.plugsolar.VeeyahConstants;
import com.veeyah.plugsolar.VeeyahLogger;

/**
 * @purpose This class manages a call to the server to report energy and power used by connected SunPorts.
 * It depends on WebServiceManager and callsback to a VeeyahGUI.
 * Note that the response to this request can include SunJoules to add to a connected SunPort.
 *
 */

public class UpdateSunPortManager implements WebServiceReceiver {

    final static String UPDATE_SUNPORT_REQUEST = "UpdateSunPort";
    private VeeyahGUI callback;

    public UpdateSunPortManager(VeeyahGUI callback) {
        this.callback = callback;
    }

    public void sendSunPortUpdate(WebServiceManager wsm, SunPort sp) {
        try {
            XmlSerializer serializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            try {
                MatchBoxModel mbm = new MatchBoxModel();
                SimpleDateFormat sdf = new SimpleDateFormat(VeeyahConstants.DEFAULT_DATE_FORMAT);

                serializer.setOutput(writer);
                serializer.startDocument("UTF-8", true);
                serializer.startTag("", "soapenv:Envelope");
                serializer.attribute("", "xmlns:soapenv",
                        "http://schemas.xmlsoap.org/soap/envelope/");
                serializer.attribute("", "xmlns:vr",
                        "http://veeyah.com/VeeyahReport");

                serializer.startTag("", "soapenv:Header");
                serializer.endTag("", "soapenv:Header");

                serializer.startTag("", "soapenv:Body");
                serializer.startTag("", "vr:UpdateSunPort");
                serializer.startTag("", "RequestTime");
                serializer.text(getTime());
                serializer.endTag("", "RequestTime");
                serializer.startTag("", "DeviceID");
                serializer.text(mbm.getDeviceID());
                serializer.endTag("", "DeviceID");
                serializer.startTag("", "PortMacAddress");
                serializer.text(sp.getAddress());
                serializer.endTag("", "PortMacAddress");
                if (sp.getEnergyTime() > 0 && sp.getEnergy() > 0) {
                    serializer.startTag("", "EnergyData");
                    serializer.startTag("", "dateTime");
                    Date energyDate = new Date(sp.getEnergyTime());
                    String eDateStr = sdf.format(energyDate);
                    serializer.text(eDateStr);
                    serializer.endTag("", "dateTime");
                    serializer.startTag("", "EnergySJ");
                    serializer.text(sp.getEnergy()+"");
                    serializer.endTag("", "EnergySJ");
                    serializer.endTag("", "EnergyData");
                }
                if (sp.getPowerTime() > 0 && sp.getPower() > 0) {
                    serializer.startTag("", "PowerData");
                    serializer.startTag("", "dateTime");
                    Date powerDate = new Date(sp.getPowerTime());
                    String pDateStr = sdf.format(powerDate);
                    serializer.text(pDateStr);
                    serializer.endTag("", "dateTime");
                    serializer.startTag("", "PowerSJ");
                    serializer.text(sp.getPower()+"");
                    serializer.endTag("", "PowerSJ");
                    serializer.endTag("", "PowerData");
                }
                serializer.endTag("", "vr:UpdateSunPort");
                serializer.endTag("", "soapenv:Body");
                serializer.endTag("", "soapenv:Envelope");
                serializer.endDocument();
                String msg = writer.toString();
                Log.i("Request", msg);
                WebServiceRequest wsr = new WebServiceRequest(UPDATE_SUNPORT_REQUEST, VeeyahConstants.WEBSERVICE_DOMAIN+"/VeeyahReport", msg, new ErrorReportManager());
                wsm.makeWebServiceRequest(wsr, true, true);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } catch (Exception e) {
            VeeyahLogger.w("Veeyah UpdateSunPort", "Exception sending UpdateSunPort: " + e.getClass()+ ": " + e.getMessage());
            callback.showError(sp.getAddress());
        }
    }

    private static String getTime() {
        java.util.Date date = new java.util.Date();
        return ((new Timestamp(date.getTime())).toString());
    }

    public void onServiceComplete(String msg) {
        String spMac = "";
        try {
            spMac = WebServiceManager.parseForSingleElt("PortMacAddress", msg);
            SunPort toUpdate = new SunPort(spMac);
            String fillAmt = WebServiceManager.parseForSingleElt("FillAmount", msg);
            toUpdate.setPendingFill(Integer.parseInt(fillAmt));
            callback.refreshView(toUpdate);
        } catch (Exception e) {
            VeeyahLogger.w("Veeyah UpdateSunPort", "Exception parsing result: "+e.getClass()+": "+e.getMessage());
            callback.showError(spMac);
        }
    }

    public void onServiceFault(String msg) {
        VeeyahLogger.e("Veeyah UpdateSunPortManager", "Fault updating SunPort.  The message is: " + msg);
        callback.showError(msg);
    }

}
