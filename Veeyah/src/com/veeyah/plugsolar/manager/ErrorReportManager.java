package com.veeyah.plugsolar.manager;

import java.io.StringWriter;
import java.sql.Timestamp;

import org.xmlpull.v1.XmlSerializer;

import android.util.Log;
import android.util.Xml;

import com.veeyah.plugsolar.MatchBoxModel;
import com.veeyah.plugsolar.VeeyahConstants;

/**
 * @purpose This class manages a call to the server to report arbitrary errors.
 * It depends on WebServiceManager and has no callback.
 *
 */

public class ErrorReportManager implements WebServiceReceiver {

    final static String ERROR_REPORT_REQUEST = "ErrorReport";

    public static void sendErrorReport(WebServiceManager wsm, Throwable error, String location) {
        try {
            MatchBoxModel mbm = new MatchBoxModel();
            String deviceID = mbm.getDeviceID();
            if (deviceID == null || "".equals(deviceID)) {
                deviceID = VeeyahConstants.DEFAULT_DEVICE_ID;
            }
            XmlSerializer serializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            try {

                serializer.setOutput(writer);
                serializer.startDocument("UTF-8", true);
                serializer.startTag("", "soapenv:Envelope");
                serializer.attribute("", "xmlns:soapenv",
                        "http://schemas.xmlsoap.org/soap/envelope/");
                serializer.attribute("", "xmlns:vr",
                        "http://veeyah.com/VeeyahReport");

                serializer.startTag("", "soapenv:Header");
                serializer.endTag("", "soapenv:Header");

                serializer.startTag("", "soapenv:Body");
                serializer.startTag("", "vr:ErrorReport");
                serializer.startTag("", "RequestTime");
                serializer.text(getTime());
                serializer.endTag("", "RequestTime");
                serializer.startTag("", "DeviceID");
                serializer.text(deviceID);
                serializer.endTag("", "DeviceID");
                serializer.startTag("", "ErrorClass");
                serializer.text(error.getClass().toString());
                serializer.endTag("", "ErrorClass");
                serializer.startTag("", "ErrorLocation");
                serializer.text(location);
                serializer.endTag("", "ErrorLocation");
                serializer.startTag("", "ErrorMessage");
                serializer.text(error.getMessage());
                serializer.endTag("", "ErrorMessage");
                serializer.endTag("", "vr:ErrorReport");
                serializer.endTag("", "soapenv:Body");
                serializer.endTag("", "soapenv:Envelope");
                serializer.endDocument();
                String msg = writer.toString();
                Log.i("Request", msg);
                WebServiceRequest wsr = new WebServiceRequest(ERROR_REPORT_REQUEST, VeeyahConstants.WEBSERVICE_DOMAIN+"/VeeyahReport", msg, new ErrorReportManager());
                wsm.makeWebServiceRequest(wsr, true, true);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } catch (Exception e) {
            Log.i("Veeyah ErrorReportingManager", "Exception sending ErrorReport: " + e.getClass()+ ": " + e.getMessage());
        }
    }

    private static String getTime() {
        java.util.Date date = new java.util.Date();
        return ((new Timestamp(date.getTime())).toString());
    }

    public void onServiceComplete(String msg) {
        //do nothing
    }

    public void onServiceFault(String msg) {
        Log.e("Veeyah ErrorReportManager", "Fault reporting error.  The message is: "+msg);
    }

}
