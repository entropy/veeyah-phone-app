package com.veeyah.plugsolar.manager;

import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.xmlpull.v1.XmlSerializer;

import android.util.Log;
import android.util.Xml;

import com.veeyah.plugsolar.MatchBoxModel;
import com.veeyah.plugsolar.SunPort;
import com.veeyah.plugsolar.VeeyahConstants;
import com.veeyah.plugsolar.VeeyahLogger;
import com.veeyah.utility.SunPortEnergyData;

/**
 * @purpose This class manages a call to the server to retrieve historical energy data about a SunPort.
 * It depends on WebServiceManager and callsback to a VeeyahGUI.
 *
 */

public class SunPortEnergyDataManager implements WebServiceReceiver {

    final static String GET_SUNPORT_ENERGY_DATA_REQUEST = "GetSunPortEnergyData";
    private VeeyahGUI callback;

    public SunPortEnergyDataManager(VeeyahGUI callback) {
        this.callback = callback;
    }

    public void getSunPortEnergyData(WebServiceManager wsm, SunPort sp, Date startTime, Date endTime) {
        try {
            XmlSerializer serializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            try {
                MatchBoxModel mbm = new MatchBoxModel();
                SimpleDateFormat sdf = new SimpleDateFormat(VeeyahConstants.DEFAULT_DATE_FORMAT);
                String startStr = sdf.format(startTime);
                String endStr = sdf.format(endTime);

                serializer.setOutput(writer);
                serializer.startDocument("UTF-8", true);
                serializer.startTag("", "soapenv:Envelope");
                serializer.attribute("", "xmlns:soapenv",
                        "http://schemas.xmlsoap.org/soap/envelope/");
                serializer.attribute("", "xmlns:vr",
                        "http://veeyah.com/VeeyahReport");

                serializer.startTag("", "soapenv:Header");
                serializer.endTag("", "soapenv:Header");

                serializer.startTag("", "soapenv:Body");
                serializer.startTag("", "vr:GetSunPortEnergyData");
                serializer.startTag("", "RequestTime");
                serializer.text(getTime());
                serializer.endTag("", "RequestTime");
                serializer.startTag("", "DeviceID");
                serializer.text(mbm.getDeviceID());
                serializer.endTag("", "DeviceID");
                serializer.startTag("", "PortMacAddress");
                serializer.text(sp.getAddress());
                serializer.endTag("", "PortMacAddress");
                serializer.startTag("", "StartTime");
                serializer.text(startStr);
                serializer.endTag("", "StartTime");
                serializer.startTag("", "EndTime");
                serializer.text(endStr);
                serializer.endTag("", "EndTime");
                serializer.endTag("", "vr:UpdateSunPort");
                serializer.endTag("", "soapenv:Body");
                serializer.endTag("", "soapenv:Envelope");
                serializer.endDocument();
                String msg = writer.toString();
                Log.i("Request", msg);
                WebServiceRequest wsr = new WebServiceRequest(GET_SUNPORT_ENERGY_DATA_REQUEST, VeeyahConstants.WEBSERVICE_DOMAIN+"/VeeyahReport", msg, new ErrorReportManager());
                wsm.makeWebServiceRequest(wsr, true, true);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } catch (Exception e) {
            VeeyahLogger.w("Veeyah SunPortEnergyDataManager", "Exception sending UpdateSunPort: " + e.getClass()+ ": " + e.getMessage());
            callback.showError(sp.getAddress());
        }
    }

    private static String getTime() {
        java.util.Date date = new java.util.Date();
        return ((new Timestamp(date.getTime())).toString());
    }

    public void onServiceComplete(String msg) {
        String spMac = "";
        try {
            ArrayList<SunPortEnergyData> spEnergyData = new ArrayList<SunPortEnergyData>();
            SimpleDateFormat sdf = new SimpleDateFormat(VeeyahConstants.DEFAULT_DATE_FORMAT);
            Iterator<String> eIter = WebServiceManager.parseForElt("EnergyData", msg).iterator();
            while (eIter.hasNext()) {
                String nextElt = eIter.next();
                try {
                    SunPortEnergyData energyPoint = new SunPortEnergyData();
                    String dateStr = WebServiceManager.parseForSingleElt("dateTime", nextElt);
                    energyPoint.setDateTime(sdf.parse(dateStr));
                    String energyPercent = WebServiceManager.parseForSingleElt("EnergyPercent", nextElt);
                    if (energyPercent != null && !"".equals(energyPercent))
                        energyPoint.setEnergyPercent(Integer.parseInt(energyPercent));
                    String energySJ = WebServiceManager.parseForSingleElt("EnergySJ", nextElt);
                    if (energySJ != null && !"".equals(energySJ))
                        energyPoint.setEnergySJ(Integer.parseInt(energySJ));
                    spEnergyData.add(energyPoint);
                } catch (Exception e) {
                    VeeyahLogger.w("Veeyah SunPortEnergyDataManager", "Exception parsing single SunPort energy point: "+e.getClass()+": "+e.getMessage());
                }
            }
            callback.refreshView(spEnergyData);
        } catch (Exception e) {
            VeeyahLogger.w("Veeyah SunPortEnergyDataManager", "Exception parsing SunPort energy data: "+e.getClass()+": "+e.getMessage());
            callback.showError(spMac);
        }
    }

    public void onServiceFault(String msg) {
        VeeyahLogger.e("Veeyah SunPortEnergyDataManager", "Fault getting SunPort energy data.  The message is: " + msg);
        callback.showError(msg);
    }

}
