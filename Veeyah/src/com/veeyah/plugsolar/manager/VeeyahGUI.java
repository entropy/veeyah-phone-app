package com.veeyah.plugsolar.manager;

/**
 * @purpose This interface allows classes that manage connections with the server to callback to other classes.
 */

public interface VeeyahGUI {
    public void refreshView(Object data);
    public void showError(String msg);
}
