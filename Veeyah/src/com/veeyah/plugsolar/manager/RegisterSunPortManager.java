package com.veeyah.plugsolar.manager;

import java.io.StringWriter;
import org.xmlpull.v1.XmlSerializer;
import android.util.Xml;

import com.veeyah.plugsolar.MatchBoxModel;
import com.veeyah.plugsolar.SunPort;
import com.veeyah.plugsolar.VeeyahConstants;
import com.veeyah.plugsolar.VeeyahLogger;
import com.veeyah.plugsolar.ViaSolarException;

/**
 * @purpose This class manages a call to the server to register a SunPort.
 * It depends on WebServiceManager and callsback to a VeeyahGUI.
 *
 */

public class RegisterSunPortManager implements WebServiceReceiver {

    final static String REGISTER_SUNPORT_REQUEST = "RegisterSunPort";
    private VeeyahGUI callback;
    private String spMAC;
    private SunPort sp = null;

    public RegisterSunPortManager(SunPort sp) {
        this.sp = sp;
    }

    public void registerSunPort(String spMAC, WebServiceManager wsm, VeeyahGUI callback) {
        this.callback = callback;
        this.spMAC = spMAC;
        updateDevice(wsm);
    }

    private void updateDevice(WebServiceManager wsm) {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            if (serializer == null) throw new ViaSolarException("Serializer is null");
            if (writer == null) throw new ViaSolarException("Writer is null");
            MatchBoxModel mbm = new MatchBoxModel();
            serializer.setOutput(writer);

            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "soapenv:Envelope");
            serializer.attribute("", "xmlns:soapenv",
                    "http://schemas.xmlsoap.org/soap/envelope/");
            serializer.attribute("", "xmlns:veey",
                    "http://veeyah.com/VeeyahAuth");
            serializer.startTag("", "soapenv:Header");
            serializer.endTag("", "soapenv:Header");
            serializer.startTag("", "soapenv:Body");
            serializer.startTag("", "veey:RegisterSunPort");
            serializer.startTag("", "RequestTime");
            serializer.text(WebServiceManager.getCurrentTimeStamp());
            serializer.endTag("", "RequestTime");
            serializer.startTag("", "DeviceID");
            serializer.text(mbm.getDeviceID());
            serializer.endTag("", "DeviceID");
            serializer.startTag("", "PortMacAddress");
            serializer.text(spMAC);
            serializer.endTag("", "PortMacAddress");
            serializer.endTag("", "veey:RegisterSunPort");
            serializer.endTag("", "soapenv:Body");
            serializer.endTag("", "soapenv:Envelope");
            serializer.endDocument();
            String msg = writer.toString();
            if (msg == null) throw new ViaSolarException("msg is null");
            VeeyahLogger.i("Request", msg);
            WebServiceRequest wsr = new WebServiceRequest(REGISTER_SUNPORT_REQUEST, VeeyahConstants.WEBSERVICE_DOMAIN+"/VeeyahAuth", msg, this);
            if (wsm == null) throw new ViaSolarException("wsm is null");
            wsm.makeWebServiceRequest(wsr, true, true);
        } catch (Exception e) {
            VeeyahLogger.w("Veeyah RegisterDeviceManager", "Exception registering device: "+e.getClass()+": "+e.getMessage());
            callback.showError("Sorry, your device could not be registered: "+e.getMessage());
        }
    }

    public void onServiceComplete(String msg) {

        try {
            String temp = WebServiceManager.parseForSingleElt("Registered", msg);
            String deviceID = WebServiceManager.parseForSingleElt("DeviceID", msg);
            if (temp.contains("t")) {
                if (sp != null) {
                    sp.setDeviceID(deviceID);
                    callback.refreshView(sp);
                } else {
                    callback.refreshView("SunPort '" + spMAC + "' is now registered to you as '"+deviceID+"'.");
                }
                MatchBoxModel mbm = new MatchBoxModel();
                mbm.addSunPort(new SunPort(spMAC));
            } else {
                VeeyahLogger.i("Veeyah RegisterSunPorteResponse Parser", "SunPort already registered!");
                callback.showError("This SunPort is already registered!");
            }
        } catch (Exception e) {
            VeeyahLogger.w("Veeyah RegisterSunPorteResponse Parser", "Failed to parse registration: "+e.getClass()+": "+e.getMessage());
            callback.showError("Could not parse response from the Veeyah server.");
            return;
        }
    }

    public void onServiceFault(String msg) {
        callback.showError("Could not register SunPort with the Veeyah server.");
    }

}

