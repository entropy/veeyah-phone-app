package com.veeyah.plugsolar.manager;

import java.io.StringWriter;
import java.net.URL;

import org.xmlpull.v1.XmlSerializer;
import android.util.Xml;
import com.veeyah.plugsolar.VeeyahConstants;
import com.veeyah.plugsolar.VeeyahLogger;

/**
 * @purpose This class manages a call to the server to get the URL for a video to watch.
 * It depends on WebServiceManager and callsback to a VeeyahGUI.
 *
 */

public class GetVideoURLManager implements WebServiceReceiver {

    final String TAG = "[Veeyah GetVideoURLManager "+VeeyahConstants.VERSION_NUMBER+"]";
    final static String GET_VIDEO_URL_REQUEST = "GetVideoURL";
    private VeeyahGUI callback;

    public void getVideoURL(String deviceID, WebServiceManager wsm, VeeyahGUI callback) {
        this.callback = callback;
        requestSunJoules(deviceID, wsm);
    }

    private void requestSunJoules(String deviceID, WebServiceManager wsm) {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            serializer.setOutput(writer);
            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "soapenv:Envelope");
            serializer.attribute("", "xmlns:soapenv",
                    "http://schemas.xmlsoap.org/soap/envelope/");
            serializer.attribute("", "xmlns:mat",
                    "http://veeyah.com/MatchBox");

            serializer.startTag("", "soapenv:Header");
            serializer.endTag("", "soapenv:Header");

            serializer.startTag("", "soapenv:Body");
            serializer.startTag("", "mat:GetVideoURL");
            serializer.startTag("", "RequestTime");
            serializer.text(WebServiceManager.getCurrentTimeStamp());
            serializer.endTag("", "RequestTime");
            serializer.startTag("", "DeviceID");
            serializer.text(deviceID);
            serializer.endTag("", "DeviceID");

            serializer.endTag("", "mat:GetVideoURL");
            serializer.endTag("", "soapenv:Body");
            serializer.endTag("", "soapenv:Envelope");
            serializer.endDocument();
            String msg = writer.toString();
            VeeyahLogger.i("Request", msg);
            WebServiceRequest wsr = new WebServiceRequest(GET_VIDEO_URL_REQUEST, VeeyahConstants.WEBSERVICE_DOMAIN+"/MatchBox", msg, this);
            wsm.makeWebServiceRequest(wsr, false, true);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception getting video URL: "+e.getClass()+": "+e.getMessage());
        }
    }

    public void onServiceComplete(String msg) {
        try {
            callback.refreshView(new URL(WebServiceManager.parseForSingleElt("URL", msg)));
        } catch (Exception e) {
            VeeyahLogger.i(TAG, "Response message: "+msg);
            VeeyahLogger.w(TAG, "Failed to parse video URL: "+e.getClass()+": "+e.getMessage());
            callback.showError("Could not parse response from the Veeyah server.");
            return;
        }
    }

    public void onServiceFault(String msg) {
        callback.showError("Could not get video URL from the Veeyah server.");
    }

}