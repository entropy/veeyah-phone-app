package com.veeyah.plugsolar.manager;

public interface WebServiceReceiver {

    /**
     * @purpose This interface allows WebServiceManager to pass the result of a server communication to an arbitrary manager.
     *
     */

    public void onServiceComplete(String msg);
    public void onServiceFault(String msg);

}
