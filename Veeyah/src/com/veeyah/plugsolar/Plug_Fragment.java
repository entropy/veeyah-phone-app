package com.veeyah.plugsolar;

import java.util.Random;
import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.veeyah.plugsolar.manager.DeviceMetricsManager;
import com.veeyah.plugsolar.manager.VideoFileManager;
import com.veeyah.utility.Astro;

/**
 * @purpose This class manages the fragment with information about connected SunPorts.
 */

@SuppressLint("NewApi")
public class Plug_Fragment extends android.support.v4.app.Fragment {
    private final static String TAG = "[ Veeyah Plug_Fragment "+VeeyahConstants.VERSION_NUMBER+"]";

    private long lastRingPosition = 0;
    protected AnimateRingTask animator = null;
    private boolean animationInProgress = false;
    private Handler frame = new Handler();
    View rootView = null;
    RelativeLayout backgroundLayout;
    ImageView sunJouleRing;
    ImageView sunJouleIcon;
    ImageView centerStatus;
    ImageView centerSuccess;
    TextView lastUpdateView;
    TextView powerView;
    static Bitmap bmp2 = null;
    Bitmap bmOverlay = null;
    static Canvas c;
    static Paint p;
    static RectF rectF;
    Button sunCubeButton;
    public boolean playNextSound = false;
    public SunPort currentPort;
    public int currentPortPos = 0;
    private Home_View home = null;
    public Handler handler = new Handler();

    AnimatorSet animatorSet = null;
    public static float sunImagePlus = 0.10f;

    @Override
    public void onDestroy() {
        VeeyahLogger.i(TAG, "onDestroy called");
        Home_View.unbindDrawables(rootView);
        super.onDestroy();
        VeeyahLogger.i(TAG, "onDestroy finished");
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        VeeyahLogger.i(TAG, "onCreateView called");
        //super.onCreateView(inflater, container, savedInstanceState);
        try {
            rootView = inflater.inflate(R.layout.plug_screen, container, false);

            home = (Home_View) getActivity();
            //if (rootView == null) return null;

            // Hide the ActionBar
            ActionBar actionBar = home.getActionBar();
            if (actionBar != null) actionBar.hide();

            //lastRingPosition = mbm.getLastRingPos();
            //lastRingPosition = 0;

            if (rootView == null) throw new ViaSolarException("rootView is null!");
            backgroundLayout = (RelativeLayout) rootView.findViewById(R.id.scv);
            sunJouleRing = (ImageView) rootView.findViewById(R.id.sunJouleCircle);
            sunJouleIcon = (ImageView) rootView.findViewById(R.id.sunJouleIcon);
            centerStatus = (ImageView) rootView.findViewById(R.id.centerstatus);
            centerSuccess = (ImageView) rootView.findViewById(R.id.centersuccess);
            lastUpdateView = (TextView) rootView.findViewById(R.id.lastUpdateView);
            powerView = (TextView) rootView.findViewById(R.id.powerView);
            updateNewsFeed();

            // Init and set the listener for the new ticker text view to open the browser
            sunCubeButton = (Button) rootView.findViewById(R.id.sunCubeButton);

            sunCubeButton.setTextColor(Color.LTGRAY);
            sunCubeButton.setTextSize(16);

            // Configure footer buttons
            ImageButton shareImageButton = (ImageButton) rootView.findViewById(R.id.shareImageButton);
            //ImageView detailsImageView = (ImageView) rootView.findViewById(R.id.usageDetailsIconButton);
            ImageButton homeImageButton = (ImageButton) rootView.findViewById(R.id.homeImageButton);
            ImageButton phoneImageButton = (ImageButton) rootView.findViewById(R.id.phoneImageButton);
            ImageButton settingsImageButton = (ImageButton) rootView.findViewById(R.id.settingsImageButton);
            ImageButton nextPlugButton = (ImageButton) rootView.findViewById(R.id.nextPlugButton);

            shareImageButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.share();
                }
            });
            /*detailsImageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.showUsageDetails();
                }
            });*/
            homeImageButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.selectNavDrawerItem(Home_View.NAV_DRAWER_ITEM_HOME);
                }
            });
            phoneImageButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.selectNavDrawerItem(Home_View.NAV_DRAWER_ITEM_PHONE);
                }
            });
            settingsImageButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.selectNavDrawerItem(Home_View.NAV_DRAWER_ITEM_SETTINGS);
                }
            });
            nextPlugButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        int nextPos = currentPortPos+1;
                        if (home.currentSunPorts != null) {
                            VeeyahLogger.i(TAG, "current port: "+currentPortPos+"; nextPos is "+nextPos);
                            if (home.currentSunPorts.length <= nextPos) {
                                VeeyahLogger.i(TAG, "wrap sunport array");
                                nextPos = 0;
                            }
                            VeeyahLogger.i(TAG, "start switching sunports to "+nextPos+" of "+home.currentSunPorts.length);
                            if (home.currentSunPorts[nextPos] != null) {
                                final int nextPosFinal = nextPos;
                                home.handler.post(new Runnable() {
                                    public void run() {
                                        currentPortPos = nextPosFinal;
                                        currentPort = home.currentSunPorts[currentPortPos];
                                        VeeyahLogger.i(TAG, "Current port set to "+currentPortPos);
                                        refreshGUI();
                                    }
                                });
                            } else {
                                VeeyahLogger.i(TAG, "Next port is null.");
                            }
                        } else {
                            VeeyahLogger.i(TAG, "home sunport array is null");
                        }
                        VeeyahLogger.i(TAG, "finish switching sunports");
                    } catch (Exception e) {
                        VeeyahLogger.i(TAG, "Exception switching ports: "+e.getClass()+": "+e.getMessage());
                    }
                }
            });

            // Set help tip longClickListeners
            home.setHelpLongClickListener(shareImageButton, "Share this app!");
            //home.setHelpLongClickListener(detailsImageView, "View more specific details.");
            home.setHelpLongClickListener(homeImageButton, "Return to the home screen.");
            home.setHelpLongClickListener(phoneImageButton, "View your phone's solar energy.");
            home.setHelpLongClickListener(settingsImageButton, "View the app settings.");
            home.setHelpLongClickListener(nextPlugButton, "Check the next plug.");

            // Configure Sun Joule Ring to open dialog for more sun joules
            Button ringButton = (Button) rootView.findViewById(R.id.mainSunJouleButton);
            ringButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    moreSolar();
                }
            });
            home.setHelpLongClickListener(ringButton, "Do you want to get more solar energy?");

            ImageButton sunJouleIconButton = (ImageButton) rootView.findViewById(R.id.sunJouleIconCircle);
            sunJouleIconButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    moreSolar();
                }
            });
            home.setHelpLongClickListener(sunJouleIconButton, "Do you want to get more solar energy?");

            // Configure help overlay button
            ImageButton helpImageButton = (ImageButton) rootView.findViewById(R.id.helpImageButton);
            helpImageButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    showHelpOverlay();
                }
            });

            // Configure menu button
            ImageButton menuImageButton = (ImageButton) rootView.findViewById(R.id.menuImageButton);
            menuImageButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Home_View.mDrawerLayout.isDrawerOpen(Home_View.mDrawerList)) {
                        Home_View.mDrawerLayout.closeDrawer(Home_View.mDrawerList);
                    } else {
                        Home_View.mDrawerLayout.openDrawer(Home_View.mDrawerList);
                    }
                }
            });

            //setBackgroundImage();
            VeeyahLogger.i(TAG, "onCreateView finished");
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception loading Plug_Fragment view: "+e.getClass()+": "+e.getMessage());
        }
        return rootView;
    }

   /* @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player,
                                        boolean wasRestored) {
        super.onInitializationSuccess(provider, player, wasRestored);
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return super.getYouTubePlayerProvider();
    }*/

    void animateRingView() {
        try {
            if (sunJouleRing == null) {
                VeeyahLogger.i(TAG, "SunJouleRing is null for animation!");
                return;
            }
            // Animate the ring
            VeeyahLogger.i(TAG, "Animate ring called");
            updateCenterTimer();
            long fill = currentPort.getEnergy();
            updateCenterSunJouleIcon(fill);

            if (fill == 0) {
                updateSunJouleRing(0);
                updateCenterTimer();
                return;
            }
            //VeeyahLogger.i(TAG, "Animating the SunJoule ring to value " + fill + " sj");

            if (animator != null) {
                animator.halt();
                frame = new Handler();
                updateSunJouleRing(lastRingPosition);
                animator = new AnimateRingTask(fill, playNextSound);
                frame.removeCallbacks(animator);
                frame.postDelayed(animator, 33);
            } else if (animationInProgress) {
                VeeyahLogger.w(TAG, "Animation in progress, but animator is null!");
            } else {
                updateSunJouleRing(lastRingPosition);
                animator = new AnimateRingTask(fill, playNextSound);
                frame.removeCallbacks(animator);
                frame.postDelayed(animator, 33);
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception in anitmateRingview: "+e.getClass()+": "+e.getMessage());
        }
    }

    void setBackgroundImage() {
        // Contextual background images
        try {
            // Get the screen dimensions for animations
            Display display = ((WindowManager) home.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;

            // Occasionally animate a satellite
            final ImageView satellite = (ImageView) rootView.findViewById(R.id.satelliteImageView);
            float satelliteXStart = width;
            float satelliteXEnd = -100;
            float satelliteYStart = height/4;
            float satelliteYEnd = height*3/4;
            ObjectAnimator satelliteTranslationXAnimator = ObjectAnimator.ofFloat(satellite, "translationX", satelliteXStart, satelliteXEnd);
            ObjectAnimator satelliteTranslationYAnimator = ObjectAnimator.ofFloat(satellite, "translationY", satelliteYStart, satelliteYEnd*3/4, satelliteYEnd);

            final AnimatorSet satelliteAnimatorSet = new AnimatorSet();
            if (satelliteAnimatorSet != null) {
                satelliteTranslationXAnimator.setDuration(15000);
                satelliteTranslationYAnimator.setDuration(15000);
                satelliteTranslationXAnimator.addListener(new AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        satellite.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        satellite.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        satellite.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                        satellite.setVisibility(View.VISIBLE);
                    }
                });
                satelliteAnimatorSet.playTogether(satelliteTranslationXAnimator, satelliteTranslationYAnimator);
                Random r = new Random();
                int i = r.nextInt(3);
                if (i == 1) {
                    satelliteAnimatorSet.start();
                }
            }

            if (Astro.isNight()) {
                backgroundLayout.setBackgroundResource(R.drawable.sky_bg_04);

                // Stop any animation on the footer for night image
                if (animatorSet != null) { animatorSet.cancel(); }

                // Occasionally animate a meteor
                final ImageView meteor = (ImageView) rootView.findViewById(R.id.meteorImageView);
                float meteorXStart = width;
                float meteorXEnd = -100;
                float meteorYStart = height/8;
                float meteorYEnd = height/2;
                ObjectAnimator meteorTranslationXAnimator = ObjectAnimator.ofFloat(meteor, "translationX", meteorXStart, meteorXEnd);
                ObjectAnimator meteorTranslationYAnimator = ObjectAnimator.ofFloat(meteor, "translationY", meteorYStart, meteorYEnd*3/4, meteorYEnd);

                final AnimatorSet meteorAnimatorSet = new AnimatorSet();
                if (meteorAnimatorSet != null) {
                    meteorTranslationXAnimator.setDuration(4000);
                    meteorTranslationYAnimator.setDuration(4000);
                    meteorTranslationXAnimator.addListener(new AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            meteor.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            meteor.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            meteor.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                            meteor.setVisibility(View.VISIBLE);
                        }
                    });
                    meteorAnimatorSet.playTogether(meteorTranslationXAnimator, meteorTranslationYAnimator);
                    Random r = new Random();
                    int i = r.nextInt(3);
                    if (i == 1) {
                        meteorAnimatorSet.start();
                    }
                }

            } else {  //is day
                backgroundLayout.setBackgroundResource(R.drawable.sky_bg);
            }

        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception setting background image: "+e.getClass()+": "+e.getMessage());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Intent refreshPlug = new Intent();
        refreshPlug.setClass(ViaSolar.getContext(), VeeyahBattery.class);
        refreshPlug.setAction(VeeyahBattery.REFRESH);
        refreshPlug.putExtra(VeeyahBattery.CURRENT_SUNPORT, currentPort);
        home.startService(refreshPlug);
        //setBackgroundImage();
        //VeeyahLogger.i(TAG, "onResume Plug_Fragment animation");
        //animateRingView();
    }

    protected void configPlug() {
        // Dialog to add more solar energy
        if (currentPort == null) return;
        LayoutInflater inflater = LayoutInflater.from(home);
        View alertDialogView = inflater.inflate(R.layout.plug_config, null);
        try {
            if (alertDialogView == null) {
                throw new ViaSolarException("The alertDialogView is null!");
            }

            final String plugName = currentPort.getAddress();
            String nickName = home.mbm.checkPlugName(plugName);
            if (nickName == null || "".equals(nickName)) nickName = plugName;

            TextView contextTextView = (TextView) alertDialogView.findViewById(R.id.dialogContextTextView);
            String context = "<br>" +
                    "You are connected to a SunPort called '"+nickName+"'<br/>";
            contextTextView.setText(Html.fromHtml(context));

            final Dialog dialog = new Dialog(home);
            dialog.setContentView(alertDialogView);
            dialog.setTitle("Rename plug");
            dialog.setCancelable(true);

            final EditText newName = (EditText) dialog.findViewById(R.id.plugNameText);

            Button renamePlug = (Button) dialog.findViewById(R.id.renamePlug);
            renamePlug.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Editable text = newName.getText();
                    if (text != null && text.length() > 0) {
                        home.mbm.storeName(plugName, text.toString());
                    }
                    dialog.dismiss();
                }
            });
            Button cancel = (Button) dialog.findViewById(R.id.renameCancel);
            cancel.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exeption getting moreSolar in Plug_Fragment: "+e.getClass()+": "+e.getMessage());
        }
    }

    protected void moreSolar() {
        //TODO: update server, callback to Home_View

        // Dialog to add more solar energy
        LayoutInflater inflater = LayoutInflater.from(home);
        View alertDialogView = inflater.inflate(R.layout.dialog_custom, null);
        try {
            if (alertDialogView == null) {
                throw new ViaSolarException("The alertDialogView is null!");
            }

            TextView contextTextView = (TextView) alertDialogView.findViewById(R.id.dialogContextTextView);
            String context = "<br>" +
                    "Update your SunPort to 50 kWh...<br>";
            contextTextView.setText(Html.fromHtml(context));

            final Dialog dialog = new Dialog(home);
            dialog.setContentView(alertDialogView);
            dialog.setTitle("Get More SunJoules");
            dialog.setCancelable(true);

            Button yesButton = (Button) dialog.findViewById(R.id.getMoreSolarYes);
            yesButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent refillPlug = new Intent();
                        refillPlug.setClass(ViaSolar.getContext(), VeeyahBattery.class);
                        refillPlug.setAction(VeeyahBattery.FILL_BLE);
                        refillPlug.putExtra(VeeyahBattery.CURRENT_SUNPORT, currentPort);
                        home.startService(refillPlug);
                        dialog.dismiss();
                        home.showSunportRefillTimer();
                    } catch (Exception e) {
                        VeeyahLogger.w(TAG, "Exception requesting sunport refill: "+e.getClass()+": "+e.getMessage());
                    }
                }
            });
            Button muteButton = (Button) dialog.findViewById(R.id.getMoreSolarMute);
            muteButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent refillPlug = new Intent();
                        refillPlug.setClass(ViaSolar.getContext(), VeeyahBattery.class);
                        refillPlug.setAction(VeeyahBattery.FILL_BLE);
                        refillPlug.putExtra(VeeyahBattery.CURRENT_SUNPORT, currentPort);
                        home.startService(refillPlug);
                        dialog.dismiss();
                        home.showSunportRefillTimer();
                    } catch (Exception e) {
                        VeeyahLogger.w(TAG, "Exception requesting sunport refill: "+e.getClass()+": "+e.getMessage());
                    }
                }
            });
            Button noButton = (Button) dialog.findViewById(R.id.getMoreSolarNo);
            noButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exeption getting moreSolar in Plug_Fragment: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void getSolarVideo(boolean mute, Dialog dialog) {
        try {
            VideoFileManager vfm = new VideoFileManager();
            Intent intent = new Intent();
            intent.setClass(ViaSolar.getContext(),
                    VideoPlayer.class);
            String vidURL = vfm.watchUnwatchedVideo();
            if (vidURL.equals(vfm.NO_VIDEO)) vidURL = vfm.watchAnyVideo();
            if (vidURL.equals(vfm.NO_VIDEO)) vidURL = "android.resource://" + home.getPackageName() + "/" + R.raw.defaultvid;
            intent.setData(Uri.parse(vidURL));
            intent.putExtra(VideoPlayer.SUNPORT_LABEL, true);
            if (mute) {
                AudioManager am = (AudioManager) ViaSolar.getContext().getSystemService(Context.AUDIO_SERVICE);
                int curVol = am.getStreamVolume(AudioManager.STREAM_MUSIC);
                intent.putExtra(VideoPlayer.MUTE_LABEL, curVol);
            }

            dialog.dismiss();
            startActivity(intent);
            home.finish();
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception in Plug_Fragment.getSolarVideo: "+e.getClass()+": "+e.getMessage());
        }
    }

    public void refreshGUI() {
        try {
            VeeyahLogger.i(TAG, "Animate GUI based on refresh message.");
            animateRingView();
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception refreshing GUI: "+e.getClass()+": "+e.getMessage());
        }

    }

    protected void updateNewsFeed() {
        try {
            if (sunCubeButton == null) return;
            MatchBoxModel mbm = home.mbm;
            //if (mbm.getAvailableSJ() < 1) news = "This phone is not using solar power :(";
            final String handleNews = mbm.getLastHomeMsg();
            final Handler handler = new Handler();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    sunCubeButton.setText(handleNews);
                    sunCubeButton.setTextColor(Color.LTGRAY);
                    sunCubeButton.setTextSize(16);
                }
            });
            //sunCubeButton.setOnClickListener(newsClickListener);
            //sunCubeImageButton.setOnClickListener(newsClickListener);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception updating news feed: "+e.getClass()+": "+e.getMessage());
        }
    }

    class AnimateRingTask implements Runnable {

        private long target = 0;
        private final long step = 500;
        private boolean upward = true;
        private MediaPlayer soundPlayer = null;

        public AnimateRingTask(long target, boolean playSound) {
            try {
                animationInProgress = true;
                this.target = target;
                upward = (lastRingPosition < target);
                if (playSound) {
                    if (soundPlayer != null) {
                        soundPlayer.release();
                        soundPlayer = null;
                    }
                    soundPlayer = MediaPlayer.create(home.getApplicationContext(), R.raw.refillsound);
                    soundPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    soundPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            stopSound();
                        }
                    });
                    soundPlayer.start();
                    playNextSound = false;
                } else {
                    VeeyahLogger.i(TAG, "SoundPlayer is null? playsound: "+playNextSound);
                }
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Exeption initializing ring animator: "+e.getClass()+": "+e.getMessage());
            }
        }

        protected void halt() {
            frame.removeCallbacks(animator);
            lastRingPosition = target;
            //stopSound();
            animationInProgress = false;
            if (currentPort.getEnergy() >= 9999) {
                enlargeSJCircleIcon();
            }
            VeeyahLogger.i(TAG, "Animator was halted.");
        }

        @Override
        synchronized public void run() {
            if (upward && (lastRingPosition < target)) {
                lastRingPosition += step;
                updateSunJouleRing(lastRingPosition);
                //VeeyahLogger.i(TAG, "Animate to: "+lastRingPosition);
            } else if (!upward && (lastRingPosition > target)) {
                lastRingPosition -= step;
                updateSunJouleRing(lastRingPosition);
            } else {
                VeeyahLogger.i(TAG, "Animator finished");
                updateSunJouleRing(target);
                updateCenterTimer();
                lastRingPosition = target;
                animationInProgress = false;
                animator = null;
                // Animate enlarging sunJouleIconCircle
                if (currentPort.getEnergy() >= 9999) {
                    enlargeSJCircleIcon();
                }
                //stopSound();
                return;
            }
            frame.removeCallbacks(animator);
            frame.postDelayed(animator, 10);
        }

        private void stopSound() {
            try {
                if (soundPlayer != null) {
                    if (soundPlayer.isPlaying()) soundPlayer.stop();
                    soundPlayer.reset();
                    soundPlayer.release();
                    soundPlayer = null;
                }
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Exception stopping sound effect: "+e.getClass()+": "+e.getMessage());
            }
        }
    }

    protected void enlargeSJCircleIcon() {
        try {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                if (rootView == null) throw new ViaSolarException("rootView is null!");
                ImageButton sunJouleIconButton = (ImageButton) rootView.findViewById(R.id.sunJouleIconCircle);
                ImageView sunJouleIcon = (ImageView) rootView.findViewById(R.id.sunJouleIcon);
                ObjectAnimator buttonScaleXAnimator = ObjectAnimator.ofFloat(sunJouleIconButton, "scaleX", 1, 1.25f, 1);
                ObjectAnimator buttonScaleYAnimator = ObjectAnimator.ofFloat(sunJouleIconButton, "scaleY", 1, 1.25f, 1);
                ObjectAnimator iconScaleXAnimator = ObjectAnimator.ofFloat(sunJouleIcon, "scaleX", 1, 1.25f, 1);
                ObjectAnimator iconScaleYAnimator = ObjectAnimator.ofFloat(sunJouleIcon, "scaleY", 1, 1.25f, 1);
                final AnimatorSet animatorSet = new AnimatorSet();
                if (animatorSet != null) {
                    buttonScaleXAnimator.setDuration(750);
                    buttonScaleYAnimator.setDuration(750);
                    iconScaleXAnimator.setDuration(750);
                    iconScaleYAnimator.setDuration(750);
                    animatorSet.playTogether(buttonScaleXAnimator, buttonScaleYAnimator, iconScaleXAnimator, iconScaleYAnimator);
                    animatorSet.start();
                }
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "SunJouleIconCircle enlargement exception: "+e.getClass()+": "+e.getMessage());
        }
    }

    protected void hideHelp() {
        try {
            if (rootView == null) return;
            RelativeLayout helpOverlayLayout = (RelativeLayout) rootView.findViewById(R.id.helpOverlayRelativeLayout);
            if (View.VISIBLE == helpOverlayLayout.getVisibility()) {
                helpOverlayLayout.setVisibility(View.GONE);
                home.showingOverlay = false;
                home.setTitle(home.getString(R.string.app_name));
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception hiding help overlay: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void showHelpOverlay() {
        try {
            // Show help overlay layout
            MatchBoxModel mbm = ((Home_View) getActivity()).mbm;
            mbm.incMetric(DeviceMetricsManager.BTN_HELP);
            RelativeLayout helpOverlayLayout = (RelativeLayout) rootView.findViewById(R.id.helpOverlayRelativeLayout);
            home.showingOverlay = true;
            helpOverlayLayout.setVisibility(View.VISIBLE);
            home.setTitle(getActivity().getString(R.string.app_name) + " Help");
            helpOverlayLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideHelp();
                }
            });

            Button whatAreSunJoulesButton = (Button) rootView.findViewById(R.id.whatAreSunJoulesButton);
            whatAreSunJoulesButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    MatchBoxModel mbm = home.mbm;
                    mbm.incMetric(DeviceMetricsManager.BTN_WHATSJ);
                    LayoutInflater factory1 = LayoutInflater.from(home);
                    final View textEntryView1 = factory1.inflate(R.layout.dialog_what_are_sun_joules, null);
                    TextView link = (TextView) textEntryView1.findViewById(R.id.sjtxt05);
                    link.setText(Html.fromHtml("Subject to change - for more info see <a href=\"http://rechoice.org\">rechoice.org</a>"));
                    link.setMovementMethod(LinkMovementMethod.getInstance());
                    AlertDialog.Builder alert1 = new AlertDialog.Builder(home, R.style.NoPadding);
                    //alert1.setTitle(" ");  //need to have some text in title for icon to appear
                    alert1.setPositiveButton("OK", null);
                    //AlertDialog dialog = alert1.create();
                    alert1.setView(textEntryView1);
                    //alert1.setIcon(R.drawable.sj_header);
                    alert1.show();
                }
            });
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception showing help overlay: "+e.getClass()+": "+e.getMessage());
        }
    }

    protected void initSunJouleRing() {
        try {
            //Bitmap bmp2 = BitmapFactory.decodeResource(this.getResources(), R.drawable.example_circle);
            if (sunJouleRing == null) return;
            BitmapDrawable bmd = (BitmapDrawable) sunJouleRing.getDrawable();
            if (bmd == null) return;
            bmp2 = bmd.getBitmap();
            bmOverlay = Bitmap.createBitmap(bmp2.getWidth()+6, bmp2.getHeight()+6, Bitmap.Config.ARGB_8888);
            rectF = new RectF(-1, -1, bmp2.getWidth() + 6, bmp2.getHeight() + 6);

            p = new Paint();
            p.setXfermode(new PorterDuffXfermode(
                    android.graphics.PorterDuff.Mode.CLEAR));
            p.setStyle(Paint.Style.FILL);
            p.setStrokeWidth(0);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception initializing SunJoule ring: "+e.getClass()+": "+e.getMessage());
        }
    }

    synchronized protected void updateSunJouleRing(long fill) {
        try {
            if (!isAdded()) return;
            //VeeyahLogger.i(TAG, "Free memory: "+Runtime.getRuntime().freeMemory());
            //VeeyahLogger.i(TAG, "Redraw SunJouleRing for: "+fill+" sj");

            if (bmp2 ==null || bmOverlay == null) initSunJouleRing();
            if (bmp2 ==null || bmOverlay == null) return;

            c = new Canvas(bmOverlay);
            c.drawBitmap(bmp2, 0, 0, null);

            float endAngle = fill*10/13;  //13 =~ 1000*3.6/275
            //VeeyahLogger.i("Veeyah", "endAngle = "+endAngle);
            if (endAngle > 0 && endAngle < 275) {
                c.drawArc(rectF, 110, -(325 - endAngle), true, p);
            } else if (endAngle < 1) { //empty circle
                c.drawArc(rectF, 110, 360, true, p);
            }
            //for full circle, do nothing
            sunJouleRing.setImageBitmap(bmOverlay);
            sunJouleRing.invalidate();

            //updateCenterTimer(fill);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception redrawing SunJouleRing: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void updateCenterTimer() {
        try {
            if (lastUpdateView == null) throw new ViaSolarException("LastUdpate element is null!");
            if (powerView == null) throw new ViaSolarException("PowerView element is null!");

            handler.post(new Runnable() {
               @Override
                public void run() {
                   double power = 0;
                   if (currentPort.getPower() > 0) power = currentPort.getPower();
                   lastUpdateView.setText("Read At: "+currentPort.getLastUpdateTime()+"\n"+power+" SJ/hr\nUnit ID: "+currentPort.getUnitID());
                   if (currentPort.getEnergy() != -1) {
                       powerView.setText(currentPort.getEnergy() + " SJ");
                   } else {
                       powerView.setText("Unknown Energy");
                   }
               }
            });
            lastUpdateView.setTextColor(Color.LTGRAY);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception updating time remaining: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void updateCenterSunJouleIcon(long sj) {
        try {
            if (sunJouleIcon == null) throw new ViaSolarException("SunJouleIcon is null!");
            sunJouleIcon.setImageResource(R.drawable.sj_green);
            /*if (sj > 216 && sj <= 360) {
                sunJouleIcon.setImageResource(R.drawable.sj_yellow);
            } else if (sj > 360) {
                sunJouleIcon.setImageResource(R.drawable.sj_green);
            } else {
                sunJouleIcon.setImageResource(R.drawable.sj_red);
            }*/
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception updating center SunJoule icon: "+e.getClass()+": "+e.getMessage());
        }

        try {
            if (centerStatus == null) throw new ViaSolarException("CenterStatus is null!");
            if (centerSuccess == null) throw new ViaSolarException("CenterSuccess is null!");
            /*if (sj > 0) {
                centerStatus.setVisibility(View.VISIBLE);
                centerSuccess.setVisibility(View.GONE);
            } else {
                centerStatus.setVisibility(View.GONE);
                centerSuccess.setVisibility(View.VISIBLE);
            }*/
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception updating center status message: "+e.getClass()+": "+e.getMessage());
        }
    }

}