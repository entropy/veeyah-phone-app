package com.veeyah.plugsolar;

public class ViaSolarException extends Exception {

    /**
     * @purpose This class provides an application specific exception.
     */

    private String msg;
    static final long serialVersionUID = 22389;

    public ViaSolarException(String message) {
        super();
        this.msg = message;
    }

    public String getMessage() {
        return msg;
    }
}
