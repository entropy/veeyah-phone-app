package com.veeyah.plugsolar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.SurfaceHolder;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import java.util.EnumSet;

public class VideoPlayer extends Activity
        implements SurfaceHolder.Callback {

    /**
     * @purpose This class manages a view that plays a video.
     * It is a state-machine that relies on a VideoView.
     * It plays a video specified in the intent.
     * If the video completes successfully, it triggers a refill process for a device or SunPort.
     * Otherwise, it toasts an error message and passes back to the main view.
     */

    Uri targetUri;
    private final String TAG = "[ Veeyah VideoPlayer "+VeeyahConstants.VERSION_NUMBER+"]";
    private State currentState;
    private SimpleTimer timer;
    private Handler handler = new Handler();
    private int oldVol = -12;
    private boolean mute = false;  //mute sound?
    private boolean sunport = false; //is this video for a sunport? true=>sunport, false=>device
    public final static String MUTE_LABEL = "mute";
    public final static String SUNPORT_LABEL = "sunport";
    private VideoView videoView;

    public enum State {
        IDLE, ERROR, INITIALIZED, PREPARED, STARTED, STOPPED
    }

    private class SimpleTimer implements Runnable {

        private ProgressDialog progressDialog = null;
        private boolean stopped = false;

        public SimpleTimer(ProgressDialog pg) {
            this.progressDialog = pg;
        }

        public void kill() {
            stopped = true;
        }

        @Override
        public void run() {
            try {
                if (progressDialog == null) throw new ViaSolarException("SimpleTimer was not initialized!");


                int timer = 0;
                while (!State.STARTED.equals(currentState)) {
                    Thread.sleep(50);
                    if (stopped) break;
                    timer++;
                    if (timer > 400) {
                        dismissProgressDialog(progressDialog);
                        returnWithError();
                        return;
                    }
                }
                dismissProgressDialog(progressDialog);
            } catch (InterruptedException e) {
                VeeyahLogger.i(TAG, "InterruptedException: "+e.toString());
            } catch (ViaSolarException vse) {
                VeeyahLogger.e(TAG, "ViaSolarException: "+vse.getMessage());
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            handler = new Handler();
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle(getString(R.string.app_name));
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    // Show the progress dialog
                    progressDialog.show();
                }
            });

            timer = new SimpleTimer(progressDialog);
            new Thread(timer).start();
            targetUri = this.getIntent().getData();
            VeeyahLogger.i(TAG, "Target URI = "+targetUri);

            oldVol = this.getIntent().getIntExtra(MUTE_LABEL, -12);
            mute = (oldVol > -1);
            sunport = this.getIntent().getBooleanExtra(SUNPORT_LABEL, false);

            //TextView mediaUri = (TextView)findViewById(R.id.mediauri);

            //Log.d("VideoPlayer", "Play video at: " + targetUri.toString());
            //mediaUri.setText(targetUri.toString());

            setContentView(R.layout.playerlayout);
            getWindow().setFormat(PixelFormat.UNKNOWN);
            videoView = (VideoView) findViewById(R.id.videoview);
            SurfaceHolder surfaceHolder = videoView.getHolder();
            if (surfaceHolder == null) throw new ViaSolarException("surfaceHolder is null in VideoPlayer.onCreate()");
            surfaceHolder.addCallback(this);
            //mediaPlayer = videoView.get;
            currentState = State.IDLE;
            /*videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    VeeyahLogger.i(TAG, "MediaPlayer prepared");
                    //mPlayer = mediaPlayer;
                    allGo = true;
                }
            });*/
            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    Intent intent = new Intent();
                    intent.setClass(ViaSolar.getContext(), Home_View.class);
                    if (mute) {
                        intent.putExtra(MUTE_LABEL, oldVol);
                    }
                    intent.putExtra(SUNPORT_LABEL, sunport);
                    intent.setType(Home_View.GET_SUNJOULES_TYPE);
                    startActivity(intent);
                    finish();
                }
            });
            videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
                    VeeyahLogger.d(TAG, "on error "+what+"; "+extra);
                    currentState = State.ERROR;
                    returnWithError();
                    return false;
                }
            });
            //mediaPlayer.setOnPreparedListener(mOnPreparedListener);
            //mediaPlayer.setOnBufferingUpdateListener(mOnBufferingUpdateListener);
            //videoView.setOnErrorListener(mOnErrorListener);
            //mediaPlayer.setOnInfoListener(mOnInfoListener);
            MediaController ctrl = new MediaController(this);
            ctrl.setMediaPlayer(videoView);
            videoView.setMediaController(ctrl);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception creating video player: "+e.getClass()+": "+e.getMessage());
        }
    }

    private boolean startVideo() {
        VeeyahLogger.i(TAG, "start playing veeyah video");
        try {
            if (mute) {
                AudioManager am = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
                am.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
            }
            videoView.start();
            currentState = State.STARTED;
            timer.kill();
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception starting video: "+e.getClass()+": "+e.getMessage());
        }
        return(true);
    }

    @Override
    public void onConfigurationChanged (Configuration newConfig) {
        //do nothing
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onDestroy() {
        VeeyahLogger.i(TAG, "MediaPlayer destroyed.");
        super.onDestroy();
    }

    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
        //do nothing

    }

    @Override
    public void surfaceCreated(SurfaceHolder arg0) {
        VeeyahLogger.i(TAG, "Surface created");

        try {
            if (currentState == State.IDLE) {
                videoView.setVideoURI(targetUri);
                currentState = State.INITIALIZED;
            } else {
                VeeyahLogger.w(TAG, "Bad state for setDataSource: "+currentState);
            }
            if (EnumSet.of(State.INITIALIZED, State.STOPPED).contains(currentState)) {
                videoView.requestFocus();
                currentState = State.PREPARED;
            } else {
                VeeyahLogger.w(TAG, "Bad state for prepareAsynch: "+currentState);
            }
            VeeyahLogger.i(TAG, "PrepareAsynch completed");
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception preparing video: "+e.getClass()+": "+e.getMessage());
        }

        VeeyahLogger.i(TAG, "Surface prepared");
        startVideo();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder arg0) {
        // do nothing
    }

    private void returnWithError() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(
                            ViaSolar.getContext(),
                            "The video could not be loaded.\nPlease try again from a location with better network access.",
                            Toast.LENGTH_LONG).show();
                }
            });

            if (timer != null) timer.kill();
            Thread.sleep(50);
            VeeyahLogger.i(TAG, "Destroy VideoPlayer");
            Intent intent = new Intent();
            intent.setClass(ViaSolar.getContext(),
                    Home_View.class);
            startActivity(intent);
            finish();
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception returning from videoPlayer error: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void dismissProgressDialog(final ProgressDialog pg) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                pg.dismiss();
            }
        });
    }

}
