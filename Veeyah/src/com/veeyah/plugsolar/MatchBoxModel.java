package com.veeyah.plugsolar;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.util.Log;

import com.veeyah.plugsolar.manager.DeviceMetricsManager;
import com.veeyah.plugsolar.manager.VideoFileManager;
import com.veeyah.utility.VeeyahMetric;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @purpose This class is the main data model for the application.
 * It uses shared preferences to store the data, so synchronization is handled by Android.
 */

public class MatchBoxModel implements Parcelable {

        private final String TAG = "[ Veeyah MatchBoxModel "+VeeyahConstants.VERSION_NUMBER+"]";
	    private String startTime, lastTime, deviceID, lastHomeMsg;
        private String userName, userEmail;
        private String shareLink;
        private String registeredPlugs;
	    private int totCap, totCharge, totDischarge, mostDays, mwh, mwhTarget;
	    private long sjBurn, sjAvail, lastPoint, sjCache;  //these are in hundredths of units, so they should be divided by 100 for display
	    private long idleTime, lastRingPos, lastMetricReport, lastMWhUpdate;
        private boolean stickyNotification;
        private SharedPreferences sh = null;
	    private static final int DEFAULT_CAPACITY = 12;  //in watthours
        private static final String DATE_FORMAT = "EEE MMM dd kk:mm:ss zzz yyyy";

	    public MatchBoxModel() {
            sh = PreferenceManager.getDefaultSharedPreferences(ViaSolar.getContext());
	        startTime = printTime();
	        lastTime = startTime;
	        totCharge = 0;
	        totDischarge = 0;
	        lastPoint = -1;
            sjCache = 0;
	        totCap = getBatteryCap();
	        sjBurn = 0;
	        idleTime = 0;
            lastRingPos = 0;
            lastMetricReport = 0;
            mostDays = 0;
	        deviceID = VeeyahConstants.DEFAULT_DEVICE_ID;
            userName = "";
            userEmail = "";
            lastHomeMsg = VeeyahConstants.DEFAULT_HOME_MSG;
            shareLink = "";
            stickyNotification = false;
            registeredPlugs = "";
            mwh = 0;
            mwhTarget = 0;
            lastMWhUpdate = 0;
	    }

	    public MatchBoxModel(Parcel in) {
            this();
            in.readBundle();
	    }

	    public void writeToParcel(Parcel out, int flags) {
	        Bundle b = new Bundle();
	        b.putString(VeeyahConstants.START_TIME, startTime);
	        b.putString(VeeyahConstants.LAST_TIME, lastTime);
	        b.putInt(VeeyahConstants.TOTAL_CHARGE, totCharge);
	        b.putInt(VeeyahConstants.TOTAL_DISCHARGE, totDischarge);
	        b.putLong(VeeyahConstants.LAST_POINT, lastPoint);
	        b.putInt(VeeyahConstants.TOTAL_CAPACITY, totCap);
	        b.putLong(VeeyahConstants.SUNJOULES_BURNED, sjBurn);
	        b.putLong(VeeyahConstants.SUNJOULES_AVAILABLE, sjAvail);
            b.putLong(VeeyahConstants.SUNJOULE_CACHE, sjCache);
	        b.putLong(VeeyahConstants.IDLE_TIME, idleTime);
            b.putLong(VeeyahConstants.LAST_RING_POSITION, lastRingPos);
            b.putLong(VeeyahConstants.LAST_METRIC_REPORT, lastMetricReport);
	        b.putString(VeeyahConstants.DEVICE_ID, deviceID);
            b.putString(VeeyahConstants.USER_NAME, userName);
            b.putString(VeeyahConstants.USER_EMAIL, userEmail);
            b.putString(VeeyahConstants.LAST_HOME_MSG, lastHomeMsg);
            b.putString(VeeyahConstants.SHARE_LINK, shareLink);
            b.putBoolean(VeeyahConstants.STICKY_NOTIFICATIONS, stickyNotification);
            b.putInt(VeeyahConstants.MOST_DAYS, mostDays);
            b.putString(VeeyahConstants.REGISTERED_PLUGS, registeredPlugs);
            b.putLong(VeeyahConstants.LAST_MWH_UPDATE, lastMWhUpdate);
            b.putInt(VeeyahConstants.TOTAL_MWH, mwh);
            b.putInt(VeeyahConstants.TARGET_MWH, mwhTarget);
	        out.writeBundle(b);
	    }

	    public static final Parcelable.Creator<MatchBoxModel> CREATOR = new Parcelable.Creator<MatchBoxModel>() {
	        public MatchBoxModel createFromParcel(Parcel in) {
	            return new MatchBoxModel(in);
	        }

	        public MatchBoxModel[] newArray(int size) {
	            return new MatchBoxModel[size];
	        }
	    };

	    public String getStartTime() {
            if (sh != null) startTime = sh.getString(VeeyahConstants.START_TIME, startTime);
            return(startTime);
	    }

	    public void setStartTime(String sTime) {
	        if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putString(VeeyahConstants.START_TIME, sTime);
                e.apply();
            }
            startTime = sTime;
	    }

	    public String getLastTime() {
            if (sh != null) lastTime = sh.getString(VeeyahConstants.LAST_TIME, printTime());
            return(lastTime);
	    }

    public long getMinSinceStartTime() {
        long millis = 0;
        try {
            Date date = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH).parse(getStartTime());
            millis = System.currentTimeMillis() - date.getTime();
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception getting minutes since start time: "+e.getClass()+": "+e.getMessage());
        }
        return (millis/1000/60);
    }

        public long getMinSinceLastTime() {
            long millis = 0;
            try {
                Date date = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH).parse(getLastTime());
                millis = System.currentTimeMillis() - date.getTime();
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Exception getting minutes since last update: "+e.getClass()+": "+e.getMessage());
            }
            return (millis/1000/60);
        }

	    public void setLastTime(String lTime) {
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putString(VeeyahConstants.LAST_TIME, lTime);
                e.apply();
            }
            lastTime = lTime;
	    }

	    public String getDeviceID() {
            if (sh != null) deviceID = sh.getString(VeeyahConstants.DEVICE_ID, deviceID);
            return(deviceID);
	    }

	    public void setDeviceID(String id) {
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putString(VeeyahConstants.DEVICE_ID, id);
                e.apply();
            }
            deviceID = id;
	    }

	    public int getTotCharge() {
            if (sh != null) totCharge = sh.getInt(VeeyahConstants.TOTAL_CHARGE, totCharge);
            return(totCharge);
	    }

	    public int getTotDischarge() {
            if (sh != null) totDischarge = sh.getInt(VeeyahConstants.TOTAL_DISCHARGE, totDischarge);
            return(totDischarge);
	    }

        public int getMostDays() {
            if (sh != null) mostDays = sh.getInt(VeeyahConstants.MOST_DAYS, mostDays);
            //if out of solar, return the last value
            if (getAvailableSJ() < 1) return(mostDays);
            //if got solar, calculate current value, set variable, and return it
            int currentDays = (int) getMinSinceStartTime()/60/24;
            if (currentDays > mostDays) {
                mostDays = currentDays;
                if (sh != null) {
                    SharedPreferences.Editor e = sh.edit();
                    e.putInt(VeeyahConstants.MOST_DAYS, mostDays);
                    e.apply();
                }
            }
            return(mostDays);
        }

	    public long getLastPoint() {
            if (sh != null) lastPoint = sh.getLong(VeeyahConstants.LAST_POINT, lastPoint);
	        return(lastPoint);
	    }

	    public int getCapacity() {
            if (sh != null) totCap = sh.getInt(VeeyahConstants.TOTAL_CAPACITY, totCap);
	        if (totCap < 1) totCap = DEFAULT_CAPACITY;
	        return(totCap);
	    }

	    public long getUsedSJ() {
            if (sh != null) sjBurn = sh.getLong(VeeyahConstants.SUNJOULES_BURNED, sjBurn);
            return(sjBurn);
	    }

	    public long getAvailableSJ() {
            if (sh != null) sjAvail = sh.getLong(VeeyahConstants.SUNJOULES_AVAILABLE, sjAvail);
            return(sjAvail);
	    }

	    public long getIdleTime() {
            if (sh != null) idleTime = sh.getLong(VeeyahConstants.IDLE_TIME, idleTime);
            return(idleTime);
	    }

        public long getLastRingPos() {
            if (sh != null) idleTime = sh.getLong(VeeyahConstants.LAST_RING_POSITION, lastRingPos);
            return(lastRingPos);
        }

        public long getCachedSJ() {
            if (sh != null) sjCache = sh.getLong(VeeyahConstants.SUNJOULE_CACHE, sjCache);
            return(sjCache);
        }

        public long getLastMetricReportTime() {
            if (sh != null) lastMetricReport = sh.getLong(VeeyahConstants.LAST_METRIC_REPORT, lastMetricReport);
            return(lastMetricReport);
        }

        public String getMWhUpdateDate() {
            try {
                if (sh != null) lastMWhUpdate = sh.getLong(VeeyahConstants.LAST_MWH_UPDATE, lastMWhUpdate);
                Date updateDate = new Date(lastMWhUpdate);
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
                return(sdf.format(updateDate));
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Exception getting MWh udpate date: "+e.getClass()+": "+e.getMessage());
            }
            return("");
        }

        public int getMWh() {
            if (sh != null) mwh = sh.getInt(VeeyahConstants.TOTAL_MWH, mwh);
            return(mwh);
        }

        public int getMWhTarget() {
            if (sh != null) mwhTarget = sh.getInt(VeeyahConstants.TARGET_MWH, mwhTarget);
            return(mwhTarget);
        }

	    public void setAvailableSJ(long val) {
	        if (Math.abs(val) >= 0) {
                if (sh != null) {
                    SharedPreferences.Editor e = sh.edit();
                    e.putLong(VeeyahConstants.SUNJOULES_AVAILABLE, val);
                    e.apply();
                }
                sjAvail = val;
            }
	    }

	    public void setUsedSJ(long val) {
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putLong(VeeyahConstants.SUNJOULES_BURNED, val);
                e.apply();
            }
            sjBurn = val;
	    }

        public void setMostDays(int val) {
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putInt(VeeyahConstants.MOST_DAYS, val);
                e.apply();
            }
            mostDays = val;
        }

        public void setCachedSJ(long val) {
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putLong(VeeyahConstants.SUNJOULE_CACHE, val);
                e.apply();
            }
            sjCache = val;
        }

	    public void incTotCharge(long val) {
	        totCharge += val;
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putLong(VeeyahConstants.TOTAL_CHARGE, val);
                e.apply();
            }
	    }

	    public void incTotDischarge(long val) {
	        totDischarge += val;
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putLong(VeeyahConstants.TOTAL_DISCHARGE, val);
                e.apply();
            }
	    }

        public void setIdleTime(long val) {
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putLong(VeeyahConstants.IDLE_TIME, val);
                e.apply();
            }
            idleTime = val;
        }

        public void setMetricReportTime(long val) {
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putLong(VeeyahConstants.LAST_METRIC_REPORT, val);
                e.apply();
            }
            lastMetricReport = val;
        }

        public void setLastRingPosition(long val) {
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putLong(VeeyahConstants.LAST_RING_POSITION, val);
                e.apply();
            }
            lastRingPos = val;
        }

	    public void setTotCharge(int val) {
	        if (val > totCharge) {
                totCharge = val;
                if (sh != null) {
                    SharedPreferences.Editor e = sh.edit();
                    e.putInt(VeeyahConstants.TOTAL_CHARGE, val);
                    e.apply();
                }
            }
	    }

	    public void setTotDischarge(int val) {
	        if (val > totDischarge)  {
                totDischarge = val;
                if (sh != null) {
                    SharedPreferences.Editor e = sh.edit();
                    e.putInt(VeeyahConstants.TOTAL_DISCHARGE, val);
                    e.apply();
                }
            }
	    }

	    public void setlastPoint(long val) {
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putLong(VeeyahConstants.LAST_POINT, val);
                e.apply();
            }
            lastPoint = val;
	    }

        private void setLastMwhUpdate(long val) {
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putLong(VeeyahConstants.LAST_MWH_UPDATE, val);
                e.apply();
            }
            lastMWhUpdate = val;
        }

        public void setTotalMWh(int val) {
            if (val < 1) return;
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putInt(VeeyahConstants.TOTAL_MWH, val);
                e.apply();
            }
            mwh = val;
            setLastMwhUpdate(System.currentTimeMillis());
        }

        public void setTargetMWh(int val) {
            if (val < 1) return;
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putInt(VeeyahConstants.TARGET_MWH, val);
                e.apply();
            }
            mwhTarget = val;
        }

        public String getUserName() {
            if (sh != null) userName = sh.getString(VeeyahConstants.USER_NAME, userName);
            return(userName);
        }

        public void setUserName(String uName) {
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putString(VeeyahConstants.USER_NAME, uName);
                e.apply();
            }
            userName = uName;
        }

        public String getUserEmail() {
            if (sh != null) userEmail = sh.getString(VeeyahConstants.USER_EMAIL, userName);
            return(userEmail);
        }

        public void setUserEmail(String uEmail) {
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putString(VeeyahConstants.USER_EMAIL, uEmail);
                e.apply();
            }
            userEmail = uEmail;
        }

    public String getLastHomeMsg() {
        if (sh != null) lastHomeMsg = sh.getString(VeeyahConstants.LAST_HOME_MSG, lastHomeMsg);
        return(lastHomeMsg);
    }

    public void setLastHomeMsg(String msg) {
        if (sh != null) {
            SharedPreferences.Editor e = sh.edit();
            e.putString(VeeyahConstants.LAST_HOME_MSG, msg);
            e.apply();
        }
        lastHomeMsg = msg;
    }

	    public void setCapacity(int val) {
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putInt(VeeyahConstants.TOTAL_CAPACITY, val);
                e.apply();
            }
	        totCap = val;
	    }

	    public String printTime() {
	        Date d = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
	        return(sdf.format(d));
	    }

        public boolean isStickyNotification() {
            if (sh != null) {
                stickyNotification = sh.getBoolean(VeeyahConstants.STICKY_NOTIFICATIONS, false);
            }
            return(stickyNotification);
        }

        public void setStickyNotification(boolean isSticky) {
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putBoolean(VeeyahConstants.STICKY_NOTIFICATIONS, isSticky);
                e.apply();
            }
            stickyNotification = isSticky;
        }

        public boolean isFirstUse() {
            boolean firstUse = true;
            if (sh != null) {
                firstUse = sh.getBoolean(VeeyahConstants.FIRST_USE, true);
            }
            return(firstUse);
        }

        public void setFirstUse(boolean isFirstUse) {
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putBoolean(VeeyahConstants.FIRST_USE, isFirstUse);
                e.apply();
            };
        }

        public String checkPlugName(String name) {
            if (sh != null) {
                return(sh.getString(name, ""));
            }
            return("");
        }

        public void storeName(String publicName, String privateName) {
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putString(publicName, privateName);
                e.apply();
            }
        }

        public void setShareLink(String link) {
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putString(VeeyahConstants.SHARE_LINK, link);
                e.apply();
            }
            shareLink = link;
        }

        public String getShareLink() throws ViaSolarException {
            try {
                if (sh != null) {
                    shareLink = sh.getString(VeeyahConstants.SHARE_LINK, shareLink);
                }
                if (!"".equals(shareLink) && shareLink != null) {
                    return (shareLink);
                } else {
                    //Toast.makeText(ctx, "Generating link...", Toast.LENGTH_SHORT).show();
                    String appShareURL = "http://veeyah.com/share?d=" + getDeviceID();
                    // Implement link shortening on appShareURL
                    String bitlyUrl = VeeyahConstants.BITLY_URL_PREFIX + appShareURL;
                    String bitlyReturn = getHttpReturn(bitlyUrl);
                    if (bitlyReturn != null) {
                        // We have a return from bit.ly
                        setShareLink(getBitlyUrl(bitlyReturn));
                        return (shareLink);
                    } else {
                        throw new ViaSolarException("Could not get a shortened URL!");
                    }
                }
            } catch (ViaSolarException vse) {
                throw vse;
            } catch (Exception e) {
                VeeyahLogger.e(TAG, "Exception getting share link: "+e.getClass()+": "+e.getMessage());
            }
            throw new ViaSolarException("Could not generate share link!");
        }

    public String getHttpReturn(String url) {
        // Method returns String that is contents of URL from URL String
        String result = null;
        HttpClient httpclient = new DefaultHttpClient();

        // Prepare a request object
        HttpGet httpget = new HttpGet(url);

        // Execute the request
        HttpResponse response;
        try {
            response = httpclient.execute(httpget);

            // Get hold of the response entity
            HttpEntity entity = response.getEntity();
            // If the response does not enclose an entity, there is no need
            // to worry about connection release

            if (entity != null) {
                // A Simple Response Read
                InputStream instream = entity.getContent();
                result = convertStreamToString(instream);
                // now you have the string representation of the HTML request
                instream.close();
                //Log.i(TAG, result);
            }
        } catch (Exception e) {
            VeeyahLogger.i(TAG, e.toString());
        }

        return result;
    }

    private static String convertStreamToString(InputStream is) {
	    /*
	     * To convert the InputStream to String we use the BufferedReader.readLine()
	     * method. We iterate until the BufferedReader return null which means
	     * there's no more data to read. Each line will appended to a StringBuilder
	     * and returned as String.
	     */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

        public boolean checkPort(SunPort sp) {
            try {
                if (sh != null) {
                    registeredPlugs = sh.getString(VeeyahConstants.REGISTERED_PLUGS, registeredPlugs);
                }
                if (registeredPlugs.contains(sp.getRawAddress())) return true;
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Exception checking port '" + sp.getAddress() + ": " + e.getClass() + ": " + e.getMessage());
            }
            return false;
        }

        public ArrayList<String> getRegisteredPorts() {
            ArrayList<String> ports = new ArrayList<String>();
            try {
                if (sh != null) {
                   registeredPlugs = sh.getString(VeeyahConstants.REGISTERED_PLUGS, registeredPlugs);
                }
                if (registeredPlugs != null && !"".equals(registeredPlugs)) {
                    for (int i=0; i<registeredPlugs.length(); i+=12) {
                        String nextAddr = registeredPlugs.substring(i, i+12);
                        ports.add(nextAddr);
                        //VeeyahLogger.i(TAG, "Has registered port: "+nextAddr);
                    }
                }
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Exception listing registered SunPorts: "+e.getClass()+": "+e.getMessage());
            }
            return(ports);
        }

        public void setRegisteredPorts(String ports) throws ViaSolarException {
            if (ports == null) throw new ViaSolarException("Cannot set registered ports to null");
            if ((ports.length() %12) != 0) throw new ViaSolarException("Port addresses must be 12-digits");
            if (sh != null) {
                SharedPreferences.Editor e = sh.edit();
                e.putString(VeeyahConstants.REGISTERED_PLUGS, ports);
                e.apply();
            }
            registeredPlugs = ports;
        }

        public void addSunPort(SunPort sp) {
            try {
                if (sh != null) {
                    registeredPlugs = sh.getString(VeeyahConstants.REGISTERED_PLUGS, registeredPlugs);
                }
                String rawAddr = sp.getRawAddress();
                if (getRegisteredPorts().contains(sp.getRawAddress())) return;
                if (rawAddr.length() != 12) throw new ViaSolarException("Invalid SunPort MAC address: "+rawAddr);
                registeredPlugs += sp.getRawAddress();
                setRegisteredPorts(registeredPlugs);
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Exception adding SunPort: "+e.getClass()+": "+e.getMessage());
            }
        }

        public void incMetric(String key) {
            try {
                if (key == null || "".equals(key)) throw new ViaSolarException("Invalid key!");
                //check that field is defined via reflection, probably more effort than it is worth
                /*if (!key.contains(DeviceMetricsManager.VIDEO_WATCHES_PREFIX)) {
                    boolean good = false;
                    Iterator<Field> fieldList = Arrays.asList(DeviceMetricsManager.class.getFields()).iterator();
                    while (fieldList.hasNext()) {
                        Field f = fieldList.next();
                        if (f.getType() == String.class) {
                            f.
                        }
                    }
                }*/
                if (sh != null) {
                    int current = sh.getInt(key, 0);
                    SharedPreferences.Editor e = sh.edit();
                    e.putInt(key, current++);
                    e.apply();
                }
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Exception incrementing metric: "+e.getClass()+": "+e.getMessage());
            }
        }

        public List<VeeyahMetric> getDeviceMetrics(VideoFileManager vfm) {
            ArrayList<VeeyahMetric> metrics = new ArrayList<VeeyahMetric>();
            try {
                metrics.add(new VeeyahMetric(DeviceMetricsManager.PHONE_CAPACITY, getBatteryCap()));
                metrics.addAll(vfm.getMetrics());
                if (sh != null) {
                    ArrayList<String> keyNames = new ArrayList<String>();
                    keyNames.add(DeviceMetricsManager.APP_LAUNCH);
                    keyNames.add(DeviceMetricsManager.APP_NOTIFICATION_LAUNCH);
                    keyNames.add(DeviceMetricsManager.APP_SHARES);
                    keyNames.add(DeviceMetricsManager.SCR_LOGIN);
                    keyNames.add(DeviceMetricsManager.SCR_MAIN);
                    keyNames.add(DeviceMetricsManager.SCR_INFO);
                    keyNames.add(DeviceMetricsManager.SCR_SETTINGS);
                    keyNames.add(DeviceMetricsManager.BTN_REGISTER);
                    keyNames.add(DeviceMetricsManager.BTN_MAIN);
                    keyNames.add(DeviceMetricsManager.BTN_RELOAD);
                    keyNames.add(DeviceMetricsManager.BTN_HELP);
                    keyNames.add(DeviceMetricsManager.BTN_WHATSJ);
                    keyNames.add(DeviceMetricsManager.BTN_COMMENT);
                    keyNames.add(DeviceMetricsManager.BTN_SHARE);
                    keyNames.add(DeviceMetricsManager.BTN_DETAIL);
                    keyNames.add(DeviceMetricsManager.BTN_SETTINGS);
                    keyNames.add(DeviceMetricsManager.BTN_UPDATE);
                    keyNames.add(DeviceMetricsManager.CHK_TERMS);
                    keyNames.add(DeviceMetricsManager.CHK_STICKY);
                    Iterator<String> keyIter = keyNames.iterator();
                    while (keyIter.hasNext()) {
                        String key = keyIter.next();
                        int value = sh.getInt(key, Integer.MIN_VALUE);
                        if (value > Integer.MIN_VALUE) {
                            metrics.add(new VeeyahMetric(DeviceMetricsManager.APP_LAUNCH, value));
                        }
                    }
                }
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Exception getting device metrics: "+e.getClass()+": "+e.getMessage());
            }
            return(metrics);
        }

	    private int getBatteryCap() {
	        File f = new File("/sys/class/power_supply/battery/charge_full_design");
	        if (!f.exists()) f = new File("/sys/class/power_supply/battery/energy_full");
	        if (!f.exists()) return(12);
	        String totStr = readLine(f);
	        try {
	            if (totStr.length() > 4) totStr = totStr.substring(0, 4);
	            return(Integer.parseInt(totStr)/200);
	        } catch (Exception e) {
	            VeeyahLogger.w(TAG, "Exception reading capacity: " + e.getClass() + ": " + e.getMessage());
	            return(-1);
	        }
	    }

	    private String readLine(File f) {
	        try {
	            BufferedReader br = new BufferedReader(new FileReader(f));
	            String line = br.readLine();
	            if (line == null) line = "";
	            br.close();
	            return(line);
	        } catch (Exception e) {
	            VeeyahLogger.w(TAG, "Exception reading current: "+e.getClass()+": "+e.getMessage());
	            return("");
	        }
	    }

	    public int describeContents() {
            return (deviceID+sjAvail).hashCode();
	    }

    public static String getBitlyUrl(String json) {
        String shortUrl = "";
        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject(json);
            JSONObject obj = (JSONObject) jsonObj.get("data");
            shortUrl = (String) obj.get("url");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return shortUrl;
    }
	}

