package com.veeyah.plugsolar;

/**
 * @purpose This class stores global constants.
 */

public final class VeeyahConstants {

    public final static String VERSION_NUMBER="2.0.14ble";
    public final static String WEBSERVICE_DOMAIN="http://cloudsolar.veeyah.com:8765";
    public final static String BITLY_API_KEY = "b7b40937bc768a21cb8959690eb3bae4282f1414";
    public final static String BITLY_URL_PREFIX = "https://api-ssl.bitly.com/v3/shorten?access_token=" + BITLY_API_KEY +"&longUrl=";

    //default messages
    public final static String DEFAULT_HOME_MSG = "My Phone is Powered via Solar!";
    public final static String DEFAULT_DEVICE_ID = "000000000000000000000000";
    public final static String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH24:mm:ss";
    public final static String DEFAULT_USER_EMAIL = "unknown";

    //data model field IDs
    public final static String START_TIME = "startTime";
    public final static String LAST_TIME = "lastTime";
    public final static String TOTAL_CHARGE = "totCharge";
    public final static String TOTAL_DISCHARGE = "totDischarge";
    public final static String LAST_POINT = "lastPoint";
    public final static String TOTAL_CAPACITY = "totCap";
    public final static String SUNJOULES_BURNED = "sjBurn";
    public final static String SUNJOULES_AVAILABLE = "sjAvail";
    public final static String LAST_RING_POSITION = "lastRingPos";
    public final static String SUNJOULE_CACHE = "sjCache";
    public final static String IDLE_TIME = "idleTime";
    public final static String DEVICE_ID = "deviceID";
    public final static String USER_NAME = "userName";
    public final static String USER_EMAIL = "userEmail";
    public final static String LAST_HOME_MSG = "lastHomeMsg";
    public final static String LAST_METRIC_REPORT = "lastMetricReport";
    public final static String STICKY_NOTIFICATIONS = "isSticky";
    public final static String FIRST_USE = "firstUse";
    public final static String MOST_DAYS = "mostDays";
    public final static String SHARE_LINK = "shareLink";
    public final static String CUST_NAME = "CUSTOMER NAME";
    public final static String CUST_EMAIL = "CUSTOMER EMAIL";
    public final static String REGISTERED_PLUGS = "regPlugs";
    public final static String LAST_MWH_UPDATE = "lastMWh";
    public final static String TOTAL_MWH = "mwh";
    public final static String TARGET_MWH = "targetMWh";
}
