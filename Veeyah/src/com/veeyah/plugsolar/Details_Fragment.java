package com.veeyah.plugsolar;

import android.animation.AnimatorSet;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.veeyah.plugsolar.manager.DeviceMetricsManager;
import com.veeyah.utility.Astro;

/**
 * @purpose This class is a fragment to display details of the device's energy usage.
 *
 */

public class Details_Fragment extends 	android.support.v4.app.Fragment {
    private final String TAG = "[ Veeyah Details_Fragment "+VeeyahConstants.VERSION_NUMBER+"]";
	private static TextView vhome;
    private Home_View home = null;
    View rootView = null;
	FrameLayout mainLayout;
	ImageView footerImageView;
	AnimatorSet animatorSet = null;


    @Override
    public void onDestroy() {
        VeeyahLogger.i(TAG, "onDestroy called");
        Home_View.unbindDrawables(rootView);
        super.onDestroy();
        VeeyahLogger.i(TAG, "onDestroy finished");
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            home = (Home_View) getActivity();
            MatchBoxModel mbm = home.mbm;
            mbm.incMetric(DeviceMetricsManager.SCR_INFO);
            rootView = inflater.inflate(R.layout.fragment_details, container, false);
            if (rootView == null) return null;
            vhome = (TextView) rootView.findViewById(R.id.veeyahhome);
            refreshView();

            mainLayout = (FrameLayout) rootView.findViewById(R.id.content_frame);
            footerImageView = (ImageView) rootView.findViewById(R.id.footerHomeImageView);

            setBackgroundImage();

            // Update the version info text view
            TextView versionInfoTextView = (TextView) rootView.findViewById(R.id.versionInfoTextView);
            PackageInfo packageInfo;
            String strVersion = "?.?";
            try {
                PackageManager pm = ViaSolar.getContext().getPackageManager();
                if (pm == null)
                    throw new ViaSolarException("PackageManager is null for Details_Fragment");
                packageInfo = pm.getPackageInfo(ViaSolar.getContext().getPackageName(), 0);
                strVersion = packageInfo.versionName;
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Exception getting package/version name: " + e.getClass() + ": " + e.getMessage());
            }

            versionInfoTextView.setText("Version " + strVersion);

            // Configure menu button
            ImageButton menuImageButton = (ImageButton) rootView.findViewById(R.id.menuImageButton);
            menuImageButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Home_View.mDrawerLayout.isDrawerOpen(Home_View.mDrawerList)) {
                        Home_View.mDrawerLayout.closeDrawer(Home_View.mDrawerList);
                    } else {
                        Home_View.mDrawerLayout.openDrawer(Home_View.mDrawerList);
                    }
                }
            });

            // Configure footer buttons
            ImageButton shareImageButton = (ImageButton) rootView.findViewById(R.id.shareImageButton);
            ImageButton homeImageButton = (ImageButton) rootView.findViewById(R.id.homeImageButton);
            ImageButton plugImageButton = (ImageButton) rootView.findViewById(R.id.plugSolarImageButton);
            ImageButton phoneImageButton = (ImageButton) rootView.findViewById(R.id.phoneImageButton);
            ImageButton settingsImageButton = (ImageButton) rootView.findViewById(R.id.settingsImageButton);

            shareImageButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.share();
                }
            });
            homeImageButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.selectNavDrawerItem(Home_View.NAV_DRAWER_ITEM_HOME);
                }
            });
            plugImageButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.clickPlugIcon();
                }
            });
            phoneImageButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.selectNavDrawerItem(Home_View.NAV_DRAWER_ITEM_PHONE);
                }
            });
            settingsImageButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.selectNavDrawerItem(Home_View.NAV_DRAWER_ITEM_SETTINGS);
                }
            });

            // Set help tip longClickListeners
            home.setHelpLongClickListener(shareImageButton, "Share this app!");
            home.setHelpLongClickListener(homeImageButton, "Return to the home screen.");
            home.setHelpLongClickListener(plugImageButton, "View your SunPort's info.");
            home.setHelpLongClickListener(phoneImageButton, "View your phone's solar energy.");
            home.setHelpLongClickListener(settingsImageButton, "View the app settings.");

        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception loading details screen: "+e.getClass()+": "+e.getMessage());
        }
        return rootView;
	}

	protected void refreshView() {
        try {
            if (vhome == null) return;

            MatchBoxModel mbm = ((Home_View) getActivity()).mbm;

            String toDisp = "<b>Using Solar Energy Since:</b><br> " + mbm.getStartTime() + "<br><br>";
            toDisp += "<b>Longest Streak Being Solar:</b><br> "  + mbm.getMostDays() + " days<br><br><br>";
            toDisp += "<b>Last Battery Update At:</b><br> "  + mbm.getLastTime() + "<br><br>";
            toDisp += "<b>Battery Level:</b> " + mbm.getLastPoint() + "%<br>";
            toDisp += "<b>Battery Capacity:</b> " + mbm.getCapacity() + " Wh<br>";
            toDisp += "<b>SunJoules Used:</b> " + dispLong(mbm.getUsedSJ()) + "<br>";
            toDisp += "<b>SunJoules Available:</b> " + dispLong(mbm.getAvailableSJ());

            vhome.setText(Html.fromHtml(toDisp));
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception refreshing view: "+e.getClass()+": "+e.getMessage());
        }
	}
	 
	private static String dispLong(long l) { // displays a long in hundredths with two decimals
		long dec = (l % 100);
		String padding = "";
		if (dec < 10)
			padding = "0";
		return ((l / 100) + "." + padding + dec);
	}
	
	@Override
	public void onResume() {
		setBackgroundImage();
		super.onResume();
	}

	void setBackgroundImage() {
        try {
            // Contextual background images
            if (Astro.isNight()) {
                mainLayout.setBackgroundResource(R.drawable.sky_bg_04);
                footerImageView.setImageResource(R.drawable.footer_moon);

                // Stop any animation on the footer for night image
                if (animatorSet != null) {
                    animatorSet.cancel();
                }
            } else {
                mainLayout.setBackgroundResource(R.drawable.sky_bg);
                footerImageView.setImageResource(R.drawable.footer_sun);
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception setting background image: "+e.getClass()+": "+e.getMessage());
        }
    }
}