package com.veeyah.plugsolar;
import android.content.Context;

import com.veeyah.plugsolar.manager.DeviceMetricsManager;

public class ViaSolar extends android.app.Application {

    /**
     * @purpose This class provides an application-wide context.
     */

    private static ViaSolar instance;

    public ViaSolar() {
        instance = this;
    }

    public static Context getContext() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            MatchBoxModel mbm = new MatchBoxModel();
            mbm.incMetric(DeviceMetricsManager.APP_LAUNCH);
        } catch (Exception e) {
            VeeyahLogger.w("[Veeyah App Context]", "Failed to record app launch: "+e.getClass()+": "+e.getMessage());
        }
    }


}
