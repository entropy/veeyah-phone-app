package com.veeyah.utility;

public class VeeyahMetric {

    /**
     * @purpose This class is a simple data model for an arbitrary metric.
     */

    private String name;
    private int value;

    public VeeyahMetric(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return(name);
    }

    public int getValue() {
        return(value);
    }
}
