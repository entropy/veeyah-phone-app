package com.veeyah.utility;

import java.util.Date;

/**
 * @purpose This class is a data model for SunPort energy data.
 */

public class SunPortEnergyData {

    private Date dateTime;
    private int energySJ;
    private int energyPercent;

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date value) {
        this.dateTime = value;
    }

    public int getEnergySJ() {
        return energySJ;
    }

    public void setEnergySJ(int value) {
        this.energySJ = value;
    }

    public int getEnergyPercent() {
        return energyPercent;
    }

    public void setEnergyPercent(int value) {
        this.energyPercent = value;
    }

}

