package com.veeyah.matchbox;

import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;

public class MySummaryResponse {

    protected XMLGregorianCalendar requestTime;
    protected String deviceID;
    protected XMLGregorianCalendar responseTime;
    protected int sunJouleBalance;
    protected int sunJouleUsageToday;
    protected int sunJouleUsageThisWeek;
    protected int sunJouleUsageThisMonth;
    protected int sunJouleUsageThisYear;
    protected int sunJouleUsageAllTime;
    protected List<LocationSummary> productionSite;
    protected List<SunJouleUsageMethod> usageMethod;
    protected List<VeeyahFriend> friend;
    protected List<String> news;

    /**
     * Gets the value of the requestTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRequestTime() {
        return requestTime;
    }

    /**
     * Sets the value of the requestTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRequestTime(XMLGregorianCalendar value) {
        this.requestTime = value;
    }

    /**
     * Gets the value of the deviceID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceID() {
        return deviceID;
    }

    /**
     * Sets the value of the deviceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceID(String value) {
        this.deviceID = value;
    }

    /**
     * Gets the value of the responseTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getResponseTime() {
        return responseTime;
    }

    /**
     * Sets the value of the responseTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setResponseTime(XMLGregorianCalendar value) {
        this.responseTime = value;
    }

    /**
     * Gets the value of the sunJouleBalance property.
     * 
     */
    public int getSunJouleBalance() {
        return sunJouleBalance;
    }

    /**
     * Sets the value of the sunJouleBalance property.
     * 
     */
    public void setSunJouleBalance(int value) {
        this.sunJouleBalance = value;
    }

    /**
     * Gets the value of the sunJouleUsageToday property.
     * 
     */
    public int getSunJouleUsageToday() {
        return sunJouleUsageToday;
    }

    /**
     * Sets the value of the sunJouleUsageToday property.
     * 
     */
    public void setSunJouleUsageToday(int value) {
        this.sunJouleUsageToday = value;
    }

    /**
     * Gets the value of the sunJouleUsageThisWeek property.
     * 
     */
    public int getSunJouleUsageThisWeek() {
        return sunJouleUsageThisWeek;
    }

    /**
     * Sets the value of the sunJouleUsageThisWeek property.
     * 
     */
    public void setSunJouleUsageThisWeek(int value) {
        this.sunJouleUsageThisWeek = value;
    }

    /**
     * Gets the value of the sunJouleUsageThisMonth property.
     * 
     */
    public int getSunJouleUsageThisMonth() {
        return sunJouleUsageThisMonth;
    }

    /**
     * Sets the value of the sunJouleUsageThisMonth property.
     * 
     */
    public void setSunJouleUsageThisMonth(int value) {
        this.sunJouleUsageThisMonth = value;
    }

    /**
     * Gets the value of the sunJouleUsageThisYear property.
     * 
     */
    public int getSunJouleUsageThisYear() {
        return sunJouleUsageThisYear;
    }

    /**
     * Sets the value of the sunJouleUsageThisYear property.
     * 
     */
    public void setSunJouleUsageThisYear(int value) {
        this.sunJouleUsageThisYear = value;
    }

    /**
     * Gets the value of the sunJouleUsageAllTime property.
     * 
     */
    public int getSunJouleUsageAllTime() {
        return sunJouleUsageAllTime;
    }

    /**
     * Sets the value of the sunJouleUsageAllTime property.
     * 
     */
    public void setSunJouleUsageAllTime(int value) {
        this.sunJouleUsageAllTime = value;
    }

    /**
     * Gets the value of the productionSite property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productionSite property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductionSite().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationSummary }
     * 
     * 
     */
    public List<LocationSummary> getProductionSite() {
        if (productionSite == null) {
            productionSite = new ArrayList<LocationSummary>();
        }
        return this.productionSite;
    }

    /**
     * Gets the value of the usageMethod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the usageMethod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUsageMethod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SunJouleUsageMethod }
     * 
     * 
     */
    public List<SunJouleUsageMethod> getUsageMethod() {
        if (usageMethod == null) {
            usageMethod = new ArrayList<SunJouleUsageMethod>();
        }
        return this.usageMethod;
    }

    /**
     * Gets the value of the friend property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the friend property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFriend().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VeeyahFriend }
     * 
     * 
     */
    public List<VeeyahFriend> getFriend() {
        if (friend == null) {
            friend = new ArrayList<VeeyahFriend>();
        }
        return this.friend;
    }

    /**
     * Gets the value of the news property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the news property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNews().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getNews() {
        if (news == null) {
            news = new ArrayList<String>();
        }
        return this.news;
    }

}
