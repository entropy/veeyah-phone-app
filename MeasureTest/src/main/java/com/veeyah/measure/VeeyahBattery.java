package com.veeyah.measure;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.RemoteViews;

import com.veeyah.matchbox.GetSunJoulesResponse;
import com.veeyah.matchbox.MySummaryResponse;
import com.veeyah.measure.manager.DeviceMetricsManager;
import com.veeyah.measure.manager.GetSunJoulesManager;
import com.veeyah.measure.manager.GetVideoURLManager;
import com.veeyah.measure.manager.RegisterSunPortManager;
import com.veeyah.measure.manager.StatusUpdateManager;
import com.veeyah.measure.manager.VeeyahGUI;
import com.veeyah.measure.manager.VideoFileManager;
import com.veeyah.measure.manager.WebServiceManager;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

public class VeeyahBattery extends Service implements VeeyahGUI {
    private final String TAG = "[ Veeyah Battery "+VeeyahConstants.VERSION_NUMBER+"]";
    VeeyahBattery self = this;
	//private MatchBoxService_Impl mbs;
	private MatchBoxModel mbm;
    private VideoFileManager vfm;
    //private DownloadManager dm;
	protected static LocalBroadcastManager lbm;
	private final IBinder mBinder = new VeeyahBinder();
    private final WebServiceManager wsm = new WebServiceManager();
	//public final static String GET_USER_INFO = "com.veeyah.GetUserInfo";
	public final static String REFRESH = "com.veeyah.Refresh";
    public final static String NOTIFICATION = "com.veeyah.Notification";
    public final static String CONNECT_BLE = "com.veeyah.ConnectBLE";
    public final static String REFRESH_BLE = "com.veeyah.RefreshBLE";
    public final static String SCAN_BLE = "com.veeyah.ScanBLE";
    public final static String FILL_BLE = "com.veeyah.FillBLE";
    public final static String BLE_ADDRESS = "bleAddr";
    public final static String UNIT_ID = "unitID";
	//public final static String REGISTER = "com.veeyah.Register";
    //public final static String RELOAD = "com.veeyah.Reload";
	//public final static String MB_MODEL = "mbm";
    public final static String NEARBY_PLUGS = "closePlugs";
    public final static String CURRENT_SUNPORT = "curPort";
    public final static String PROGRESS_MSG = "progMsg";
	//public final static String USER_NAME = "name";
	//public final static String USER_EMAIL = "email";
	//public final static String PASSWORD = "pass";
	public final static int NOTE_ID = 11101;
    public final static long METRIC_REPORT_FREQUENCY = 1440;  //1440 min = 24 hours
    public final static int PLUG_CAPACITY = 50000;

    private final String PLUG_SERVICE_UUID = "0000fff0-0000-1000-8000-00805f9b34fb";
    private final String WRITER_GCUUID = "0000fff3-0000-1000-8000-00805f9b34fb";
    private final String NOTIFY_GCUUID = "0000fff4-0000-1000-8000-00805f9b34fb";
    private final String NOTIFY_DESCRIPTOR_GCUUID = "00002902-0000-1000-8000-00805f9b34fb";

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private final static int SCAN_PERIOD = 10000;
    //private final int MAX_METER_NUM = 10;
    public final static long SUNPORT_HOLD_TIME = 300000; //5 min in ms
    private boolean bleScanInProgress = false;
    SunPort userPort = null;
    private HashMap<String, SunPort> ports = new HashMap<String, SunPort>();
    private boolean registrationScan = false;
    //private ArrayList<SunPort> meters = new ArrayList<SunPort>();
    //private HashSet<String> spAddrs = new HashSet<String>();

	public VeeyahBattery() {
		super();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
        mbm = new MatchBoxModel();
        final Intent innerIntent = intent;
        Thread worker = new Thread() {
            @Override
            public void run() {
                if (REFRESH.equals(innerIntent.getAction())) {
                    VeeyahLogger.i(TAG, "onStartCommand handles GUI refresh");
                    refreshActivity();
                    SunPort sp = innerIntent.getParcelableExtra(CURRENT_SUNPORT);
                    if (sp != null) {
                        sp = ports.get(sp.getRawAddress());
                        if (SunPort.CONNECTED != sp.getConnectionState()) {
                            connect(sp);
                        } else {
                            VeeyahLogger.i(TAG, "request energy on GUI refresh");
                            getEnergy(sp);
                            getPower(sp);
                        }
                    }
                    ArrayList<String> registeredPorts = mbm.getRegisteredPorts();
                    if (!registeredPorts.isEmpty()) {
                        Iterator<String> portIter = registeredPorts.iterator();
                        while (portIter.hasNext()) {
                            String rawAddr = portIter.next();
                            if (ports.containsKey(rawAddr)) {
                                refreshBLE(ports.get(rawAddr));
                                break;
                            }
                        }
                    }
                } else if (Intent.ACTION_BOOT_COMPLETED.equals(innerIntent.getAction())) {
                    VeeyahLogger.i(TAG, "Started after reboot");
                } else if (CONNECT_BLE.equals(innerIntent.getAction())) {
                    String spAddress = innerIntent.getStringExtra(BLE_ADDRESS);
                    if ("".equals(spAddress) || spAddress == null) {
                        VeeyahLogger.i(TAG, "Invalid BLE device location.");
                    } else {
                        userPort = ports.get(spAddress.replace(":", ""));
                        if (userPort != null) {
                            VeeyahLogger.i(TAG, "Update BLE GUI (energy=" + userPort.getEnergy() + ", power=" + userPort.getPower() + ")");
                            Intent bleData = new Intent(self, Home_View.class);
                            bleData.setAction(CONNECT_BLE);
                            bleData.putExtra(CURRENT_SUNPORT, userPort);
                            if (lbm != null) lbm.sendBroadcast(bleData);
                        } else {
                            Intent bleData = new Intent(self, Home_View.class);
                            bleData.setAction(CONNECT_BLE);
                            if (lbm != null) lbm.sendBroadcast(bleData);
                        }
                    }
                } else if (SCAN_BLE.equals(innerIntent.getAction())) {
                    try {
                        String unitID = innerIntent.getStringExtra(UNIT_ID);
                        if (!"".equals(unitID) && unitID != null) {
                            registrationScan = true;
                            Iterator<String> plugAddrs = ports.keySet().iterator();
                            while (plugAddrs.hasNext()) {
                                String addr = plugAddrs.next();
                                if (addr.contains(unitID.toUpperCase())) {
                                    registerDevice(ports.get(addr.replace(":", "")));
                                    return;
                                }
                            }
                        }
                        VeeyahLogger.i(TAG, "GUI requested scan");
                        if (ports.isEmpty()) {
                            scanLeDevice();
                        } else {
                            Iterator<SunPort> spIter = ports.values().iterator();
                            ArrayList<SunPort> connectedPlugs = new ArrayList<SunPort>();
                            while (spIter.hasNext()) {
                                SunPort sp = spIter.next();
                                if (sp.getConnectionState() == SunPort.CONNECTED) {
                                    connectedPlugs.add(sp);
                                } else {
                                    VeeyahLogger.i(TAG, "Plug "+sp.getAddress()+" in state "+sp.getConnectionState());
                                }
                            }

                            Intent detectBLE = new Intent(self, Home_View.class);
                            detectBLE.setAction(SCAN_BLE);
                            VeeyahLogger.i(TAG, "Connected plugs = "+connectedPlugs.size());
                            SunPort[] portArray = new SunPort[connectedPlugs.size()];
                            detectBLE.putExtra(NEARBY_PLUGS, connectedPlugs.toArray(portArray));
                            if (lbm == null) VeeyahLogger.i(TAG, "broadcaster is null!");
                            if (lbm != null) {
                                lbm.sendBroadcast(detectBLE);
                                VeeyahLogger.i(TAG, "scan response broadcast sent");
                            }
                            scanLeDevice();
                            /*Thread.sleep(SUNPORT_HOLD_TIME);
                            Iterator<SunPort> connectIter = connectedPlugs.iterator();
                            while (connectIter.hasNext()) {
                                SunPort sp = connectIter.next();
                                if (sp.getLatestTime() < (System.currentTimeMillis() - SUNPORT_HOLD_TIME)) {
                                    getEnergy(sp);
                                    getPower(sp);
                                }
                            }*/
                        }
                        /*if (!registrationScan) {  //TODO: handle registration as a separate intent
                            Intent detectBLE = new Intent(self, Home_View.class);
                            detectBLE.setAction(SCAN_BLE);
                            ArrayList<SunPort> connectedPlugs = getConnectedPlugs();
                            VeeyahLogger.i(TAG, "Conencted plugs = "+connectedPlugs.size());
                            detectBLE.putParcelableArrayListExtra(NEARBY_PLUGS, connectedPlugs);
                            if (lbm == null) VeeyahLogger.w(TAG, "broadcaster is null!");
                            if (lbm != null) {
                                lbm.sendBroadcast(detectBLE);
                                VeeyahLogger.i(TAG, "scan response broadcast sent");
                            }
                        }*/
                    } catch (Exception e) {
                        VeeyahLogger.w(TAG, "Exception reporting BLE scan results to GUI: "+e.getClass()+": "+e.getMessage());
                    }
                } else if (FILL_BLE.equals(innerIntent.getAction())) {
                    try {
                        //VeeyahLogger.i(TAG, "Fill BLE");
                        SunPort toFill = innerIntent.getParcelableExtra(CURRENT_SUNPORT);
                        //if (userPort == null) VeeyahLogger.i(TAG, "User Sunport is null on Fill request.");
                        if (toFill != null) {
                            String addr = toFill.getRawAddress();
                            toFill = ports.get(addr);
                            if (toFill == null) throw new ViaSolarException("Userport not found by background service for refill operation on port "+addr+".");
                            if (toFill.getConnectionState() != SunPort.CONNECTED) VeeyahLogger.i(TAG, "SunPort "+addr+" is not connected for refill operation.");
                            fillSunPort(toFill);
                        } else {
                            VeeyahLogger.i(TAG, "plug argument is null on refill operation");
                        }
                    } catch (Exception e) {
                        VeeyahLogger.w(TAG, "Exception handling refill request: "+e.getClass()+": "+e.getMessage());
                    }
                }
            }
        };
        worker.start();

		return START_NOT_STICKY;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		init();
	}

    @Override
    public void onDestroy() {
        VeeyahLogger.i(TAG, "onDestory called");
        //disconnectAll();
        super.onDestroy();
    }

	protected static void registerLocalBroadcast(BroadcastReceiver br,
			Context myContext) {
		if (lbm == null)
			lbm = LocalBroadcastManager.getInstance(myContext);
		lbm.registerReceiver(br, getIntentFilter());
	}

	protected static void unregister(BroadcastReceiver br) {
		if (lbm != null) {
			lbm.unregisterReceiver(br);
		}
	}

	private void init() {
		try {
            VeeyahLogger.i(TAG, "init");
			IntentFilter batteryLevelFilter = new IntentFilter(
					Intent.ACTION_BATTERY_CHANGED);
            //disconnectAll();

			BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent intent) {
                    final Intent innerIntent = intent;
                    Thread worker = new Thread() {
                      @Override
                      public void run() {
                          //VeeyahLogger.i(TAG+"("+Thread.currentThread().getName()+")", "worker starts");
                          processBatteryInfo(innerIntent);
                          fillSunJouleCache();
                          fillVideoCache();
                          handleMetricReport();
                          //checkForDisconnects();
                          //scanLeDevice();
                          //VeeyahLogger.i(TAG+"("+Thread.currentThread().getName()+")", "worker ends");
                      }
                    };
                    worker.getName();
                    worker.start();
				}
			};
			registerReceiver(batteryLevelReceiver, batteryLevelFilter);

            IntentFilter refreshFilter = new IntentFilter(REFRESH);
            BroadcastReceiver refreshReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    final Intent innerIntent = intent;
                    Thread worker = new Thread() {
                        @Override
                        public void run() {
                            VeeyahLogger.i(TAG, "separate receiver handles GUI refresh");
                            refreshActivity();
                            SunPort sp = innerIntent.getParcelableExtra(CURRENT_SUNPORT);
                            if (sp != null) {
                                sp = ports.get(sp.getRawAddress());
                                if (SunPort.CONNECTED != sp.getConnectionState()) {
                                    connect(sp);
                                } else {
                                    VeeyahLogger.i(TAG, "get sunport power and energy on separate refresh");
                                    getEnergy(sp);
                                    getPower(sp);
                                }
                            }
                            ArrayList<String> registeredPorts = mbm.getRegisteredPorts();
                            if (!registeredPorts.isEmpty()) {
                                Iterator<String> portIter = registeredPorts.iterator();
                                while (portIter.hasNext()) {
                                    String rawPort = portIter.next();
                                    if (ports.containsKey(rawPort)) {
                                        refreshBLE(ports.get(rawPort));
                                        break;
                                    }
                                }
                            }
                        }
                    };
                    worker.start();
                }
            };
            registerReceiver(refreshReceiver, refreshFilter);

            /*dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                        DownloadManager.Query query = new DownloadManager.Query();
                        query.setFilterById(vfm.getEnqueue());
                        Cursor c = dm.query(query);
                        if (c.moveToFirst()) {
                            int columnIndex = c
                                    .getColumnIndex(DownloadManager.COLUMN_STATUS);
                            if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                                String tempFile = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                                tempFile += "/"+c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME));
                                try {
                                    FileInputStream inputStream = new FileInputStream(new File(tempFile));
                                    VeeyahLogger.w(TAG, "File size: "+inputStream.getChannel().size());
                                    FileOutputStream outputStream = openFileOutput(vfm.getCurrentFileName(), Context.MODE_PRIVATE);
                                    byte[] buffer = new byte[1024];
                                    int read;
                                    while ((read = inputStream.read(buffer)) != -1) {
                                        outputStream.write(buffer, 0, read);
                                    }
                                    outputStream.close();
                                    inputStream.close();
                                    vfm.downloadSuccessful();
                                } catch (Exception e) {
                                    VeeyahLogger.w(TAG, "Exception copying file into local storage:"+e.getClass()+": "+e.getMessage());
                                }
                            }
                        }
                    }
                }
            };
            registerReceiver(downloadReceiver, new IntentFilter(
                    DownloadManager.ACTION_DOWNLOAD_COMPLETE));*/
            vfm = new VideoFileManager();

            // For API level 18 and above, get a reference to BluetoothAdapter through
            // BluetoothManager.
            if (mBluetoothManager == null) {
                mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
                if (mBluetoothManager == null) {
                    VeeyahLogger.e(TAG, "Unable to initialize BluetoothManager.");
                }
            }

            mBluetoothAdapter = mBluetoothManager.getAdapter();
            if (mBluetoothAdapter == null) {
                VeeyahLogger.e(TAG, "Unable to obtain a BluetoothAdapter.");
            } else {
                //registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
                VeeyahLogger.i(TAG, "Initial scan");
                scanLeDevice();
            }


		} catch (Exception e) {
			VeeyahLogger.w(TAG,
					"Exception initializing VeeyahBattery: " + e.getClass()
							+ ": " + e.getMessage());
		}
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	protected Notification getServiceNotification(MatchBoxModel mbm) {
		VeeyahLogger.i(TAG, "Configuring notification (sticky="+mbm.isStickyNotification()+")...");
        //NotificationManager nman = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //nman.cancelAll();
        //nman.cancel(VeeyahBattery.NOTE_ID);
        stopForeground(true);
        //VeeyahLogger.i(TAG, "Remove old notifications");
		
		//sh = PreferenceManager.getDefaultSharedPreferences(this);
		long sj = mbm.getAvailableSJ();

		//boolean hasSJ = (sj > 0);
		//if (hasSJ)
		//	cStr = "You've got solar!";

		// Start with red and set other values if green/yellow levels are reached
		String cStr = "I am not solar.  Get more now!";
		int notificationIcon = R.drawable.sj_red;
		int priority = Notification.PRIORITY_DEFAULT;
        if (!mbm.isStickyNotification()) priority = Notification.PRIORITY_MIN;
		if (sj > 2500) {
			// Green level
			cStr = "I am SOLAR!";
			notificationIcon = R.drawable.sj_green;
            if (!mbm.isStickyNotification()) {
                return(null);
            }
		} else if (1000 < sj && sj <= 2500) {
			// Yellow level
			cStr = "I am SOLAR, but I am starting to run out.";
			notificationIcon = R.drawable.sj_yellow;
			priority = Notification.PRIORITY_DEFAULT;
		} else if (sj > 99 && sj <= 1000) {
            cStr = "I am SOLAR, but I need more SunJoules soon!";
            priority = Notification.PRIORITY_DEFAULT;
        }

		Intent notificationIntent = new Intent(this, Home_View.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED | Intent.FLAG_ACTIVITY_NEW_TASK);
        notificationIntent.putExtra(NOTIFICATION, NOTE_ID);
        notificationIntent.setType(NOTIFICATION);

		PendingIntent pi = PendingIntent.getActivity(ViaSolar.getContext(), NOTE_ID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		String title = getString(R.string.app_name);
		String ticker = "Solar Status";

        // Expandable notification for >= JellyBean devices
        RemoteViews customNotificationView = new RemoteViews(getPackageName(), R.layout.custom_notification);
        customNotificationView.setImageViewResource(R.id.notifyMainIconImageView, notificationIcon);
        customNotificationView.setTextViewText(R.id.notifyTitleTextView, title);
        customNotificationView.setTextViewText(R.id.notifyTextTextView, cStr);

        Notification notification = (new Notification.Builder(this).setTicker(ticker)
                .setSmallIcon(notificationIcon)
                .setContentTitle(title)
                .setContentText(cStr)
                .setContentIntent(pi)
                .setAutoCancel(false)
                .setPriority(priority)
                .build());
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        if (mbm.isStickyNotification()) notification.flags |= Notification.FLAG_NO_CLEAR;
        notification.bigContentView = customNotificationView;

        return notification;

	}

    private void fillSunPort(SunPort sp) {
        try {
            String deviceID = sp.getDeviceID();
            if (deviceID == null || "".equals(deviceID)) {
                RegisterSunPortManager rspm = new RegisterSunPortManager(sp);
                rspm.registerSunPort(sp.getRawAddress(), wsm, this);
                VeeyahLogger.i(TAG, "Register SunPort '"+sp.getAddress()+"'.");
                Intent updateFill = new Intent(self, Home_View.class);
                updateFill.setAction(FILL_BLE);
                if (lbm != null) {
                    updateFill.putExtra(PROGRESS_MSG, "Contacting CloudSolar...");
                    lbm.sendBroadcast(updateFill);
                } else {
                    VeeyahLogger.w(TAG, "Local broadcast system is null!");
                }
                return;
            }
            GetSunJoulesManager getSJ = new GetSunJoulesManager(sp);
            int sj = sp.getEnergy();
            if (sj < 180000) {
                getSJ.getSunJoules(sp.getDeviceID(), (180000 - sj), new WebServiceManager(), this, false);
                VeeyahLogger.i(TAG, "Request SunJoules for SunPort '"+sp.getAddress()+"'.");
                Intent updateFill = new Intent(self, Home_View.class);
                updateFill.setAction(FILL_BLE);
                if (lbm != null) {
                    updateFill.putExtra(PROGRESS_MSG, "Requesting SunJoules...");
                    lbm.sendBroadcast(updateFill);
                } else {
                    VeeyahLogger.w(TAG, "Local broadcast system is null!");
                }
            } else {
                VeeyahLogger.i(TAG, "SunPort '"+sp.getAddress()+"' is full.");
                Intent updateFill = new Intent(self, Home_View.class);
                updateFill.setAction(FILL_BLE);
                if (lbm != null) {
                    updateFill.putExtra(CURRENT_SUNPORT, sp);
                    lbm.sendBroadcast(updateFill);
                } else {
                    VeeyahLogger.w(TAG, "Local broadcast system is null!");
                }
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception getting SunJoules for SunPort '"+sp.getAddress()+"': "+e.getClass()+": "+e.getMessage());
        }
    }

    private void fillSunJouleCache() {
        try {
            GetSunJoulesManager getSJ = new GetSunJoulesManager();
            long sj = mbm.getCachedSJ();
            if (sj < 10000 && !VeeyahConstants.DEFAULT_DEVICE_ID.equals(mbm.getDeviceID())) {
                getSJ.getSunJoules(mbm.getDeviceID(), (int) (100 - sj / 100), wsm, this, true);
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception refilling sunjoule cache: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void handleMetricReport() {
        try {
            long waitTime = mbm.getLastMetricReportTime();
            if (waitTime > METRIC_REPORT_FREQUENCY) {
                mbm.setMetricReportTime(0);
                DeviceMetricsManager dmm = new DeviceMetricsManager();
                dmm.sendMetricsReport(wsm, vfm);
                updateHomeMessage();
            } else {
                mbm.setMetricReportTime(waitTime+mbm.getMinSinceLastTime());
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception handling metric report: "+e.getClass()+": "+e.getMessage());
        }
    }

    public void updateHomeMessage() {
        try {
            //MySummaryManager mySummaryManager = new MySummaryManager();
            //mySummaryManager.getUserSummary(mbm.getDeviceID(), wsm, this);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception updaing home message: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void fillVideoCache() {
        try {
            if (!vfm.isWaiting() && (vfm.countUnwatchedVideos() < vfm.MIN_VID_STORAGE)) {
                vfm.startWaiting();
                //VeeyahLogger.i("Veeyah video cache status: ", vfm.toString());
                VeeyahLogger.i(TAG, "Fill video cache");
                GetVideoURLManager gvum = new GetVideoURLManager();
                gvum.getVideoURL(mbm.getDeviceID(), wsm, this);
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception filling video cache: "+e.getClass()+": "+e.getMessage());
        }
    }

    public void refreshView(Object data) {
        try {
            if (data == null) return;
            if (data.getClass().equals(URL.class)) {
                if (vfm == null) vfm = new VideoFileManager();
                VeeyahLogger.i(TAG, "Download video: "+data.toString());
                if (!vfm.isWaiting()) {
                    vfm.startWaiting();
                }
                vfm.downloadNewVideo((URL) data);
            } else if (GetSunJoulesResponse.class.equals(data.getClass())) {
                VeeyahLogger.i(TAG, "Refill SunJoule cache operation complete.");
            } else if (MySummaryResponse.class.equals(data.getClass())) {
                MySummaryResponse msr = (MySummaryResponse) data;
                if (msr.getNews() != null && !msr.getNews().isEmpty()) {
                    String news = msr.getNews().get(0);
                    mbm.setLastHomeMsg(news);
                }
            } else if (SunPort.class.equals(data.getClass())) {
                SunPort regSp = (SunPort) data;
                SunPort serviceCopy = ports.get(regSp.getRawAddress());
                if (serviceCopy == null) {
                    throw new ViaSolarException("Registered an unknown SunPort: "+regSp.getAddress());
                }

                if (regSp.getPendingFill() > 0) {
                    if (serviceCopy.getConnectionState() == SunPort.CONNECTED) {
                        Intent updateFill = new Intent(self, Home_View.class);
                        updateFill.setAction(FILL_BLE);
                        if (lbm != null) {
                            updateFill.putExtra(PROGRESS_MSG, "Reloading Sunport...");
                            lbm.sendBroadcast(updateFill);
                        } else {
                            VeeyahLogger.w(TAG, "Local broadcast system is null!");
                        }
                        setEnergy(serviceCopy, (serviceCopy.getEnergy()+regSp.getPendingFill())*10/36);
                        VeeyahLogger.i(TAG, "refilled plug: " +serviceCopy.getAddress());
                        return;
                    }
                    throw new ViaSolarException("Tried to refill a disconnected plug '"+serviceCopy.getAddress()+"'!");
                }

                serviceCopy.setDeviceID(regSp.getDeviceID());
                if (serviceCopy.getConnectionState() == SunPort.CONNECTED) {
                    fillSunPort(serviceCopy);
                }
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception refreshing view: "+e.getClass()+": "+e.getMessage());
        }

    }

    public void showError(String msg) {
        VeeyahLogger.i(TAG, "Webservice Error reported to VeeyahBattery: "+msg);
        vfm.stopWaiting();
    }

	public void refreshActivity() {
		//MatchBoxJSONSerializer.saveMbInfo(mbm, this);
        //if (mbm == null) loadData();
        Intent refresh = new Intent(this, Home_View.class);
        refresh.setAction(REFRESH);
        if (lbm != null) lbm.sendBroadcast(refresh);
        Notification n = getServiceNotification(mbm);
        if (n == null) return;

		startForeground(NOTE_ID, n);
	}

	protected void processBatteryInfo(Intent i) {
		try {
            long minSinceUpdate = mbm.getMinSinceLastTime();

			int curPoint = i.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
			long lastPoint = mbm.getLastPoint();

			int batteryStatus = i.getIntExtra(BatteryManager.EXTRA_STATUS, 0);

			if (lastPoint <= 0)
				lastPoint = curPoint;
			long delta = lastPoint - curPoint;

            long newBurn = mbm.getUsedSJ();
			checkSunJoules(delta, mbm.getCapacity(), (batteryStatus == BatteryManager.BATTERY_STATUS_FULL));
            if (delta > 0) {
                mbm.setLastTime(mbm.printTime());
                newBurn = mbm.getUsedSJ() - newBurn;
                StatusUpdateManager.sendStatusUpdate(wsm, mbm, minSinceUpdate, (int) delta, (int) (newBurn/100));
            }
			mbm.setlastPoint(curPoint);
		} catch (Exception e) {
			VeeyahLogger.w(TAG,
					"Exception processing battery info: " + e.getClass() + ": "
							+ e.getMessage());
		}
		refreshActivity();
	}

	public static IntentFilter getIntentFilter() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(REFRESH);
        filter.addAction(SCAN_BLE);
        filter.addAction(CONNECT_BLE);
        filter.addAction(REFRESH_BLE);
        filter.addAction(FILL_BLE);
		return (filter);
	}

	private boolean burnSunJoules(long l) { // long l describes sunjoules in hundredths

		long sj = mbm.getAvailableSJ();
		long usedSJ = mbm.getUsedSJ();

		if (l > sj) {
			mbm.setUsedSJ(usedSJ + sj);
			mbm.setAvailableSJ(0);
			return (false);
		}
		mbm.setUsedSJ(usedSJ + l);
		mbm.setAvailableSJ(sj - l);
		return (true);
	}

	protected boolean checkSunJoules(long delta, int cap, boolean full) {
		// note delta will be negative whenever a charge occurs and positive
		// whenever a discharge occurs
		// delta=1;
		//sh = PreferenceManager.getDefaultSharedPreferences(this);
		//connectionDetector = new ConnectionDetector(ViaSolar.getContext());

		long sj = mbm.getAvailableSJ();

		// Editor e1 = sh.edit();
		// e1.putInt("avaiSJ", getSJ+ (sj * 100));
		// e1.commit();
		// long sj = mbm.getAvailableSJ();
		if (delta != 0 || !full)
			mbm.setIdleTime(0);
		if (sj <= 0) { // no sun joules! try to get some, increment stats,
						// return false
            /*if (sj == 0) {
                Toast.makeText(
                        ViaSolar.getContext(),
                        "Your phone has run out of solar energy!"
                                + "  Please re-connect to a network so Veeyah can retrieve more solar energy.",
                        Toast.LENGTH_LONG).show();
            }*/

			if (delta > 0) {
				mbm.incTotDischarge((long) Math.ceil(delta * cap * 3.6));
			} else if (delta < 0) {
				mbm.incTotCharge((long) (-1 * Math.floor(delta * cap * 3.6)));
			}
			return (false);
		}
		if (delta > 0) { // battery level decreased, update stats, burn
							// sunjoules
			long toBurn = (long) Math.ceil(delta * cap * 3.6);
			mbm.incTotDischarge(toBurn);
			if (burnSunJoules(toBurn)) return (true);
		} else if (delta < 0) { // battery level increased, update stats,
								// try to get more sunjoules
			long toBurn = (long) Math.floor(-1 * delta * cap * 3.6);
			mbm.incTotCharge(toBurn);

            //TODO: auto SunJoule refill was removed for MVP ad-based user experience, it should be re-enabled for paid accounts
			/*if (sj < 1000) {

				if (connectionDetector.isConnectingToInternet()) {
					mbs.getSunJoules((int) Math.ceil(100 - sj / 100), this, deviceManager.getDeviceId());
				} else {
					if (sj == 0) {
						Toast.makeText(
								LoginScreen.APPLICATION_CONTEXT,
								"Your phone has run out of solar energy!"
										+ "  Please re-connect to a network so Veeyah can retrieve more solar energy.",
								Toast.LENGTH_LONG).show();
					} else {
						Toast.makeText(
								LoginScreen.APPLICATION_CONTEXT,
								"Veeyah needs a data connection to retrieve more solar energy, but your phone is still solar powered with "
										+ dispLong(sj)
										+ " SunJoules stored in this phone.",
								Toast.LENGTH_LONG).show();
					}
				}

			}*/
		} else { // battery level constant
			if (!full)
				return (true);
            long idleTime = mbm.getIdleTime();
			if (idleTime == 0) { // the first time this has
											// happened, note the time
				mbm.setIdleTime(System.currentTimeMillis());
				return (true);
			}
			long d = System.currentTimeMillis() - idleTime;
			if (d > 60 * 60 * 1000) { // the level has not changed in an
										// hour, reset the timer and burn a
										// sunjoule
				mbm.setIdleTime(System.currentTimeMillis());
				return (burnSunJoules(100));
			} else {
				return (true);
			}
		}

		return (false);
	}

	public class VeeyahBinder extends Binder {
		/*
		 * VeeyahBattery getService() { return VeeyahBattery.this; }
		 */
	}

	@Override
	public IBinder onBind(Intent bindIntent) {
		return (mBinder);
	}

	// this commented section notes a number of device-dependant places where
	// battery info may reside
	/*
	 * private String readCurrent() { File f;
	 *
	 * f = new File("/sys/class/power_supply/usb/current_now"); if (f.exists())
	 * return readLine(f);
	 *
	 * f = new File("/sys/class/power_supply/ac/current_now"); if (f.exists())
	 * return readLine(f);
	 *
	 * f = new File("/sys/class/power_supply/battery/current_now"); if
	 * (f.exists()) return readLine(f);
	 *
	 * // htc desire hd / desire z / inspire? if
	 * (Build.MODEL.toLowerCase().contains("desire hd") ||
	 * Build.MODEL.toLowerCase().contains("desire z") ||
	 * Build.MODEL.toLowerCase().contains("inspire")) {
	 *
	 * f = new File("/sys/class/power_supply/battery/batt_current"); if
	 * (f.exists()) { return readLine(f); } }
	 *
	 * // nexus one cyangoenmod f = new
	 * File("/sys/devices/platform/ds2784-battery/getcurrent"); if (f.exists())
	 * { return readLine(f); }
	 *
	 * // sony ericsson xperia x1 f = new File(
	 * "/sys/devices/platform/i2c-adapter/i2c-0/0-0036/power_supply/ds2746-battery/current_now"
	 * ); if (f.exists()) { return readLine(f); }
	 *
	 * // xdandroid //if (Build.MODEL.equalsIgnoreCase("MSM")) { f = new File(
	 * "/sys/devices/platform/i2c-adapter/i2c-0/0-0036/power_supply/battery/current_now"
	 * ); if (f.exists()) { return readLine(f); } //}
	 *
	 * // droid eris f = new File("/sys/class/power_supply/battery/smem_text");
	 * if (f.exists()) { return readLine(f); }
	 *
	 * // htc sensation / evo 3d f = new
	 * File("/sys/class/power_supply/battery/batt_attr_text"); if (f.exists()) {
	 * return readLine(f); }
	 *
	 * // some htc devices f = new
	 * File("/sys/class/power_supply/battery/batt_current"); if (f.exists())
	 * return readLine(f);
	 *
	 * // nexus one f = new File("/sys/class/power_supply/battery/current_now");
	 * if (f.exists()) return readLine(f);
	 *
	 * // samsung galaxy vibrant f = new
	 * File("/sys/class/power_supply/battery/batt_chg_current"); if (f.exists())
	 * return readLine(f);
	 *
	 * // sony ericsson x10 f = new
	 * File("/sys/class/power_supply/battery/charger_current"); if (f.exists())
	 * return readLine(f);
	 *
	 * // Nook Color f = new
	 * File("/sys/class/power_supply/max17042-0/current_now"); if (f.exists())
	 * return readLine(f);
	 *
	 * return("0.0"); }
	 */

    //TODO: begin BLE service

    //private BluetoothGattCharacteristic mGattNotify;

    //  private int tag = 0;
    //   private String mBluetoothDeviceAddress;
//    private ArrayList<BluetoothGatt>  mBluetoothGatts= new ArrayList<BluetoothGatt>();
//    private BluetoothGatt mBluetoothGatt;
    //   private int mConnectionState = STATE_DISCONNECTED;
    /*private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_GATT_CONNECTED);
        intentFilter.addAction(ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(ACTION_DATA_AVAILABLE);
        intentFilter.addAction(ACTION_DATA_NAME);
        intentFilter.addAction(ACTION_DATA_VER);
        return intentFilter;
    }*/

    private void setEnergy(SunPort p, long energy)
    {
        try {
            VeeyahLogger.i(TAG, p.getAddress()+" set energy =" + energy + "\n");

            byte cmd[] = {0x0F, 0x07, 0x02, 0x00,
                    (byte) (energy / 0x1000000),
                    (byte) ((energy % 0x1000000) / 0x10000),
                    (byte) ((energy % 0x10000) / 0x100),
                    (byte) (energy % 0x100), 0x00, (byte) 0xFF, (byte) 0xFF
            };//new byte[17];

            int checksum = 1;
            for (int i = 2; i < 8; i++)
                checksum += cmd[i];
            cmd[8] = (byte) (checksum & 0xFF);

            //for (int i = 0; i < cmd.length; i++) {
            //    VeeyahLogger.i(TAG, "Set Energy: " + cmd[i]);
            //}
            //VeeyahLogger.i(TAG, "\n");

            p.mGattWrite.setValue(cmd);
            writeCharacteristic(p);
            Thread.sleep(100);
            getEnergy(p);
            //VeeyahLogger.i(TAG, "sent write characteristic for energy");
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception setting energy: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void toggleLight(SunPort p, boolean on) {
        try {
            byte cmd[] = {0x0F, 0x06, 0x10, 0x00, 0x01, 0%0x100, 0x00, (byte) 0xFF, (byte) 0xFF};
            if (on) {
                cmd[5] = 1%0x100;
                VeeyahLogger.i(TAG, "Turn light on\n");
            } else {
                VeeyahLogger.i(TAG, "Turn light off\n");
            }
            //p.mGattWrite.setValue(cmd);
            //writeCharacteristic(p);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception blinking: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void getEnergy(SunPort p)
    {
        try {
            if (p == null) throw new ViaSolarException("SunPort is null");
            byte cmd[] = {0x0F, 0x05, 0x03, 0x00, 0x00, 0x00, 0x04, (byte) 0xFF, (byte) 0xFF};
            p.mGattWrite.setValue(cmd);
            writeCharacteristic(p);
        } catch(Exception e) {
            VeeyahLogger.w(TAG, "Exception getting energy: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void getPower(SunPort p)
    {
        try {
            if (p == null) throw new ViaSolarException("SunPort is null");
            byte cmd[] = {0x0F, 0x05, 0x06, 0x00, 0x00, 0x00, 0x07, (byte) 0xFF, (byte) 0xFF};
            p.mGattWrite.setValue(cmd);
            writeCharacteristic(p);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception getting power: "+e.getClass()+": "+e.getMessage());
        }
    }

    public final static String ACTION_GATT_CONNECTED =
            "com.revogi.delite.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.revogi.delite.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.revogi.delite.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.revogi.delite.ACTION_DATA_AVAILABLE";
    public final static String ACTION_DATA_NAME =
            "com.revogi.delite.ACTION_DATA_NAME";
    public final static String ACTION_DATA_VER =
            "com.revogi.delite.ACTION_DATA_VER";
    public final static String EXTRA_DATA ="com.revogi.delite.EXTRA_DATA";

    private ArrayList<SunPort> getConnectedPlugs(){
        ArrayList<SunPort> output = new ArrayList<SunPort>();
        try {
            if (ports == null) {
                VeeyahLogger.i(TAG, "ports memvar is null");
                return(output);
            }
            if (mbm == null) mbm = new MatchBoxModel();
            Iterator<SunPort> allPorts = ports.values().iterator();
            while (allPorts.hasNext()) {
                SunPort sp = allPorts.next();
                if (sp != null) {
                    //VeeyahLogger.i(TAG, "check sunport: "+sp.printState());
                    if (output.contains(sp.getAddress())) continue;
                    //VeeyahLogger.i(TAG, "latest = "+sp.getLatestTime()+" v "+(System.currentTimeMillis() - SUNPORT_HOLD_TIME));
                    if (sp.getLatestTime() >= (System.currentTimeMillis() - SUNPORT_HOLD_TIME)) {
                        //VeeyahLogger.i(TAG, "it's recent data");
                        output.add(sp);
                    }
                }
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception displaying meter data: "+e.getClass()+": "+e.getMessage());
        }
        return output;
    }

    class BLETask extends AsyncTask<String, Integer, String> {

        BluetoothAdapter.LeScanCallback mLeScanCallback;

        protected BLETask(BluetoothAdapter.LeScanCallback callback) {
            this.mLeScanCallback = callback;
        }

        protected void onProgressUpdate(Integer... progress) {
            // setProgressPercent(progress[0]);
        }

        protected void onPostExecute(String result) {

        }

        protected String doInBackground(String... i) {
            mBluetoothAdapter.startLeScan(mLeScanCallback);
            VeeyahLogger.i(TAG+"Thread: "+Thread.currentThread().getName(), "BLE scan started");
            try {
                Thread.sleep(SCAN_PERIOD);
            } catch (InterruptedException ie) {
                //do nothing
            }
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            bleScanInProgress = false;
            VeeyahLogger.i(TAG+"Thread: "+Thread.currentThread().getName(), "BLE scanning finished.");
            return("");
        }
    }

    private void scanLeDevice()
    {
        try {
            if (mbm == null) mbm = new MatchBoxModel();
            //if (!registrationScan) {
            //    if (mbm.getRegisteredPorts().isEmpty()) return;
            //}
            if (bleScanInProgress) return;
            bleScanInProgress = true;
            VeeyahLogger.i(TAG, "LE Scan started");
            // Device scan callback.
            final BluetoothAdapter.LeScanCallback mLeScanCallback =
                   new BluetoothAdapter.LeScanCallback() {
                        @Override
                        public void onLeScan(final BluetoothDevice device,final int rssi, byte[] scanRecord) {

                            try {
                                if (!"SOLAR".equals(device.getName())) return;
                                VeeyahLogger.i(TAG, "name="+device.getName()+" RSSI="+rssi);

                                SunPort lt = ports.get(device.getAddress().replace(":", ""));
                                if (lt == null) {
                                    lt = new SunPort(device.getAddress());
                                    ports.put(lt.getRawAddress(), lt);
                                    VeeyahLogger.i(TAG, "Added sunport: "+rssi);
                                } else {
                                    VeeyahLogger.i(TAG, "Already knew about SunPort "+lt.getAddress());
                                }
                                if (lt.getConnectionState() != SunPort.CONNECTED || lt.mBluetoothGatt == null) {
                                    VeeyahLogger.i(TAG, "connect to "+lt.getAddress());
                                    connect(lt);
                                } else {
                                    VeeyahLogger.i(TAG, "request energy update on scan");
                                    getEnergy(lt);
                                    //getPower(lt);
                                }
                            } catch (Exception e) {
                                VeeyahLogger.i(TAG, "Exception onLeScan: "+e.getClass()+": "+e.getMessage());
                            }
                        }
                    };

            // Stops scanning after a pre-defined scan period.
            //ViaSolar.getContext().getMainLooper()
            BLETask bleTask = new BLETask(mLeScanCallback);
            //final Handler mHandler = new Handler();
            //Thread scanner = new Thread() {
            //    @Override
            //    public void run() {

            //    }
            //};
            //scanner.setPriority(Thread.MIN_PRIORITY);
            //mHandler.post(scanner);

            VeeyahLogger.i(TAG, "Begin BLE scan.");
            bleTask.doInBackground();

        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception scanning LE device: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void registerDevice(SunPort userPort) {
        try {
            VeeyahLogger.i(TAG, "Register device: "+userPort.getAddress());
            Intent bleData = new Intent(this, RegisterPort.class);
            bleData.setAction(SCAN_BLE);
            bleData.putExtra(CURRENT_SUNPORT, userPort);
            registrationScan = false;
            if (lbm == null) throw new ViaSolarException("Cannot register device! BroadcastManager is null!");
            lbm.sendBroadcast(bleData);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception refreshing BLE for registration: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void refreshBLE(SunPort userPort) {
        try {
            if (mbm == null) mbm = new MatchBoxModel();
            //VeeyahLogger.i(TAG, "Check port: "+mbm.checkPort(userPort));
            VeeyahLogger.i(TAG, "update port: "+userPort.getAddress());
            Intent bleData = new Intent(this, Home_View.class);
            bleData.setAction(REFRESH_BLE);
            bleData.putExtra(CURRENT_SUNPORT, userPort);
            if (lbm == null)
                throw new ViaSolarException("Cannot update GUI! BroadcastManager is null!");
            lbm.sendBroadcast(bleData);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception refreshing BLE: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void analyEnergy(SunPort sp, byte cmd[])//The remaining energy
    {
        try {
            int energy = (((int) (cmd[8] & 0xFF)) << 24) + (((int) (cmd[9] & 0xFF)) << 16) + (((int) (cmd[10] & 0xFF)) << 8) + (int) (cmd[11] & 0xFF);
            VeeyahLogger.i(TAG, sp.getAddress()+"has energy = " + energy + "\n");
            sp.setEnergy(energy*36/10);
            refreshBLE(sp);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception analyzing energy response: "+e.getClass()+": "+e.getMessage());
        }

        //config.PowerHandler.sendEmptyMessage(config.GET_COMMAND);

        //lastEnergy = energy;
        //updateBleGui();
    }

    private void analyPower(SunPort sp, byte cmd[])
    {
        try {
            int power = (((int) (cmd[4] & 0xFF)) << 24) + (((int) (cmd[5] & 0xFF)) << 16) + (((int) (cmd[6] & 0xFF)) << 8) + (int) (cmd[7] & 0xFF);
            VeeyahLogger.i(TAG, sp.getAddress()+"has power = " + power + "\n");
            sp.setPower(power*36/10000);
            refreshBLE(sp);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception analyzing power response: "+e.getClass()+": "+e.getMessage());
        }
    }

    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    private void displayGattServices(SunPort sp) throws ViaSolarException {
        try {
            if (sp.mBluetoothGatt == null) throw new ViaSolarException("Sunport gatt server is null!");
            if (sp.mBluetoothGatt.getServices() == null) throw new ViaSolarException("Gatt services is null!");
            //ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();

            //VeeyahLogger.i(TAG, "Number of Services: " + sp.mBluetoothGatt.getServices().size());

            BluetoothGattService plugService = sp.mBluetoothGatt.getService(UUID.fromString(PLUG_SERVICE_UUID));
            if (plugService == null)
                throw new ViaSolarException("Plug Gatt service not found for SunPort '" + sp.getAddress() + "'.");
            BluetoothGattCharacteristic writeGC = plugService.getCharacteristic(UUID.fromString(WRITER_GCUUID));
            if (writeGC == null)
                throw new ViaSolarException("Writer characteristic not found for SunPort '" + sp.getAddress() + "'.");
            sp.mGattWrite = writeGC;
            BluetoothGattCharacteristic notifyGC = plugService.getCharacteristic(UUID.fromString(NOTIFY_GCUUID));
            if (notifyGC == null)
                throw new ViaSolarException("Notify characteristic not found for SunPort '" + sp.getAddress() + "'.");
            sp.mGattNotify = notifyGC;
            sp.setConnectionState(SunPort.CONNECTED);

            //for (BluetoothGattService gattService : sp.mBluetoothGatt.getServices()) {
            //    HashMap<String, String> currentServiceData = new HashMap<String, String>();

            //    gattServiceData.add(currentServiceData);

            //    List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
            //    VeeyahLogger.i(TAG, "Service UUID: "+gattService.getUuid());
            //    BluetoothGattCharacteristic writeGC = gattService.getCharacteristic(UUID.fromString("0000fff3-0000-1000-8000-00805f9b34fb"));
            //    if (writeGC != null) {
            //        VeeyahLogger.i(TAG, "Write UUID: " + writeGC.getUuid().toString());
            //        sp.mGattWrite = writeGC;
            //    }
            //    BluetoothGattCharacteristic notifyGC = gattService.getCharacteristic(UUID.fromString("0000fff4-0000-1000-8000-00805f9b34fb"));
            //    if (notifyGC != null) {
            //        VeeyahLogger.i(TAG, "Notify UUID: " + notifyGC.getUuid().toString());
            //        mGattNotify = notifyGC;
            //    }

            //    ArrayList<BluetoothGattCharacteristic> charas = new ArrayList<BluetoothGattCharacteristic>();

            //    for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
            //        charas.add(gattCharacteristic);
            //        VeeyahLogger.i(TAG, "Characteristic: "+gattCharacteristic.getUuid());
            //        VeeyahLogger.i(TAG, "Permissions: "+gattCharacteristic.getPermissions());
            //        VeeyahLogger.i(TAG, "Properties: "+gattCharacteristic.getProperties());
            //        VeeyahLogger.i(TAG, "Permissions: "+bytesToHex(gattCharacteristic.getValue()));
            //        switch ((int) gattCharacteristic.getUuid().timestamp()) {
            //            case 0xFFF3:
            //                if (sp == null) throw new ViaSolarException("Null SunPort on display services!");
            //                sp.setConnectionState(SunPort.CONNECTED);
            //                VeeyahLogger.i(TAG, "Connected");
                            // 	System.out.printf("Get.......mGattWrites.......%d....\n",tag);
            //                VeeyahLogger.i(TAG, "Write characteristic set");
            //                sp.mGattWrite = gattCharacteristic;
            //                getEnergy(sp);
            //                getPower(sp);
            //                VeeyahLogger.i(TAG, "requested power and energy");
            //                break;
            //            case 0xFFF4:
            //                sp.mGattNotify = gattCharacteristic;
            //                VeeyahLogger.i(TAG, "Notify characteristic set");
            //                break;
            //        }
            //    }
            //}
        //} catch (ViaSolarException vse) {
        //    VeeyahLogger.w(TAG, "ViaSolarException getting plugs service: "+vse.getMessage());
        //    throw vse;
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception displaying gatt services: "+e.getClass()+": "+e.getMessage());
            disconnect(sp);
        }
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public final static String bytesToHex(byte[] bytes) {
        try {
            if (bytes == null) return "null";
            if (bytes.length < 1) return "";
            char[] hexChars = new char[bytes.length * 2];
            for (int j = 0; j < bytes.length; j++) {
                int v = bytes[j] & 0xFF;
                hexChars[j * 2] = hexArray[v >>> 4];
                hexChars[j * 2 + 1] = hexArray[v & 0x0F];
            }
            return new String(hexChars);
        } catch (Exception e) {
            return("Exception parsing byte array: "+e.getClass()+": "+e.getMessage());
        }
    }

    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            try {
                VeeyahLogger.i(TAG, "onConnectionStateChange..........................\n");
                String addr = gatt.getDevice().getAddress();
                SunPort sp = ports.get(addr.replace(":", ""));

                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    if (sp == null) {
                        VeeyahLogger.i(TAG, "Got Connection from unknown SunPort at address'"+addr+"'.");
                        sp = new SunPort(addr);
                        ports.put(sp.getRawAddress(), sp);
                    }
                    if (registrationScan) {
                        registerDevice(sp);
                    }
                    gatt.discoverServices();
                    VeeyahLogger.i(TAG, "request discover services");
                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    //   	System.out.println("...........STATE_DISCONNECTED");
                    //intentAction = ACTION_GATT_DISCONNECTED;
                    //    config.meters.get(tag).mConnectionState = STATE_DISCONNECTED;
                    VeeyahLogger.i(TAG, "Disconnected from GATT server.");
                    //broadcastUpdate(intentAction, tag);
                    if (sp == null) throw new ViaSolarException("Got disconnection from unknown SunPort at address'"+addr+"'.");
                    sp.setConnectionState(SunPort.DISCONNECTED);
                } else if (newState == BluetoothProfile.STATE_CONNECTING) {
                    System.out.println("...........STATE_CONNECTING");
                } else if (newState == BluetoothProfile.STATE_DISCONNECTING) {
                    System.out.println("...........STATE_DISCONNECTING");
                }
                super.onConnectionStateChange(gatt, status, newState);
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Exception onConnectionStateChange: "+e.getClass()+": "+e.getMessage());
            }

        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            try {
                VeeyahLogger.i(TAG, "onDescriptorRead");
                super.onDescriptorRead(gatt, descriptor, status);
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Exception onDescriptorRead: "+e.getClass()+": "+e.getMessage());
            }
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            try {
                VeeyahLogger.i(TAG, "onDescriptorWrite");
                //String addr = gatt.getDevice().getAddress();
                super.onDescriptorWrite(gatt, descriptor, status);
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Exception onDescriptorWrite: "+e.getClass()+": "+e.getMessage());
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            try {
                VeeyahLogger.i(TAG, "onServicesDiscovered..........................\n");
                String addr = gatt.getDevice().getAddress();
                SunPort sp = ports.get(addr.replace(":", ""));
                if (sp == null) throw new ViaSolarException("Services discovered for unknown SunPort at addr '"+addr+"'.");
                try {
                    displayGattServices(sp);
                    setCharacteristicNotification(sp, sp.mGattNotify, true);
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        //request initial energy and power
                        //setEnergy(sp, SUNPORT_CAPACITY);
                        //Thread.sleep(1000);
                        getEnergy(sp);
                        getPower(sp);
                    } else {
                        throw new ViaSolarException("onServiceDiscovered error status = " + status);
                    }
                } catch (Exception e) {
                    VeeyahLogger.i(TAG, "Exception initializing SunPort connection: "+e.getClass()+": "+e.getMessage());
                    disconnect(sp);
                }
                super.onServicesDiscovered(gatt, status);
            } catch (Exception e) {
                VeeyahLogger.i(TAG, "Exception on services discovered: "+e.getClass()+": "+e.getMessage());
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            try {
                VeeyahLogger.i(TAG, "onCharacteristicRead");
                int mode = (int) characteristic.getUuid().timestamp();
                String addr = gatt.getDevice().getAddress();
                SunPort sp = ports.get(addr.replace(":", ""));
                VeeyahLogger.i(TAG, "read [mode=" + mode + "]\n");
                if (sp == null) throw new ViaSolarException("Unkown SunPort onCharacteristic read: status = "+status);

                if (status == BluetoothGatt.GATT_SUCCESS) {
                    byte[] data = characteristic.getValue();
                    if (mode == 0x2A00)//read name
                        VeeyahLogger.i(TAG, "read name: "+data.toString());
                    else if (mode == 0xFFF1)
                        VeeyahLogger.i(TAG, "version: "+data.toString());
                    else
                        VeeyahLogger.i(TAG, "Other["+mode+"]: "+data.toString());
                }
                super.onCharacteristicRead(gatt, characteristic, status);
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Exception onCharacteristicRead: "+e.getClass()+": "+e.getMessage());
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            try {
                String addr = gatt.getDevice().getAddress();
                VeeyahLogger.i(TAG, "got sunport update:("+characteristic.getValue()[2]+") "+addr);
                SunPort sp = ports.get(addr.replace(":", ""));
                if (sp == null)
                    throw new ViaSolarException("Unkown SunPort onCharacteristic changed.");

                byte[] cmd = characteristic.getValue();
                //for (int i = 0; i < cmd.length; i++) {
                //    VeeyahLogger.i(TAG, "Gatt data: " + cmd[i]);
                //}

                if (cmd.length < 3) throw new ViaSolarException("Invalid BLE response length: "+cmd.length);

                switch (cmd[2]) //cmd
                {
                    case 2://set energy
                        //set energy complete!
                        Intent refillBLE = new Intent(self, Home_View.class);
                        refillBLE.setAction(FILL_BLE);
                        if (lbm == null)
                            VeeyahLogger.i(TAG, "broadcaster is null on confirm energy set!");
                        if (lbm != null) {
                            refillBLE.putExtra(CURRENT_SUNPORT, sp);
                            lbm.sendBroadcast(refillBLE);
                            VeeyahLogger.i(TAG, "energy set confirmation sent");
                        }
                        //config.PowerHandler.sendEmptyMessage(config.SET_COMMAND);
                        break;
                    case 3://get energy
                        analyEnergy(sp, cmd);
                        break;
                    case 6://get power
                        analyPower(sp, cmd);
                        break;
                }
                super.onCharacteristicChanged(gatt, characteristic);
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Exception on CharacteristicChanged: "+e.getClass()+": "+e.getMessage());
            }

        }
    };

    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        VeeyahLogger.i(TAG, "onUnbind called");
        //disconnectAll();
        return super.onUnbind(intent);
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param sunPort The device model of the destination device.
     *
     * @return Return true if the connection is initiated successfully. The connection result
     *         is reported asynchronously through the
     *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     *         callback.
     */
    public boolean connect(SunPort sunPort) {
        try {
            VeeyahLogger.i(TAG, "connect to SunPort: "+sunPort.getAddress());
            if (sunPort == null) {
                VeeyahLogger.w(TAG, "SunPort is null!");
            }
            if (mBluetoothAdapter == null || sunPort.getAddress() == null) {
                VeeyahLogger.w(TAG, "During connect, bluetoothAdapter not initialized or unspecified address.");
                return false;
            }

            sunPort.setConnectionState(SunPort.CONNECTING);
            if (sunPort.mBluetoothGatt != null) {
                if (sunPort.mBluetoothGatt.connect()) {
                    return true;
                } else {
                    VeeyahLogger.w(TAG, "Gatt server connection failed!");
                    return false;
                }
            } else {
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(sunPort.getAddress());
                //device.
                if (device == null) {
                    VeeyahLogger.w(TAG, "Device not found.  Unable to connect.");
                    return false;
                }
                if (mGattCallback == null) VeeyahLogger.i(TAG, "mGattCallback is null!!");
                sunPort.mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
                VeeyahLogger.i(TAG, "set SunPort gatt server");
                return true;
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception connecting to Plug: "+e.getClass()+": "+e.getMessage());
        }
        return(false);
    }

    private void checkForDisconnects() {
        try {
            long curTime = System.currentTimeMillis();
            Iterator<SunPort> spIter = ports.values().iterator();
            while (spIter.hasNext()) {
                SunPort sp = spIter.next();
                if (sp.getConnectionState() == SunPort.CONNECTED && sp.getPower() >= 0) {
                    if ((curTime - sp.getEnergyTime() > SUNPORT_HOLD_TIME) && (curTime - sp.getPowerTime() > SUNPORT_HOLD_TIME)) {
                        disconnect(sp);
                    }
                }
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception cheking SunPort for disconnect: "+e.getClass()+": "+e.getMessage());
        }
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public boolean disconnect(SunPort sunPort) {
        try {
            VeeyahLogger.i(TAG, "disconnect SunPort: "+sunPort.getAddress());
            if (sunPort == null) {
                VeeyahLogger.w(TAG, "SunPort is null!");
            }
            if (mBluetoothAdapter == null || sunPort.getAddress() == null) {
                VeeyahLogger.w(TAG, "During disconnect, bluetoothAdapter not initialized or unspecified address.");
                return false;
            }

            sunPort.setConnectionState(SunPort.CONNECTING);
            if (sunPort.mBluetoothGatt != null) {
                sunPort.mBluetoothGatt.disconnect();
                sunPort.mBluetoothGatt.close();
                sunPort.mBluetoothGatt = null;
                sunPort.setConnectionState(SunPort.DISCONNECTED);
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception disconnecting from Plug: "+e.getClass()+": "+e.getMessage());
        }
        return(false);
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnectAll() {
        try {
            if (mBluetoothAdapter == null) {
                VeeyahLogger.w(TAG, "During disconnect all, BluetoothAdapter not initialized");
                return;
            }
            Iterator<SunPort> spIter = ports.values().iterator();
            while (spIter.hasNext()) {
                SunPort sp = spIter.next();
                if (sp.mBluetoothGatt != null) {
                    if (sp.getConnectionState() == SunPort.CONNECTED || sp.getConnectionState() == SunPort.CONNECTING) {
                        sp.mBluetoothGatt.disconnect();
                        sp.setConnectionState(SunPort.DISCONNECTED);
                        sp.mBluetoothGatt.close();
                        sp.mBluetoothGatt = null;
                        //ports.remove(sp.getAddress());
                    }
                }
            }
            ports = new HashMap<String, SunPort>();
            VeeyahLogger.i(TAG, "Disconnected all ports.");
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception disconnecting all sunports: "+e.getClass()+": "+e.getMessage());
        }
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     */
    public void readCharacteristic(SunPort p) {
        if (mBluetoothAdapter == null || p.mBluetoothGatt == null) {
            VeeyahLogger.w(TAG, "During readCharacteristic, BluetoothAdapter not initialized");
            return;
        }
        p.mBluetoothGatt.readCharacteristic(p.mGattNotify);
    }

    public void writeCharacteristic(SunPort p) {
        try {
            if (mBluetoothAdapter == null || p.mBluetoothGatt == null) {
                VeeyahLogger.w(TAG, "During writeCharacteristic, BluetoothAdapter not initialized");
                return;
            }

            if (p.mGattWrite == null) VeeyahLogger.w(TAG, "write characteristic is null!");

            if (!p.mBluetoothGatt.writeCharacteristic(p.mGattWrite)) VeeyahLogger.w(TAG, "write characteristic returned false!");
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception writing characteristic: "+e.getClass()+": "+e.getMessage());
        }
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled If true, enable notification.  False otherwise.
     */
    public void setCharacteristicNotification(SunPort sp,BluetoothGattCharacteristic characteristic, boolean enabled)
    {
        try {
            if (sp == null) throw new ViaSolarException("Called setCharacteristicNotification with null SunPort!");
            if (mBluetoothAdapter == null || sp.mBluetoothGatt == null) {
                VeeyahLogger.w(TAG, "During setCharacteristicNotification, BluetoothAdapter not initialized");
                return;
            }
            sp.mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);
            VeeyahLogger.i(TAG, "Notification characteristic set");
            //BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString(NOTIFY_DESCRIPTOR_GCUUID));
            //descriptor.setValue(enabled ? BluetoothGattDescriptor.ENABLE_INDICATION_VALUE : new byte[] { 0x00, 0x00 });
            //sp.mBluetoothGatt.writeDescriptor(descriptor);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception setting characteristic notification: "+e.getClass()+": "+e.getMessage());
        }

    }

    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    /*public List<BluetoothGattService> getSupportedGattServices(SunPort sp) {
        if (sp.mBluetoothGatt == null) return null;

        return ;
    }*/
}

