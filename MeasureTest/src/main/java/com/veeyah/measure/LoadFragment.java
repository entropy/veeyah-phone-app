package com.veeyah.measure;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class LoadFragment extends Fragment {

    public boolean done = false;
    private RelativeLayout background;
    private int counter = 0;
    private BroadcastReceiver listen;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.phone_screen, container, false);
        background = (RelativeLayout) rootView.findViewById(R.id.loadingLayout);
        VeeyahLogger.v("Veeyah LoadingFragment", "Background created.");

        listen = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    done = true;
                } catch (Exception e) {
                    VeeyahLogger.w("Veeyah",
                            "Exception receiving local broadcast '"
                                    + intent.getAction()
                                    + "' in LoadingFragment: " + e.getClass()
                                    + ": " + e.getMessage()
                    );
                }
            }
        };

        VeeyahBattery.registerLocalBroadcast(listen, ViaSolar.getContext());
        VeeyahLogger.v("Veeyah", "Receiver is listening. . .");

        new LoadRunner();
        return(rootView);
    }

    public void updateBackground() {
        if (background == null) return;
        if (counter == 0) {
            background.setBackgroundResource(R.drawable.sky_bg_02);
        } else if (counter == 1) {
            background.setBackgroundResource(R.drawable.sky_bg_03);
        } else if (counter == 2) {
            background.setBackgroundResource(R.drawable.sky_bg_04);
        } else if (counter == 3) {
            background.setBackgroundResource(R.drawable.sky_bg_05);
        } else if (counter == 4) {
            background.setBackgroundResource(R.drawable.sky_bg_04);
        } else if (counter == 5) {
            background.setBackgroundResource(R.drawable.sky_bg_03);
        } else if (counter == 6) {
            background.setBackgroundResource(R.drawable.sky_bg_02);
        } else if (counter == 7) {
            background.setBackgroundResource(R.drawable.sky_bg);
        }
        counter++;
        if (counter > 7) counter = 0;
    }

    public class LoadRunner implements Runnable {

        Handler h;

        public LoadRunner() {
            h = new Handler();
            h.postDelayed(this, 500);
        }

        @Override
        public void run() {
            if (!done) {
                updateBackground();
                h.postDelayed(this, 500);
            } else {
                h.removeCallbacks(this);
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

    }

}
