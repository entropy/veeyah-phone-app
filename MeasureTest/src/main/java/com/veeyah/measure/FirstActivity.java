package com.veeyah.measure;

import java.math.BigInteger;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.veeyah.matchbox.GetSunJoulesResponse;
import com.veeyah.measure.manager.DeviceMetricsManager;
import com.veeyah.measure.manager.GetSunJoulesManager;
import com.veeyah.measure.manager.RegisterDeviceManager;
import com.veeyah.measure.manager.StatusUpdateManager;
import com.veeyah.measure.manager.VeeyahGUI;
import com.veeyah.measure.manager.WebServiceManager;
import com.veeyah.auth.RegisterDeviceResponse;
import com.veeyah.auth.RegisterType;


@SuppressLint("NewApi")
public class FirstActivity extends Activity implements VeeyahGUI {
    private final String TAG = "[ Veeyah FirstActivity "+VeeyahConstants.VERSION_NUMBER+"]";

    public MatchBoxModel mbm;
    FirstActivity self;
    private ProgressDialog progressDialog = null;
    private boolean registered = false;
    private boolean hasPort = false;
    ImageButton startBtn = null;
    ImageButton registerBtn = null;

    public void refreshView(Object data) {
        if (data == null) {
            VeeyahLogger.w(TAG, "Null data Received!");
            return;
        }

        try {
            if (GetSunJoulesResponse.class.equals(data.getClass())) {
                registered = true;
                if (progressDialog.isShowing()) {
                    if (hasPort) {
                        registerPort();
                    } else {
                        goHome();
                    }
                }
            } else if (RegisterDeviceResponse.class.equals(data.getClass())) {
                GetSunJoulesManager getSJ = new GetSunJoulesManager();
                WebServiceManager wsm = new WebServiceManager();
                getSJ.getSunJoules(mbm.getDeviceID(), 50, wsm, this, false);
                //need to send status update to get initial cumulative impact
                StatusUpdateManager.sendStatusUpdate(wsm, mbm, 0, 0, 0);
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception redirecting to Home_View: "+e.getClass()+": "+e.getMessage());
        }

    }

    public void showError(String msg) {
        //do nothing for now
        VeeyahLogger.w(TAG, "App stops due to network error: "+msg);
        showToast("This app requires network services to work properly.  Please try again from a location with a good data signal.");
        finish();
    }

    protected void onCreate(Bundle savedInstanceState) {
        try {
            self = this;
            super.onCreate(savedInstanceState);

            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.activity_intro);

            TextView introMsg = (TextView) findViewById(R.id.sjHomeMsg);
            /*introMsg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                    browserIntent.setData(Uri.parse("http://veeyah.com/terms.jsp"));
                    startActivity(browserIntent);
                }
            });*/

            mbm = new MatchBoxModel();
            String deviceID = mbm.getDeviceID();
            if ("".equals(deviceID) || VeeyahConstants.DEFAULT_DEVICE_ID.equals(deviceID) || deviceID == null) {
                //register w/o email address
                registered = false;
                mbm.incMetric(DeviceMetricsManager.BTN_REGISTER);
                RegisterDeviceManager reg = new RegisterDeviceManager();
                RegisterType deviceInfo = new RegisterType();
                TelephonyManager tm = (TelephonyManager) ViaSolar.getContext().getSystemService(TELEPHONY_SERVICE);
                deviceInfo.setName("");
                deviceInfo.setEmail("");
                String guid = tm.getDeviceId();
                if (guid == null) guid = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                if (guid == null) guid = "Null GUID";
                deviceInfo.setClientGUID(guid);
                deviceInfo.setDeviceName(android.os.Build.DEVICE);
                deviceInfo.setDeviceType(android.os.Build.MODEL);
                deviceInfo.setPassword("");
                deviceInfo.setInviteCode("0KAwbgfzG00CNk6XX02qejsf");
                deviceInfo.setBatteryCapacity(new BigInteger("" + mbm.getCapacity()));
                reg.registerDevice(deviceInfo, new WebServiceManager(), self);
            } else {
                registered = true;
            }

            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle(getString(R.string.app_name));
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);

            startBtn = (ImageButton) findViewById(R.id.startBtn);
            startBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!registered) {
                        progressDialog.show();
                    } else {
                        goHome();
                    }
                }
            });

            registerBtn = (ImageButton) findViewById(R.id.regPlugBtn);
            registerBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!registered) {
                        hasPort = true;
                        progressDialog.show();
                    } else {
                        registerPort();
                    }
                }
            });

            ImageButton plugBtn = (ImageButton) findViewById(R.id.regPlugBtn);
            plugBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!registered) {
                        hasPort = true;
                        progressDialog.show();
                    } else {
                        registerPort();
                    }
                }
            });

            /*Typeface handFont = Typeface.createFromAsset(getAssets(), "fonts/HandOfSeanPro.ttf");

            TextView sjHomeMsg = (TextView) findViewById(R.id.sjHomeMsg);
            sjHomeMsg.setTypeface(handFont);*/

        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception onCreate: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void goHome() {
        final Intent startMainActivity = new Intent(self,
                Home_View.class);
        startActivity(startMainActivity);
        if (progressDialog.isShowing()) progressDialog.dismiss();
        finish();
    }

    private void registerPort() {
        final Intent registerPort = new Intent(self,
                RegisterPort.class);
        startActivity(registerPort);
        if (progressDialog.isShowing()) progressDialog.dismiss();
        finish();
    }

    protected void showToast(final String msg) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(
                            self,
                            msg,
                            Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception showing toast: "+e.getClass()+": "+e.getMessage());
        }
    }
}

