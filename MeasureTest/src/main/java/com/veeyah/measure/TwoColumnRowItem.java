package com.veeyah.measure;

public class TwoColumnRowItem {
    private String left;
    private String right;

    public TwoColumnRowItem(String left, String right) {
        this.left= left;
        this.right = right;

    }

    public String getLeftText() {
        return left;
    }
    public void setLeftText(String left) { this.left = left; }
    public String getRightText() {
        return right;
    }

}
