package com.veeyah.measure;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.veeyah.matchbox.GetSunJoulesResponse;
import com.veeyah.measure.manager.DeviceMetricsManager;
import com.veeyah.measure.manager.GetSunJoulesManager;
import com.veeyah.measure.manager.RegisterDeviceManager;
import com.veeyah.measure.manager.VeeyahGUI;
import com.veeyah.measure.manager.WebServiceManager;
import com.veeyah.auth.RegisterDeviceResponse;
import com.veeyah.auth.RegisterType;


@SuppressLint("NewApi")
public class LoginScreen extends Activity implements OnClickListener, VeeyahGUI {
    private final String TAG = "[ Veeyah LoginScreen "+VeeyahConstants.VERSION_NUMBER+"]";
	//public static Context APPLICATION_CONTEXT = null;
	//DeviceManager deviceManager;

    public MatchBoxModel mbm;
    LoginScreen self;
    private boolean acceptedTerms = false;
    protected RelativeLayout termsContainer = null;
	//SocialMediaManager socialMediaManager = new SocialMediaManager();
	//static String TWITTER_CONSUMER_KEY = "3e1TZ7iXo1bbtvAFnIe1MhjVM"; // place
																		// your
																		// cosumer
																		// key
																		// here
	//static String TWITTER_CONSUMER_SECRET = "kRyB1eqGgzADPtvGNYdqdUlugF6jLX8Qmhd3FYyY8OApxKLpPW"; // place
																									// your
																									// consumer
																									// secret
																									// here

	// Preference Constants
	//static String PREFERENCE_NAME = "twitter_oauth";
	//static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
	//static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
	//static final String PREF_KEY_TWITTER_LOGIN = "isTwitterLogedIn";

	//static final String TWITTER_CALLBACK_URL = "oauth://t4jsample";

	// Twitter oauth urls
	//static final String URL_TWITTER_AUTH = "auth_url";
	//static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
	//static final String URL_TWITTER_OAUTH_TOKEN = "oauth_token";

	//private static Twitter twitter;
	//private static RequestToken requestToken;
	//SharedPreferences sh;
	//AlertDialogManager alert = new AlertDialogManager();
	/*private boolean mIntentInProgress;
	private boolean mSignInClicked;
	private ConnectionResult mConnectionResult;*/
	//private GoogleApiClient mGoogleApiClient;
	//private static final int RC_SIGN_IN = 0;
	//private String personName;
	private String email;
    private ProgressDialog progressDialog = null;
    private CheckBox termsBox = null;
	//private String gNetwrokId;
	//public String socialNetwork;
	//ConnectionDetector connectionDetector;
	//public static final String SCOPES = "https://www.googleapis.com/auth/plus.login "		+ "https://www.googleapis.com/auth/drive.file";
	//public static int REQUEST_CODE_TOKEN_AUTH = 9001;
	// FB
	//private static String APP_ID = "688179821246679"; // Replace with your App
														// ID

	// Instance of Facebook Class
	//private Facebook facebook = new Facebook(APP_ID);
	//private AsyncFacebookRunner mAsyncRunner;
	//String FILENAME = "AndroidSSO_data";
	//private SharedPreferences mPrefs;
	LinearLayout loginLinearLayout;

    public void refreshView(Object data) {
        if (data == null) {
            VeeyahLogger.w(TAG, "Null data Received!");
            return;
        }

        try {
            if (GetSunJoulesResponse.class.equals(data.getClass())) {
                final Intent startMainActivity = new Intent(LoginScreen.this,
                        Home_View.class);
                if (progressDialog != null) progressDialog.dismiss();
                
                // Animate the solar panel to enter over the sun 
                ImageView celestialObject = (ImageView) findViewById(R.id.daynight);
                ImageView solarPanelImageView = (ImageView) findViewById(R.id.solarPanelImageView);
                float xStart = (float) (celestialObject.getLeft()*5.0);

                ObjectAnimator translationXAnimator = ObjectAnimator.ofFloat(solarPanelImageView, "translationX", xStart, 0f);
        		ObjectAnimator translationYAnimator = ObjectAnimator.ofFloat(solarPanelImageView, "translationY", 50f, 0f);
        		translationXAnimator.setDuration(5000);
        		translationYAnimator.setDuration(5000);
        		
        		final AnimatorSet animatorSet = new AnimatorSet();
        		animatorSet.playTogether(translationXAnimator, translationYAnimator);
        		animatorSet.addListener(new AnimatorListenerAdapter() {
        			@Override
        			public void onAnimationEnd(Animator animation) {
        			    super.onAnimationEnd(animation);
        			    startActivity(startMainActivity);
                        finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        			}
        		});
        		if (animatorSet != null) {
        			animatorSet.start();
        		}
        		solarPanelImageView.setVisibility(View.VISIBLE);
				
            } else if (RegisterDeviceResponse.class.equals(data.getClass())) {
                GetSunJoulesManager getSJ = new GetSunJoulesManager();
                getSJ.getSunJoules(mbm.getDeviceID(), 50, new WebServiceManager(), this, false);
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception redirecting to Home_View: "+e.getClass()+": "+e.getMessage());
        }

    }

    public void showError(String msg) {
        Toast.makeText(ViaSolar.getContext(),
                "Cannot reach the server.\nPlease check your data connection and try again.", Toast.LENGTH_LONG)
                .show();
    }

    private void toggleTermsCheckbox() {
        try {
            if (mbm == null) mbm = new MatchBoxModel();
            mbm.incMetric(DeviceMetricsManager.CHK_TERMS);
            if (termsBox != null) {
                if (acceptedTerms) {
                    termsBox.setChecked(false);
                    acceptedTerms = false;
                } else {
                    termsBox.setChecked(true);
                    acceptedTerms = true;
                }
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception toggling terms checkbox: "+e.getClass()+": "+e.getMessage());
        }
    }

	// public static Context APPLICATION_CONTEXT;
	protected void onCreate(Bundle savedInstanceState) {	
		try {

            self = this;
            super.onCreate(savedInstanceState);
            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.activity_login_screen);
            ImageView solarPanelImageView = (ImageView) findViewById(R.id.solarPanelImageView);
            solarPanelImageView.setVisibility(View.GONE);

            termsBox = (CheckBox) findViewById(R.id.termsCheckbox);
            termsBox.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleTermsCheckbox();
                }
            });
            TextView termsLabel = (TextView) findViewById(R.id.termsCheckboxLabel);
            termsLabel.setText(Html.fromHtml("I agree to the <a href=\"https://veeyah.com/terms.jsp\">terms of use</a>."));
            termsLabel.setMovementMethod(LinkMovementMethod.getInstance());
            termsLabel.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleTermsCheckbox();
                }
            });
            termsContainer = (RelativeLayout) findViewById(R.id.termsContainer);
            
		/*deviceManager = new DeviceManager();
		//mAsyncRunner = new AsyncFacebookRunner(facebook);
		sh = PreferenceManager.getDefaultSharedPreferences(ViaSolar.getContext());
		socialNetwork = "no network";*/

            mbm = new MatchBoxModel();
            mbm.incMetric(DeviceMetricsManager.SCR_LOGIN);
            String deviceID = mbm.getDeviceID();
            loginLinearLayout = (LinearLayout) findViewById(R.id.scv);
            //String saveDeviceId = sh.getString(VeeyahConstants.DEVICE_ID, "");
            //getBackgroundScreen();
            // Intent startMainActivity = new Intent(LoginScreen.this,
            // Home_View.class);
            // startActivity(startMainActivity);
            // finish();
            if (!"".equals(deviceID) && !VeeyahConstants.DEFAULT_DEVICE_ID.equals(deviceID) && deviceID != null) {
                Intent startMainActivity = new Intent(LoginScreen.this, Home_View.class);
                startActivity(startMainActivity);
                finish();
                return;
            }

            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle(getString(R.string.app_name));
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);

            //final EditText nameText = new EditText(this);
            //final EditText passwordText = new EditText(this);
            final EditText mailText = (EditText) findViewById(R.id.mailTextView);
            mailText.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View v, int keyCode, KeyEvent event)
                {
                    if (event.getAction() == KeyEvent.ACTION_DOWN)
                    {
                        switch (keyCode)
                        {
                            case KeyEvent.KEYCODE_DPAD_CENTER:
                            case KeyEvent.KEYCODE_ENTER:
                                loginButtonProcess(mailText);
                                return true;
                            default:
                                break;
                        }
                    }
                    return false;
                }
            });
            Button loginBtn = (Button) findViewById(R.id.loginBtn);
            loginBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    loginButtonProcess(mailText);
                }
            });
		/*Button twitterLogin = (Button) findViewById(R.id.twitterAuthButton);
		twitterLogin.setVisibility(View.GONE);
		twitterLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				new ValidateLogin(nameText.getText().toString(), passwordText
						.getText().toString(), mailText.getText().toString(),
						"Twitter").execute();
				// loginToTwitter();

			}
		});
		if (!isTwitterLoggedInAlready()) {
			Uri uri = getIntent().getData();
			if (uri != null && uri.toString().startsWith(TWITTER_CALLBACK_URL)) {
				// oAuth verifier
				String verifier = uri
						.getQueryParameter(URL_TWITTER_OAUTH_VERIFIER);

				try {
					// Get the access token
					AccessToken accessToken = twitter.getOAuthAccessToken(
							requestToken, verifier);

					// Shared Preferences
					Editor e = sh.edit();

					// After getting access token, access token secret
					// store them in application preferences
					e.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
					e.putString(PREF_KEY_OAUTH_SECRET,
							accessToken.getTokenSecret());
					// Store login status - true
					e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
					e.commit(); // save changes

					VeeyahLogger.e("Twitter OAuth Token", "> " + accessToken.getToken());

					// Hide login button

					// Getting user details from twitter
					// For now i am getting his name only
					long userID = accessToken.getUserId();
					User user = twitter.showUser(userID);
					String username = user.getName();
					sh = PreferenceManager.getDefaultSharedPreferences(ViaSolar.getContext());
					String password = sh.getString("password", null);
					String email = sh.getString("email", null);

					passwordText.setText(password);
					mailText.setText(email);
					nameText.setText(username);
					Toast.makeText(
							getApplicationContext(),
							"Please Enter Email and Password then press Login button for continue",
							Toast.LENGTH_LONG).show();
					// new ValidateTwitterLogin(username,
					// activityBridge.getTwitterEmailId(),activityBridge.getTwitterPassword(),
					// "Twitter", accessToken.getToken()).execute();
					//
					VeeyahLogger.d("Twittwr Name", username);
					// Displaying in xml ui
				} catch (Exception e) {
					// Check log for login errors
					VeeyahLogger.e("Twitter Login Error", "> " + e.getMessage());
				}
			}
		}
		Button mLogin = (Button) findViewById(R.id.fbAuthButton);
        mLogin.setVisibility(View.GONE);
		mLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				VeeyahLogger.d("Image Button", "button Clicked");
				connectionDetector = new ConnectionDetector(ViaSolar.getContext());

				if (connectionDetector.isConnectingToInternet()) {
					loginToFacebook();
				} else {

					Toast.makeText(getApplicationContext(),
							"Internet not available", Toast.LENGTH_LONG).show();
				}

			}
		});

		Button btnSignIn = (Button) findViewById(R.id.googleAuthButton);
        btnSignIn.setVisibility(View.GONE);
		btnSignIn.setOnClickListener(this);

		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this).addApi(Plus.API, null)
				.addScope(Plus.SCOPE_PLUS_LOGIN).build();*/
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception onCreate: "+e.getClass()+": "+e.getMessage());
        }
	}

	private void loginButtonProcess(EditText mailText) {
        try {
            mbm.incMetric(DeviceMetricsManager.BTN_REGISTER);
            Editable text = mailText.getText();
            if (text != null && text.length() > 0) {
                if (!acceptedTerms) {
                    Toast.makeText(
                            ViaSolar.getContext(),
                            "Please accept the terms of use.",
                            Toast.LENGTH_LONG).show();
                    if (termsContainer != null) {
                        termsContainer.setBackgroundResource(R.drawable.red_border);
                    }
                    return;
                }
                
                if (android.util.Patterns.EMAIL_ADDRESS.matcher(text).matches()) {
                    // Check for invalid email characters (whitespace, etc)
                	progressDialog.show();
                    RegisterDeviceManager reg = new RegisterDeviceManager();
                    RegisterType deviceInfo = new RegisterType();
                    TelephonyManager tm = (TelephonyManager) ViaSolar.getContext().getSystemService(TELEPHONY_SERVICE);
                    deviceInfo.setName("");
                    deviceInfo.setEmail(text.toString());
                    String guid = tm.getDeviceId();
                    if (guid == null) guid = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                    if (guid == null) guid = "Null GUID";
                    deviceInfo.setClientGUID(guid);
                    deviceInfo.setDeviceName(android.os.Build.DEVICE);
                    deviceInfo.setDeviceType(android.os.Build.MODEL);
                    deviceInfo.setPassword("");
                    deviceInfo.setInviteCode("0KAwbgfzG00CNk6XX02qejsf");
                    deviceInfo.setBatteryCapacity(new BigInteger("" + mbm.getCapacity()));
                    reg.registerDevice(deviceInfo, new WebServiceManager(), self);
                    Toast.makeText(
                            ViaSolar.getContext(),
                            "Registering your device. . .",
                            Toast.LENGTH_LONG).show();
                } else {
                	Toast.makeText(
                            ViaSolar.getContext(),
                            "Please enter email & tap the Go button to continue",
                            Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(
                        ViaSolar.getContext(),
                        "Please enter email & tap the Go button to continue",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception registering device onClick of Go: " + e.getClass() + ": " + e.getMessage());
            Toast.makeText(
                    ViaSolar.getContext(),
                    "Sorry, there was a problem registering your device.  Please contact customersupport@veeyah.com",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onConfigurationChanged (Configuration newConfig) {
        //do nothing
        super.onConfigurationChanged(newConfig);
    }

	/*class ValidateLogin extends AsyncTask<String, String, String> {
		ProgressDialog pd;
		String status;
		JSONObject responseObject;
		String username;
		String password;
		String mail;
		String statusCode;
		String socialMedia;

		public ValidateLogin(String username, String password, String mail,
				String socialMedia) {
			this.username = username;
			this.password = password;
			this.mail = mail;
			this.socialMedia = socialMedia;
		}

		protected String doInBackground(String... params) {
			if (socialMedia == null) {
				statusCode = deviceManager.registerDevice(username, password,
						mail);
			} else {
				if (socialMedia.equalsIgnoreCase("Twitter")) {
					Editor e = sh.edit();
					e.putString("email", mail);
					e.commit();

					Editor e1 = sh.edit();
					e1.putString("password", password);
					e1.commit();

					// statusCode = deviceManager.registerDevice(username,
					// password,
					// mail);
					String networkId = loginToTwitter();
					statusCode = "UnSucessfully";

				} else if (socialMedia.equalsIgnoreCase("Google+")) {
					VeeyahAuth_SOAPClient client = new VeeyahAuth_SOAPClient();
					RegisterType deviceInfo = new RegisterType();
					TelephonyManager tm = (TelephonyManager) ViaSolar.getContext().getSystemService(ViaSolar.getContext().TELEPHONY_SERVICE);
					deviceInfo.name = getPersonName();
					deviceInfo.email = getEmail();
					deviceInfo.clientGUID = tm.getDeviceId();// "9F22AE93-89B2-4F31-BD28-B49E36860412";
					deviceInfo.deviceName = android.os.Build.DEVICE;
					deviceInfo.deviceType = android.os.Build.MODEL;
					deviceInfo.password = getEmail();
					deviceInfo.inviteCode = "0KAwbgfzG00CNk6XX02qejsf";
					deviceInfo.batteryCapacity = new BigInteger("" + 32);

					try {

						setgNetwrokId(GoogleAuthUtil.getToken(LoginScreen.this,
								Plus.AccountApi
										.getAccountName(mGoogleApiClient),
								"oauth2:" + SCOPES));
					} catch (IOException transientEx) {
						// Network or server error, try later
						VeeyahLogger.v("Veeyah", transientEx.toString());
					} catch (UserRecoverableAuthException e) {
						// Recover (with e.getIntent())
						VeeyahLogger.e("Veeyah", e.toString());
						Intent recover = e.getIntent();
						startActivityForResult(recover, REQUEST_CODE_TOKEN_AUTH);
					} catch (GoogleAuthException authEx) {
						// The call is not ever expected to succeed
						// assuming you have already verified that
						// Google Play services is installed.
						VeeyahLogger.e("Veeyah", authEx.toString());
					}
					socialNetwork = "Google";
					RegisterDevice register = new RegisterDevice();
					register.deviceInfo = deviceInfo;
					Calendar cal = Calendar.getInstance(TimeZone
							.getTimeZone("GMT+5:30"));
					Date currentLocalTime = cal.getTime();
					register.requestTime = currentLocalTime;
					client.registerDevice(register,
							new SOAPServiceCallback<RegisterDevice>() {

								@Override
								public void onSuccess(RegisterDevice arg0) {
									System.out.println("Success!!");

								}

								@Override
								public void onFailure(Throwable arg0,
										String arg1) {
									System.out.println("Failure");

								}

								@Override
								public void onSOAPFault(Object arg0) {
									System.out.println("Soap Fault");

								}

							});
					socialNetwork = "Google";
					// SocialMediaManager socialMediaManager=new
					// SocialMediaManager();
					//
					// socialMediaManager.registerForSocialMedia("Google Plus",
					// networkId);
					statusCode = "Sucessfully";

				}
			}
			return null;

		}

		@Override
		protected void onPreExecute() {
			Activity a = LoginScreen.this;
			while (a.getParent() != null) {
				a = a.getParent();
			}
			pd = new ProgressDialog(a);
			Window window = pd.getWindow();
			pd.setMessage("Loading...");
			window.setLayout(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			pd.show();
		}

		protected void onProgressUpdate(String... progress) {
		}

		@Override
		protected void onPostExecute(String file_url) {
			pd.dismiss();
			if (statusCode.equalsIgnoreCase("Sucessfully")) {
				if (socialNetwork.equalsIgnoreCase("Facebook")) {
					String access_token = mPrefs
							.getString("access_token", null);
					socialMediaManager.registerForSocialMedia("Facebook",
							facebook.getAccessToken());
					Intent startMainActivity = new Intent(LoginScreen.this,
							Home_View.class);
					startActivity(startMainActivity);
					finish();
				} else if (socialNetwork.equalsIgnoreCase("Google")) {
					String networkId = getgNetwrokId();

					socialMediaManager.registerForSocialMedia("Google",
							networkId);
					Intent startMainActivity = new Intent(LoginScreen.this,
							Home_View.class);
					startActivity(startMainActivity);
					finish();
				} else {
					Intent startMainActivity = new Intent(LoginScreen.this,
							Home_View.class);
					startActivity(startMainActivity);
					finish();
				}

			}
			// ShowAlert(responseObject);

		}
	}

	public void call(Session session, SessionState state, Exception exception) {

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Google+
		if (requestCode == RC_SIGN_IN) {
			if (resultCode != RESULT_OK) {
				mSignInClicked = false;
			}

			mIntentInProgress = false;

			if (!mGoogleApiClient.isConnecting()) {
				mGoogleApiClient.connect();
			}
		}
		// Facebook
		super.onActivityResult(requestCode, resultCode, data);
		facebook.authorizeCallback(requestCode, resultCode, data);

	}

	private String loginToTwitter() {
		// Check if already logged in
		if (!isTwitterLoggedInAlready()) {
			ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
			builder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);
			Configuration configuration = builder.build();

			TwitterFactory factory = new TwitterFactory(configuration);
			twitter = factory.getInstance();

			try {

				requestToken = twitter
						.getOAuthRequestToken(TWITTER_CALLBACK_URL);
				this.startActivity(new Intent(Intent.ACTION_VIEW, Uri
						.parse(requestToken.getAuthenticationURL())));
			} catch (Exception e) {
				e.printStackTrace();
			}

			return requestToken.getToken();
		} else {

			// user already logged into twitter
			Toast.makeText(getApplicationContext(),
					"Already Logged into twitter", Toast.LENGTH_LONG).show();
			return "Already Logged into twitter";

		}
	}

	private boolean isTwitterLoggedInAlready() {
		// return twitter login status from Shared Preferences
		return sh.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
	}*/

	protected void onStart() {
		super.onStart();
		//mGoogleApiClient.connect();
	}

	protected void onStop() {
		super.onStop();
		/*if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}*/
	}

	/*@Override
	public void onConnectionFailed(ConnectionResult result) {
		if (!result.hasResolution()) {
			GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
					0).show();
			return;
		}

		if (!mIntentInProgress) {
			// Store the ConnectionResult for later usage
			mConnectionResult = result;

			if (mSignInClicked) {
				// The user has already clicked 'sign-in' so we attempt to
				// resolve all
				// errors until the user is signed in, or they cancel.
				resolveSignInError();
			}
		}

	}

	@Override
	public void onConnected(Bundle arg0) {
		mSignInClicked = false;
		Toast.makeText(this, "User is connected!", Toast.LENGTH_LONG).show();

		// Get user's information
		getProfileInformation();

	}

	@Override
	public void onConnectionSuspended(int arg0) {
		mGoogleApiClient.connect();

	}*/

	@Override
	public void onClick(View v) {
		/*if (!mGoogleApiClient.isConnecting()) {
			mSignInClicked = true;
			resolveSignInError();
		}*/
	}

	/*private void resolveSignInError() {
		if (mConnectionResult!=null && mConnectionResult.hasResolution()) {
			try {
				mIntentInProgress = true;
				mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
			} catch (SendIntentException e) {
				mIntentInProgress = false;
				mGoogleApiClient.connect();
			}
		}
	}

	// Get google+ profile info and call RegisterDevice service if result is
	// success than call BindSocialNetwork service
	private void getProfileInformation() {
		try {
			if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
				Person currentPerson = Plus.PeopleApi
						.getCurrentPerson(mGoogleApiClient);
				setPersonName(currentPerson.getDisplayName());
				setEmail(Plus.AccountApi.getAccountName(mGoogleApiClient));

				VeeyahLogger.e("Veeyah", "Name: " + personName + ", email: " + email);
				new ValidateLogin(getPersonName(), getEmail(), getEmail(),
						"Google+").execute();

			} else {
				Toast.makeText(getApplicationContext(),
						"Person information is null", Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	/*public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}*/

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/*public String getgNetwrokId() {
		return gNetwrokId;
	}

	public void setgNetwrokId(String gNetwrokId) {
		this.gNetwrokId = gNetwrokId;
	}

	@SuppressWarnings("deprecation")
	public void loginToFacebook() {

		mPrefs = getPreferences(MODE_PRIVATE);
		String access_token = mPrefs.getString("access_token", null);
		long expires = mPrefs.getLong("access_expires", 0);

		if (access_token != null) {
			facebook.setAccessToken(access_token);
			// btnFbLogin.setVisibility(View.INVISIBLE);
			//
			// // Making get profile button visible
			// btnFbGetProfile.setVisibility(View.VISIBLE);
			//
			// // Making post to wall visible
			// btnPostToWall.setVisibility(View.VISIBLE);
			//
			// // Making show access tokens button visible
			// btnShowAccessTokens.setVisibility(View.VISIBLE);

			VeeyahLogger.d("FB Sessions", "" + facebook.isSessionValid());
		}

		if (expires != 0) {
			facebook.setAccessExpires(expires);
		}
		if (!facebook.isSessionValid()) {
			facebook.authorize(LoginScreen.this,
					new String[] { "email", "publish_stream" }, facebook.FORCE_DIALOG_AUTH,
					new DialogListener() {

						@Override
						public void onCancel() {
							// Function to handle cancel event
						}
						@Override
						public void onComplete(Bundle values) {
							getFBProfileInformation();
							// Function to handle complete event
							// Edit Preferences and update facebook acess_token
							// SharedPreferences.Editor editor = mPrefs.edit();
							// editor.putString("access_token",
							 String Temp=facebook.getAccessToken();
							// editor.putLong("access_expires",
							// facebook.getAccessExpires());
							// editor.commit();

							// // Making Login button invisible
							// btnFbLogin.setVisibility(View.INVISIBLE);
							//
							// // Making logout Button visible
							// btnFbGetProfile.setVisibility(View.VISIBLE);
							//
							// // Making post to wall visible
							// btnPostToWall.setVisibility(View.VISIBLE);
							//
							// // Making show access tokens button visible
							// btnShowAccessTokens.setVisibility(View.VISIBLE);
						}

						@Override
						public void onError(DialogError error) {
							// Function to handle error

						}

						@Override
						public void onFacebookError(FacebookError fberror) {
							// Function to handle Facebook errors

						}

					});
		}
	}*/

	/**
	 * Get Profile information by making request to Facebook Graph API
	 * */
	/*@SuppressWarnings("deprecation")
	public void getFBProfileInformation() {
		mAsyncRunner.request("me", new RequestListener() {
			@Override
			public void onComplete(String response, Object state) {
				VeeyahLogger.d("Profile", response);
				String json = response;
				try {
					// Facebook Profile JSON data
					JSONObject profile = new JSONObject(json);

					// getting name of the user
					final String name = profile.getString("name");

					// getting email of the user
					final String email = profile.getString("email");

					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							socialNetwork = "Facebook";
							new ValidateLogin(name, email, email, null)
									.execute();
							// Toast.makeText(getApplicationContext(), "Name: "
							// + name + "\nEmail: " + email,
							// Toast.LENGTH_LONG).show();
						}

					});

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onIOException(IOException e, Object state) {
			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e,
					Object state) {
			}

			@Override
			public void onMalformedURLException(MalformedURLException e,
					Object state) {
			}

			@Override
			public void onFacebookError(FacebookError e, Object state) {
			}
		});
	}*/

	public void getBackgroundScreen() {

		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("H");
		String strDate = sdf.format(c.getTime());
		int hours = Integer.parseInt(strDate);
		final ImageView dayNight = (ImageView) findViewById(R.id.daynight);

		if (isBetween(hours, 6, 8)) {
			loginLinearLayout.setBackgroundResource(R.drawable.sky_bg_04);
			dayNight.setImageResource(R.drawable.sun);
		} else if (isBetween(hours, 8, 12)) {
			loginLinearLayout.setBackgroundResource(R.drawable.sky_bg_04);
			dayNight.setImageResource(R.drawable.sun);

		} else if (isBetween(hours, 12, 16)) {

			loginLinearLayout.setBackgroundResource(R.drawable.sky_bg);
			dayNight.setImageResource(R.drawable.sun);

		} else if (isBetween(hours, 16, 20)) {

			loginLinearLayout.setBackgroundResource(R.drawable.sky_bg);
			dayNight.setImageResource(R.drawable.sun);

		} else if (isBetween(hours, 20, 24)) {
			dayNight.setImageResource(R.drawable.moon);
			loginLinearLayout.setBackgroundResource(R.drawable.sky_bg_04);

		} else if (isBetween(hours, 0, 6)) {
			dayNight.setImageResource(R.drawable.moon);

			loginLinearLayout.setBackgroundResource(R.drawable.sky_bg_04);

		}
	}

	public static boolean isBetween(int x, int lower, int upper) {
		return lower < x && x <= upper;
	}
}
