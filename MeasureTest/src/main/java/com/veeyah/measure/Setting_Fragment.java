package com.veeyah.measure;

import com.veeyah.measure.manager.ChangeSettingManager;
import com.veeyah.measure.manager.DeviceMetricsManager;
import com.veeyah.measure.manager.WebServiceManager;
import com.veeyah.utility.Astro;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Setting_Fragment extends android.support.v4.app.Fragment {
    private final static String TAG = "[Veeyah Setting Fragment "+VeeyahConstants.VERSION_NUMBER+"]";
    View rootView;
	EditText userName;
	EditText email;
    CheckBox stickySetting;
	Button update;
    MatchBoxModel mbm;
	RelativeLayout mainLayout;
    protected Setting_Fragment self;
    private Home_View home = null;

    @Override
    public void onDestroy() {
        VeeyahLogger.i("Veeyah Home Fragment", "onDestroy called");
        Home_View.unbindDrawables(rootView);
        super.onDestroy();
        VeeyahLogger.i("Veeyah Home Fragment", "onDestroy finished");
    }

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            home = (Home_View) getActivity();
            mbm = home.mbm;
            mbm.incMetric(DeviceMetricsManager.SCR_SETTINGS);
            self = this;
            rootView = inflater.inflate(R.layout.fragment_settings, container, false);
            if (rootView == null) throw new ViaSolarException("Root View is null!");

            // Update the version info text view
            TextView versionInfoTextView = (TextView) rootView.findViewById(R.id.versionInfoTextView);
            PackageInfo packageInfo;
            String strVersion = "?.?";
            try {
                PackageManager pm = ViaSolar.getContext().getPackageManager();
                if (pm == null)
                    throw new ViaSolarException("PackageManager is null for Details_Fragment");
                packageInfo = pm.getPackageInfo(ViaSolar.getContext().getPackageName(), 0);
                strVersion = packageInfo.versionName;
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Exception getting package/version name: " + e.getClass() + ": " + e.getMessage());
            }

            versionInfoTextView.setText("Version " + strVersion);

            userName = (EditText) rootView.findViewById(R.id.edtxt_name);
            email = (EditText) rootView.findViewById(R.id.edtxt_mail);

            email.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (email.getText().toString().equals(VeeyahConstants.DEFAULT_USER_EMAIL)) {
                            email.setText("");
                        }
                    } catch (Exception e) {
                        VeeyahLogger.w(TAG, "Exception handling email onClick event: "+e.getClass()+": "+e.getMessage());
                    }
                }
            });

            stickySetting = (CheckBox) rootView.findViewById(R.id.stickyNotificationCheckbox);
            stickySetting.setChecked(mbm.isStickyNotification());
            stickySetting.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        mbm.setStickyNotification(true);
                        Toast.makeText(
                                ViaSolar.getContext(),
                                "Sorry, this version of Android cannot use minimal notifications in conjunction with our battery service.",
                                Toast.LENGTH_LONG).show();
                    } else {
                        if (!stickySetting.isChecked()) {
                            mbm.setStickyNotification(false);
                            //stickySetting.setChecked(false);
                        } else {
                            mbm.setStickyNotification(true);
                            Toast.makeText(
                                    ViaSolar.getContext(),
                                    "The notification will appear whenever the battery service is running.",
                                    Toast.LENGTH_LONG).show();
                            //stickySetting.setChecked(true);
                        }
                        Intent loadData = new Intent(home.getApplicationContext(), VeeyahBattery.class);
                        loadData.setAction(VeeyahBattery.REFRESH);
                        home.startService(loadData);
                    }
                }
            });
            update = (Button) rootView.findViewById(R.id.btn_update);
            update.setEnabled(true);
            mainLayout = (RelativeLayout) rootView.findViewById(R.id.settingsFragment);
            update.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    try {
                        if (mbm == null) mbm = home.mbm;
                        mbm.incMetric(DeviceMetricsManager.BTN_UPDATE);
                        if (userName.getText() == null || email.getText() == null) return;
                        if (userName.getText().length() > 0
                                && email.getText().length() > 0) {
                            ChangeSettingManager changeSettingManager = new ChangeSettingManager();
                            changeSettingManager.changeSetting(mbm.getDeviceID(), userName.getText().toString(), email.getText().toString(), new WebServiceManager(), home);
                            update.setEnabled(false);
                            home.selectNavDrawerItem(Home_View.NAV_DRAWER_ITEM_HOME);
                        } else if (userName.getText().length() < 1) {
                            home.showToast("Please enter a valid username!");
                        } else if (email.getText().length() < 1) {
                            home.showToast("Please enter a valid email!");
                        }
                    } catch (Exception e) {
                        VeeyahLogger.w(TAG, "Exception updating settings in onClick method: " + e.getClass() + ": " + e.getMessage());
                    }

                }
            });
            
            // Configure menu button
            ImageButton menuImageButton = (ImageButton) rootView.findViewById(R.id.menuImageButton);
            menuImageButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
                    if (mbm == null) mbm = home.mbm;
                    mbm.incMetric(DeviceMetricsManager.CHK_STICKY);
					if (Home_View.mDrawerLayout.isDrawerOpen(Home_View.mDrawerList)) {
						Home_View.mDrawerLayout.closeDrawer(Home_View.mDrawerList);
					} else {
						Home_View.mDrawerLayout.openDrawer(Home_View.mDrawerList);
					}
				}
			});

            Button regPlugBtn = (Button) rootView.findViewById(R.id.regPlugBtn);
            regPlugBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.registerPort();
                }
            });

            Button feedbackBtn = (Button) rootView.findViewById(R.id.feedbackBtn);
            feedbackBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.feedback();
                }
            });

            Button termsBtn = (Button) rootView.findViewById(R.id.termsBtn);
            termsBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                    browserIntent.setData(Uri.parse("http://veeyah.com/terms.jsp"));
                    startActivity(browserIntent);
                }
            });

            // Configure footer buttons
            ImageButton shareImageButton = (ImageButton) rootView.findViewById(R.id.shareImageButton);
            ImageButton homeImageButton = (ImageButton) rootView.findViewById(R.id.homeImageButton);
            ImageButton plugImageButton = (ImageButton) rootView.findViewById(R.id.plugSolarImageButton);
            ImageButton phoneImageButton = (ImageButton) rootView.findViewById(R.id.phoneImageButton);

            shareImageButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.share();
                }
            });
            homeImageButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.selectNavDrawerItem(Home_View.NAV_DRAWER_ITEM_HOME);
                }
            });
            plugImageButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.clickPlugIcon();
                }
            });
            phoneImageButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.selectNavDrawerItem(Home_View.NAV_DRAWER_ITEM_PHONE);
                }
            });

            // Set help tip longClickListeners
            home.setHelpLongClickListener(shareImageButton, "Share this app!");
            home.setHelpLongClickListener(homeImageButton, "Return to the home screen.");
            home.setHelpLongClickListener(plugImageButton, "View your SunPort's info.");
            home.setHelpLongClickListener(phoneImageButton, "View your phone's status.");

            if (home.sunportDetected) {
                plugImageButton.setImageResource(R.drawable.icon_plug_white);
            }

            setBackgroundImage();
            setFields();
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception onCreate: "+e.getClass()+": "+e.getMessage());
        }

		return rootView;

	}

	void setBackgroundImage() {
        try {
            // Contextual background images
            if (Astro.isNight()) {
                mainLayout.setBackgroundResource(R.drawable.sky_bg_04);
                //bottomLayout.setBackgroundResource(R.drawable.footer_moon);

                // Stop any animation on the footer for night image
                /*if (animatorSet != null) {
                    animatorSet.cancel();
                }*/
            } else {
                mainLayout.setBackgroundResource(R.drawable.sky_bg);
                //bottomLayout.setBackgroundResource(R.drawable.footer_sun);

                // Animate the sun slightly
                /*ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(bottomLayout, "scaleX", 1, 1f + Phone_Fragment.sunImagePlus, 1);
                ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(bottomLayout, "scaleY", 1, 1f + Phone_Fragment.sunImagePlus, 1);
                scaleXAnimator.setDuration(3000);
                scaleXAnimator.setRepeatCount(Animation.INFINITE);
                scaleYAnimator.setDuration(3000);
                scaleYAnimator.setRepeatCount(Animation.INFINITE);
                animatorSet = new AnimatorSet();
                if (animatorSet != null) {
                    animatorSet.playTogether(scaleXAnimator, scaleYAnimator);
                    animatorSet.start();
                }*/
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception setting background: "+e.getClass()+": "+e.getMessage());
        }
	}

	@Override
	public void onResume() {
		setBackgroundImage();
		super.onResume();
	}

    public void setFields() {
        try {
            if (userName == null || email == null) return;
            if (home == null) home = (Home_View) getActivity();
            if (home.mbm == null) throw new ViaSolarException("Data model was not found!");
            VeeyahLogger.i("Veeyah Settings Fragment", "Set name="+home.mbm.getUserName()+": email="+home.mbm.getUserEmail());
            final Handler handler = new Handler();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (home.mbm.getUserName() != null) userName.setText(home.mbm.getUserName());
                    if (home.mbm.getUserEmail() != null) email.setText(home.mbm.getUserEmail());
                }
            });
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception setting fields: "+e.getClass()+": "+e.getMessage());
        }
    }

}
