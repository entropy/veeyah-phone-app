package com.veeyah.measure;

import android.util.Log;

import com.veeyah.measure.manager.ErrorReportManager;
import com.veeyah.measure.manager.WebServiceManager;

public final class VeeyahLogger {
    VeeyahLogger() {}

    public static int v(java.lang.String tag, java.lang.String msg) { ErrorReportManager.sendErrorReport(new WebServiceManager(), new ViaSolarException(msg), tag); return Log.v(tag, msg); }

    public static int v(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) { ErrorReportManager.sendErrorReport(new WebServiceManager(), tr, tag); return Log.v(tag, msg, tr); }

    public static int d(java.lang.String tag, java.lang.String msg) { ErrorReportManager.sendErrorReport(new WebServiceManager(), new ViaSolarException(msg), tag); return Log.d(tag, msg); }

    public static int d(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) { ErrorReportManager.sendErrorReport(new WebServiceManager(), tr, tag); return Log.d(tag, msg, tr); }

    public static int i(java.lang.String tag, java.lang.String msg) { /*ErrorReportManager.sendErrorReport(new WebServiceManager(), new ViaSolarException(msg), tag);*/ return Log.i(tag, msg); }

    public static int i(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) { /*ErrorReportManager.sendErrorReport(new WebServiceManager(), tr, tag);*/ return Log.i(tag, msg, tr); }

    public static int w(java.lang.String tag, java.lang.String msg) { ErrorReportManager.sendErrorReport(new WebServiceManager(), new ViaSolarException(msg), tag); return Log.w(tag, msg); }

    public static int w(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) { ErrorReportManager.sendErrorReport(new WebServiceManager(), tr, tag); return Log.w(tag, msg, tr); }

    public static boolean isLoggable(java.lang.String s, int i) { return Log.isLoggable(s, i); }

    public static int w(java.lang.String tag, java.lang.Throwable tr) { return Log.w(tag, tr); }

    public static int e(java.lang.String tag, java.lang.String msg) { ErrorReportManager.sendErrorReport(new WebServiceManager(), new ViaSolarException(msg), tag); return Log.e(tag, msg); }

    public static int e(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) { ErrorReportManager.sendErrorReport(new WebServiceManager(), tr, tag); return Log.e(tag, msg, tr); }

    public static int wtf(java.lang.String tag, java.lang.String msg) { ErrorReportManager.sendErrorReport(new WebServiceManager(), new ViaSolarException(msg), tag); return Log.wtf(tag, msg); }

    public static int wtf(java.lang.String tag, java.lang.Throwable tr) { ErrorReportManager.sendErrorReport(new WebServiceManager(), tr, tag); return Log.wtf(tag, tr); }

    public static int wtf(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) { ErrorReportManager.sendErrorReport(new WebServiceManager(), tr, tag); return Log.wtf(tag, msg, tr); }

    public static java.lang.String getStackTraceString(java.lang.Throwable tr) { return Log.getStackTraceString(tr); }

    public static int println(int priority, java.lang.String tag, java.lang.String msg) { return Log.println(priority, tag, msg); }
}
