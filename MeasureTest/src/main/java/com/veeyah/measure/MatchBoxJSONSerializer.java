package com.veeyah.measure;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.json.JSONArray;
import org.json.JSONTokener;

import android.content.Context;

public class MatchBoxJSONSerializer {

	 private static final String fileName = "veeyahLog";

	    public static void saveMbInfo(MatchBoxModel mbm, Context myContext) {
	        JSONArray array = new JSONArray();
	        try {
	            array.put(0, mbm.getStartTime());
	            array.put(1, mbm.getLastPoint());
	            array.put(2, mbm.getCapacity());
	            array.put(3, mbm.getTotCharge());
	            array.put(4, mbm.getTotDischarge());
	            array.put(5, mbm.getAvailableSJ());
	            array.put(6, mbm.getUsedSJ());
	            array.put(7, mbm.getIdleTime());
                array.put(8, mbm.getLastRingPos());
	            array.put(9, mbm.getDeviceID());
	            array.put(10, mbm.getLastTime());
                array.put(11, mbm.getUserName());
                array.put(12, mbm.getUserEmail());
                array.put(13, mbm.isStickyNotification());
                array.put(14, mbm.getMostDays());
                array.put(15, mbm.getLastHomeMsg());
                array.put(16, mbm.getCachedSJ());
                array.put(17, mbm.getLastMetricReportTime());
                array.put(18, mbm.isFirstUse());
	        } catch (Exception e) {
	            VeeyahLogger.w("Veeyah", "Exception building JSONArray: " + e.getClass() + ": " + e.getMessage());
	        }
	        Writer w = null;
	        try {
	            OutputStream out = myContext.openFileOutput(fileName, Context.MODE_PRIVATE);
	            w = new OutputStreamWriter(out);
	            w.write(array.toString());
	        } catch (Exception e) {
	            VeeyahLogger.w("Veeyah", "Exception writing to file: "+e.getClass()+": "+e.getMessage());
	        } finally {
	            try {
	                if (w != null) w.close();
	            } catch (Exception e) {
	                VeeyahLogger.w("Veeyah", "Exception closing file: "+e.getClass()+": "+e.getMessage());
	            }
	        }
	    }

	    //if this method returns null, this means the device should be registered
	    public static MatchBoxModel loadMbInfo(Context myContext) {
	        MatchBoxModel mbm = new MatchBoxModel();
	        BufferedReader br = null;
	        try {
	            if (myContext == null) {
	                VeeyahLogger.w("Veeyah", "MatchBoxJSONSerializer.loadMbInfo: Null context!");
	                return(mbm);
	            }
	            InputStream in = myContext.openFileInput(fileName);
	            br = new BufferedReader(new InputStreamReader(in));
	            StringBuilder json = new StringBuilder();
	            String line;
	            while ((line = br.readLine()) != null) {
	                json.append(line);
	            }
	            JSONArray array = (JSONArray) new JSONTokener(json.toString()).nextValue();
	            if (array.length() != 13) {
	                VeeyahLogger.w("Veeyah", "Invalid JSON array ("+array.length()+")!");
	            } else {
	                mbm.setStartTime(array.getString(0));
	                mbm.setlastPoint(array.getLong(1));
	                if (mbm.getCapacity() != array.getInt(2)) {
	                    VeeyahLogger.i("Veeyah", "Capacity changed from "+array.getInt(2)+" to "+mbm.getCapacity());
                        mbm.setCapacity(array.getInt(2));
	                }
	                mbm.setTotCharge(array.getInt(3));
	                mbm.setTotDischarge(array.getInt(4));
	                mbm.setAvailableSJ(array.getLong(5));
	                mbm.setUsedSJ(array.getLong(6));
	                mbm.setIdleTime(array.getLong(7));
                    mbm.setLastRingPosition(array.getLong(8));
	                mbm.setDeviceID(array.getString(9));
	                mbm.setLastTime(array.getString(10));
                    mbm.setUserName(array.getString(11));
                    mbm.setUserEmail(array.getString(12));
                    mbm.setStickyNotification(array.getBoolean(13));
                    mbm.setMostDays(array.getInt(14));
                    mbm.setLastHomeMsg(array.getString(15));
                    mbm.setCachedSJ(array.getLong(16));
                    mbm.setMetricReportTime(array.getLong(17));
                    mbm.setFirstUse(array.getBoolean(18));

	            }
	            if (mbm.getDeviceID() == null || "".equals(mbm.getDeviceID())) return(null);
	        } catch (FileNotFoundException fnfe) {
	            //caller should register device
	            return(null);
	        } catch (Exception e) {
	            VeeyahLogger.w("Veeyah", "Exception loading file: "+e.getClass()+": "+e.getMessage());
	        } finally {
	            try {
	                if (br != null) br.close();
	            } catch (Exception e) {
	                VeeyahLogger.w("Veeyah", "Exception closing input file: "+e.getClass()+": "+e.getMessage());
	            }
	        }
	        return(mbm);
	    }
	}
