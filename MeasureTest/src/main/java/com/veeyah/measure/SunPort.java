package com.veeyah.measure;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by netripy on 12/2/2014.
 */
public class SunPort implements Parcelable {
    public static final int DISCONNECTED = 0;
    public static final int CONNECTING	= 1;
    public static final int CONNECTED	= 2;

    //		String  name="BLE";
    //private String  ver = "1.00";
    //private String	sn = "";
    private String 	address="";
    private int	mConnectionState = DISCONNECTED;
    private int lastEnergy = -1;
    private double lastPower = -1;
    private long energyTime = -1;
    private long powerTime = -1;
    private int toFill = 0;
    private long usedEnergyTime = -1;
    private int usedEnergy = -1;
    private String deviceID = "";

    BluetoothGattCharacteristic mGattWrite=null;
    BluetoothGattCharacteristic mGattNotify=null;
    BluetoothGatt mBluetoothGatt = null;

    public SunPort() {

    }

    public SunPort(String macAddr) {
        this.address = macAddr;
    }

    public SunPort(Parcel in) {
        this.address = in.readString();
        this.mConnectionState = in.readInt();
        this.lastEnergy = in.readInt();
        this.lastPower = in.readDouble();
        this.toFill = in.readInt();
        this.energyTime = in.readLong();
        this.powerTime = in.readLong();
        this.deviceID = in.readString();
    }

    public String getAddress() {
        return(address);
    }

    public String getRawAddress() { return(address.replace(":", ""));}

    public String getUnitID() {
        String raw = getRawAddress();
        return(raw.substring(raw.length()-4));
    }

    protected void setAddress(String address) {
        this.address = address;
    }

    public void setDeviceID(String deviceID) { this.deviceID = deviceID;  }

    public String getDeviceID() { return(deviceID); }

    public int getConnectionState() {
        return mConnectionState;
    }

    public void setConnectionState(int newState) {
        this.mConnectionState = newState;
    }

    public int getEnergy() {
        return(lastEnergy);
    }

    public void setEnergy(int energy) {
        if (lastEnergy == -1) {
            usedEnergy = 0;
            usedEnergyTime = System.currentTimeMillis();
        } else {
            if (lastEnergy-energy > 0) usedEnergy += lastEnergy-energy;
        }
        if ((System.currentTimeMillis()-usedEnergyTime) > 86400000) { //24 hours have passed
            usedEnergy = 0;
            usedEnergyTime = System.currentTimeMillis();
        }
        this.lastEnergy = energy;
        this.energyTime = System.currentTimeMillis();
    }

    public double getPower() {
        return(lastPower);
    }

    public void setPower(double power) {
        this.lastPower = power;
        this.powerTime = System.currentTimeMillis();
    }

    public void resetFill() {
        toFill = 0;
    }

    public void setPendingFill(int fillSJ) {
        toFill += fillSJ;
    }

    public long getEnergyTime() {
        return(energyTime);
    }

    public long getPowerTime() {
        return(powerTime);
    }

    public long getLatestTime() { if (powerTime >= energyTime) return(powerTime); return(energyTime); }

    public String getLastUpdateTime() {
        Date d = new Date();
        if (energyTime >= powerTime) {
            d.setTime(energyTime);
        } else {
            d.setTime(powerTime);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm zzz", Locale.ENGLISH);
        return(sdf.format(d));
    }

    public int getPendingFill() { return(toFill);}

    public int getUsedEnergy() {
        if (usedEnergy <= 0) return(0);
        return(usedEnergy);
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(address);
        out.writeInt(mConnectionState);
        out.writeInt(lastEnergy);
        out.writeDouble(lastPower);
        out.writeInt(toFill);
        out.writeLong(energyTime);
        out.writeLong(powerTime);
        out.writeString(deviceID);
    }

    public static final Parcelable.Creator<SunPort> CREATOR = new Parcelable.Creator<SunPort>() {
        public SunPort createFromParcel(Parcel in) {
            return new SunPort(in);
        }
        public SunPort[] newArray(int size) { return new SunPort[size];}
    };

    public int describeContents() { return(address.hashCode()+lastEnergy+mConnectionState+toFill+deviceID.hashCode());}

    public String printState() { return(address+":"+lastEnergy+":"+lastPower+":"+energyTime+":"+powerTime+":"+deviceID);}
}
