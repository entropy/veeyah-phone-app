package com.veeyah.measure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.veeyah.matchbox.GetSunJoulesResponse;
import com.veeyah.matchbox.MySummaryResponse;
import com.veeyah.measure.manager.DeviceMetricsManager;
import com.veeyah.measure.manager.DisplaySettingsManager;
import com.veeyah.measure.manager.GetSunJoulesManager;
import com.veeyah.measure.manager.VeeyahGUI;
import com.veeyah.measure.manager.WebServiceManager;
import com.veeyah.auth.DisplaySettingsResponse;

public class Home_View extends ActionBarActivity implements VeeyahGUI {

    final String TAG = "[Veeyah Home_View "+VeeyahConstants.VERSION_NUMBER+"]";
    public static final String GET_SUNJOULES_TYPE = "GetSunJoules";

    public static final long SETTINGS_CHECK_TIMEOUT = 1800000;

	Home_View self = this;
    MatchBoxModel mbm;
    private long lastSettingCheck = 0;
	private boolean networkRequestPending = false;
    private boolean ignoreLastRequest = false;
    android.support.v4.app.Fragment fragment = null;
    Phone_Fragment phoneFrag = null;
    MySummaryResponse mySummary = null;
    Setting_Fragment settingFrag = null;
    Plug_Fragment plugFrag = null;
    //Details_Fragment detailsFrag = null;
    private RelativeLayout homeFrame = null;
    public boolean sunportDetected = false;
    public boolean showingOverlay = false;
    public boolean showingHome = false;
    public Handler handler = new Handler();
    private ProgressDialog progressDialog = null;
    public SunPort[] currentSunPorts = null;

    public Typeface handFont = null;

	String[] menutitles;
	TypedArray menuIcons;

	// nav drawer title
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;

	public static DrawerLayout mDrawerLayout;
	public static ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

    private TextView mwhText;
    private TextView mwhTargetText;

	//private TextView home;
	private BroadcastReceiver listen = null;
	private AlertDialog msg = null;
	//static boolean waitingOnUserInfo = false;

	// public static Context APPLICATION_CONTEXT;
	//GoogleAnalytics tracker;
	//ConnectionDetector connectionDetector;
	//DeviceManager deviceManager = new DeviceManager();

	//SharedPreferences sh;
	//private static Intent registerReceiver;

    @Override
	protected void onPause() {
		if (msg != null) {
			msg.dismiss();
			msg = null;
		}
        VeeyahBattery.unregister(listen);
        listen = null;
		super.onPause();
	}

    @Override
    public void onDestroy() {
        VeeyahLogger.i("Veeyah Home", "onDestroy called");
        unbindDrawables(mDrawerLayout);
        hideDialog();
        super.onDestroy();
        VeeyahLogger.i("Veeyah Home", "onDestroy finished");
    }

    protected static void unbindDrawables(View view) {
        if (view == null) return;
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            if (!(view instanceof AdapterView<?>)) ((ViewGroup) view).removeAllViews();
        }
    }

	@Override
	protected void onStart() {
        try {
            super.onStart();
            // The rest of your onStart() code.
            //EasyTracker.getInstance(this).activityStart(this);
        } catch (Exception e) {
            VeeyahLogger.w("Veeyah Home_View", "Exception onStart: "+e.getClass()+": "+e.getMessage());
        }

	}

    @Override
    protected void onResume() {
        super.onResume();
        VeeyahLogger.i(TAG, "onResume()");
        registerBroadcastReceiver();
        requestDataRefresh();
        updateMWh();
    }

    protected void requestDataRefresh() {
        try {
            Intent loadData = new Intent(getApplicationContext(), VeeyahBattery.class);
            loadData.setAction(VeeyahBattery.REFRESH);
            startService(loadData);
            VeeyahLogger.i(TAG, "requested data refresh");
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception requesting data refresh: "+e.getClass()+": "+e.getMessage());
        }
    }

	@Override
	protected void onStop() {
		VeeyahBattery.unregister(listen);
		//EasyTracker.getInstance(this).activityStop(this); // Add this method.

		super.onStop();
	}

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            VeeyahLogger.i(TAG, "Home_View onCreate");
            if (getIntent().getBooleanExtra("EXIT", false)) {
                VeeyahLogger.i(TAG, "Will exit");
                finish();
                return;
            }
            mbm = new MatchBoxModel();
            if (VeeyahConstants.DEFAULT_DEVICE_ID.equals(mbm.getDeviceID())) {
                Intent firstPage = new Intent(getApplicationContext(), FirstActivity.class);
                startActivity(firstPage);
                finish();
                return;
            }
            if (getIntent().getIntExtra(VeeyahBattery.NOTIFICATION, 0) == VeeyahBattery.NOTE_ID) {
                mbm.incMetric(DeviceMetricsManager.APP_NOTIFICATION_LAUNCH);
            }

            setContentView(R.layout.activity_home);

            mTitle = mDrawerTitle = getTitle();

            menutitles = getResources().getStringArray(R.array.titles);
            menuIcons = getResources().obtainTypedArray(R.array.icons);

            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerList = (ListView) findViewById(R.id.slider_list);

            ArrayList<RowItem> rowItems = new ArrayList<RowItem>();

            for (int i = 0; i < menutitles.length; i++) {
                RowItem items = new RowItem(menutitles[i], menuIcons.getResourceId(
                        i, -1));
                rowItems.add(items);
            }

            menuIcons.recycle();

            CustomAdapter adapter = new CustomAdapter(getApplicationContext(), rowItems);

            mDrawerList.setAdapter(adapter);
            mDrawerList.setOnItemClickListener(new SlideitemListener());

            // enabling action bar app icon and behaving it as toggle button
            ActionBar actionBar = getSupportActionBar();
            if (actionBar == null) throw new ViaSolarException("Action bar is null!");
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);

            mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                    R.drawable.ic_drawer, R.string.app_name, R.string.app_name) {
                public void onDrawerClosed(View view) {
                    getSupportActionBar().setTitle(mTitle);
                    // calling onPrepareOptionsMenu() to show action bar icons
                    invalidateOptionsMenu();
                }

                public void onDrawerOpened(View drawerView) {
                    getSupportActionBar().setTitle(mDrawerTitle);
                    // calling onPrepareOptionsMenu() to hide action bar icons
                    invalidateOptionsMenu();
                }
            };

            mDrawerLayout.setDrawerListener(mDrawerToggle);

            // Configure menu button
            ImageButton menuImageButton = (ImageButton) findViewById(R.id.menuImageButton);
            menuImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                        mDrawerLayout.closeDrawer(mDrawerList);
                    } else {
                        mDrawerLayout.openDrawer(mDrawerList);
                    }
                }
            });

            homeFrame = (RelativeLayout) findViewById(R.id.scv);

            ImageView helpImageButton = (ImageView) findViewById(R.id.helpImageButton);
            helpImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayHelp();
                }
            });
            ImageView earthImageButton = (ImageView) findViewById(R.id.earth_home);
            earthImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayHelp();
                }
            });

            ImageButton shareImageButton = (ImageButton) findViewById(R.id.shareImageButton);
            ImageButton phoneImageButton = (ImageButton) findViewById(R.id.phoneImageButton);
            ImageButton plugImageButton = (ImageButton) findViewById(R.id.plugSolarImageButton);
            ImageButton settingsImageButton = (ImageButton) findViewById(R.id.settingsImageButton);

            shareImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    share();
                }
            });
            phoneImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectNavDrawerItem(NAV_DRAWER_ITEM_PHONE);
                }
            });
            plugImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickPlugIcon();
                }
            });
            settingsImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectNavDrawerItem(Home_View.NAV_DRAWER_ITEM_SETTINGS);
                }
            });

            // Set help tip longClickListeners
            setHelpLongClickListener(shareImageButton, "Share this app!");
            setHelpLongClickListener(plugImageButton, "View your phone's info.");
            setHelpLongClickListener(plugImageButton, "View your SunPort's info.");
            setHelpLongClickListener(settingsImageButton, "View the app settings.");

            handFont = Typeface.createFromAsset(getAssets(), "fonts/HandOfSeanPro.ttf");

            TextView tagline = (TextView) findViewById(R.id.tagline);
            tagline.setTypeface(handFont);

            mwhText = (TextView) findViewById(R.id.totalmwh);
            mwhTargetText = (TextView) findViewById(R.id.targetmwh);
            updateMWh();

            if (this.getIntent() != null) {
                if (VeeyahBattery.NOTIFICATION.equals(this.getIntent().getType())) {
                    VeeyahLogger.i(TAG, "Loaded from notification");
                    selectNavDrawerItem(NAV_DRAWER_ITEM_PHONE);
                } else if (GET_SUNJOULES_TYPE.equals(this.getIntent().getType())) {
                    VeeyahLogger.i(TAG, "Get more SunJoules after watching video.");
                    boolean sunport = this.getIntent().getBooleanExtra(VideoPlayer.SUNPORT_LABEL, false);
                    int oldVol = this.getIntent().getIntExtra(VideoPlayer.MUTE_LABEL, -12);
                    boolean mute = false;
                    if (oldVol > -1) {
                        mute = true;
                        AudioManager am = (AudioManager) ViaSolar.getContext().getSystemService(Context.AUDIO_SERVICE);
                        am.setStreamVolume(AudioManager.STREAM_MUSIC, oldVol, 0);
                    }
                    if (sunport) {
                        if (plugFrag == null) VeeyahLogger.w(TAG, "PlugFragment is null after watching refill video!");
                        if (plugFrag.currentPort != null) {
                            Intent refillPlug = new Intent();
                            refillPlug.setClass(ViaSolar.getContext(), VeeyahBattery.class);
                            refillPlug.setAction(VeeyahBattery.FILL_BLE);
                            refillPlug.putExtra(VeeyahBattery.CURRENT_SUNPORT, plugFrag.currentPort);
                            startService(refillPlug);
                        } else {
                            VeeyahLogger.i(TAG, "plugfrag.currentport is null on refill operation");
                        }
                    } else {
                        getMoreSunJoules();
                        if (phoneFrag == null) phoneFrag = new Phone_Fragment();
                        phoneFrag.playNextSound = !mute;
                        selectNavDrawerItem(Home_View.NAV_DRAWER_ITEM_PHONE);
                    }
                } else if (VeeyahBattery.CONNECT_BLE.equals(this.getIntent().getType())) {
                    VeeyahLogger.i(TAG, "Go to plug screen for newly registered plug.");
                    //showPlugFrag((SunPort) this.getIntent().getParcelableExtra(VeeyahBattery.CURRENT_SUNPORT));
                }
            }

            registerBroadcastReceiver();

            requestDataRefresh();

            /*if (mbm.isFirstUse()) {
                displayHelp();
                //mbm.setFirstUse(false);
            }*/
        } catch (Exception e) {
            VeeyahLogger.w("Veeyah Home View", "Exception on create: "+e.getClass()+": "+e.getMessage());
        }
	}

    private void registerBroadcastReceiver() {
        if (listen != null) return;
        listen = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {

                    VeeyahLogger.i(TAG, "Got broadcast: "+intent.getAction());
                    if (VeeyahBattery.REFRESH.equals(intent.getAction())) {
                            /*if (mainFrag != null) {
                                VeeyahLogger.i(TAG, "onCreate Home_View animation");
                                mainFrag.animateRingView();
                            }*/
                    } else if (VeeyahBattery.SCAN_BLE.equals(intent.getAction())) {
                        try {
                            VeeyahLogger.i(TAG, "Got BLE Scan response.");
                            final SunPort[] sunports = (SunPort[]) intent.getParcelableArrayExtra(VeeyahBattery.NEARBY_PLUGS);
                            if (sunports == null || sunports.length < 1) {
                                VeeyahLogger.i(TAG, "Got Empty BLE scan response.");
                                //showToast("No SunPort devices have been detected nearby.");
                                //updateDisplay(NAV_DRAWER_ITEM_PHONE);
                                ignoreLastRequest = true;
                                networkRequestPending = false;
                                VeeyahLogger.i(TAG, "select Nav drawer Home_View animation");
                                //phoneFrag.animateRingView();
                                return;
                            } else if (sunports.length == 1) {
                                if (sunports[0] == null) VeeyahLogger.i(TAG, "SunPort is null");
                                currentSunPorts = sunports;
                                plugFrag.currentPortPos = 0;
                                showPlugFrag(sunports[0]);
                                /*Intent spIntent = new Intent();
                                spIntent.setClass(ViaSolar.getContext(), VeeyahBattery.class);
                                spIntent.setAction(VeeyahBattery.CONNECT_BLE);
                                spIntent.putExtra(VeeyahBattery.BLE_ADDRESS, devices.get(0));
                                startService(spIntent);*/
                            } else if (sunports.length > 1) { //multiple sunports returned
                                VeeyahLogger.i(TAG, "Got BLE scan response with multiple elts.");
                                currentSunPorts = sunports;
                                /*ArrayList<TwoColumnRowItem> rowItems = new ArrayList<TwoColumnRowItem>();
                                for (int i=0; i<sunports.length; i++) {
                                    SunPort device = sunports[i];
                                    TwoColumnRowItem rowItem = new TwoColumnRowItem(device.getRawAddress(), "R");
                                    String nickname = mbm.checkPlugName(device.getRawAddress());
                                    if (nickname != null && !"".equals(nickname)) {
                                        rowItem.setLeftText(nickname);
                                    }
                                    rowItems.add(rowItem);
                                }*/
                                try {
                                    // Dialog to pick BLE device
                                    /*LayoutInflater inflater = LayoutInflater.from(self);
                                    View bleSelectorView = inflater.inflate(R.layout.ble_selector, null);

                                    if (bleSelectorView == null) {
                                        throw new ViaSolarException("The bleSelectorView is null!");
                                    }

                                    VeeyahLogger.i(TAG, "Begin generate BLE Dialog");
                                    final Dialog dialog = new Dialog(self);
                                    dialog.setContentView(bleSelectorView);
                                    dialog.setTitle("Choose which SunPort to Read");
                                    dialog.setCancelable(true);
                                    VeeyahLogger.i(TAG, "Generate BLE List");
                                    ListView listview = (ListView) bleSelectorView.findViewById(R.id.listView1);
                                    TwoColumnListAdapter adapter = new TwoColumnListAdapter(self, rowItems);
                                    adapter.setLeftClickListener(new View.OnClickListener() {
                                        public void onClick(View v) {
                                            try {
                                                /*Intent intent = new Intent();
                                                intent.setClass(ViaSolar.getContext(), VeeyahBattery.class);
                                                intent.setAction(VeeyahBattery.CONNECT_BLE);
                                                TextView tv = (TextView) v;
                                                String vText = tv.getText().toString();
                                                int colPos = vText.indexOf(": ");
                                                int pos = Integer.parseInt(vText.substring(0, colPos));
                                                intent.putExtra(VeeyahBattery.BLE_ADDRESS, devices.get(pos));
                                                startService(intent);
                                                dialog.dismiss();*/
                                                /*TextView tv = (TextView) v;
                                                String vText = tv.getText().toString();
                                                int colPos = vText.indexOf(": ");
                                                int pos = Integer.parseInt(vText.substring(0, colPos));
                                                showPlugFrag(sunports[pos]);
                                            } catch (Exception e) {
                                                VeeyahLogger.w(TAG, "Exception handlind BLE list click: "+e.getClass()+": "+e.getMessage());
                                            }
                                        }
                                    });
                                    listview.setAdapter(adapter);

                                    /*listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
                                        }
                                    });*/
                                    //dialog.show();
                                    int mostRecentPos = 0;
                                    long mostRecentTime = 0;
                                    for (int i=0; i<sunports.length; i++) {
                                        SunPort device = sunports[i];
                                        if (device != null) {
                                            if (device.getLatestTime() > mostRecentTime) {
                                                mostRecentPos = i;
                                                mostRecentTime = device.getLatestTime();
                                            }
                                        }
                                    }
                                    VeeyahLogger.i(TAG, "show plug frag for multi sunport");
                                    plugFrag.currentPortPos = mostRecentPos;
                                    showPlugFrag(sunports[mostRecentPos]);
                                } catch (Exception e) {
                                    VeeyahLogger.w(TAG, "Exception selecting BLE device: "+e.getClass()+": "+e.getMessage());
                                }
                            }
                        } catch (Exception e) {
                            VeeyahLogger.w(TAG, "Exception processing BLE scan results: "+e.getClass()+": "+e.getMessage());
                        }
                    } else if (VeeyahBattery.CONNECT_BLE.equals(intent.getAction())) {
                        VeeyahLogger.i(TAG, "Connect BLE");
                        //showPlugFrag((SunPort) intent.getParcelableExtra(VeeyahBattery.CURRENT_SUNPORT));
                    } else if (VeeyahBattery.REFRESH_BLE.equals(intent.getAction())) {
                        VeeyahLogger.i(TAG, "Refresh BLE");
                        sunportDetected = true;
                        SunPort refreshed = intent.getParcelableExtra(VeeyahBattery.CURRENT_SUNPORT);
                        updateSunPortArray(refreshed);
                        if (plugFrag == null) plugFrag = new Plug_Fragment();
                        if (refreshed != null && plugFrag.currentPort != null) {
                            if (plugFrag.currentPort.getAddress().equals(refreshed.getAddress())) {
                                plugFrag.currentPort = refreshed;
                                plugFrag.refreshGUI();
                            }
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ImageButton plugBtn = (ImageButton) findViewById(R.id.plugSolarImageButton);
                                plugBtn.setImageResource(R.drawable.icon_plug_white);
                            }
                        });
                    } else if (VeeyahBattery.FILL_BLE.equals(intent.getAction())) {
                        if (plugFrag == null) plugFrag = new Plug_Fragment();
                        final String msg = intent.getStringExtra(VeeyahBattery.PROGRESS_MSG);
                        if (progressDialog != null) {
                            if (msg != null && progressDialog.isShowing()) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressDialog.setMessage(msg);
                                    }
                                });
                            }
                        }
                        SunPort sp = intent.getParcelableExtra(VeeyahBattery.CURRENT_SUNPORT);
                        if (sp != null) {
                            updateSunPortArray(sp);
                            networkRequestPending = false;
                            hideDialog();
                        }
                    } else {
                        VeeyahLogger.i(TAG, "Received unknown broadcast: "+intent.getAction());
                    }
                } catch (Exception e) {
                    VeeyahLogger.w(TAG,
                            "Exception receiving local broadcast '"
                                    + intent.getAction()
                                    + "' in MainActivity: " + e.getClass()
                                    + ": " + e.getMessage()
                    );
                }
            }
        };

        VeeyahBattery.registerLocalBroadcast(listen, this);
        VeeyahLogger.i(TAG, "Broadcast Receiver is listening. . .");
    }

    private void updateSunPortArray(SunPort sp) {
        try {
            if (currentSunPorts == null) return;
            String rawAddr = sp.getRawAddress();
            if (rawAddr == null) throw new ViaSolarException("Received SunPort with null address!");
            for (int i=0; i<currentSunPorts.length; i++) {
                if (currentSunPorts[i] ==null) continue;
                if (rawAddr.equals(currentSunPorts[i].getRawAddress())) {
                    currentSunPorts[i] = sp;
                    break;
                }
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception updating sunport array"+e.getClass()+": "+e.getMessage());
        }
    }

    private void showPlugFrag(SunPort sp) {
        try {
            networkRequestPending = false;
            if (plugFrag == null) plugFrag = new Plug_Fragment();
            if (sp == null) throw new ViaSolarException("showPlugFrag called with null plug");
            plugFrag.currentPort = sp;
            VeeyahLogger.i(TAG, "Energy: " + plugFrag.currentPort.getEnergy() + ", Power: " + plugFrag.currentPort.getPower());
            updateDisplay(NAV_DRAWER_PLUG_SOLAR);
            plugFrag.refreshGUI();
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception showing plug fragment: "+e.getClass()+": "+e.getMessage());
            showToast("The connection with that SunPort failed.\nPlease make sure the SunPort is plugged in nearby and try again.");
            updateDisplay(NAV_DRAWER_ITEM_HOME);
        }
    }

    protected void getMoreSunJoules() {
        try {
            long sj = mbm.getAvailableSJ();
            if (sj < 10000) {
                long diff = 10000 - sj;
                long cache = mbm.getCachedSJ();
                if (cache > diff) {
                    if (mbm.getAvailableSJ() < 1) mbm.setStartTime(mbm.printTime());
                    mbm.setCachedSJ(cache-diff);
                    mbm.setAvailableSJ(sj+diff);
                    showToast("Thanks for being green!");
                } else if (cache > 0) {
                    if (mbm.getAvailableSJ() < 1) mbm.setStartTime(mbm.printTime());
                    mbm.setCachedSJ(0);
                    mbm.setAvailableSJ(sj+cache);
                    showToast("Thanks for being green!");
                } else {
                    GetSunJoulesManager getSJ = new GetSunJoulesManager();
                    getSJ.getSunJoules(mbm.getDeviceID(), (int) (100 - sj / 100), new WebServiceManager(), this, false);
                }
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Getting more sunjoules: "+e.getClass()+": "+e.getMessage());
        }
    }

    protected void hideHelp() {
        try {
            RelativeLayout helpOverlayLayout = (RelativeLayout) findViewById(R.id.homeOverlayRelativeLayout);
            if (View.VISIBLE == helpOverlayLayout.getVisibility()) {
                helpOverlayLayout.setVisibility(View.GONE);
                showingOverlay = false;
                setTitle(getString(R.string.app_name));
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception hiding help overlay: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void displayHelp() {
        try {
            // Show help overlay layout
            if (mbm == null) mbm = new MatchBoxModel();
            mbm.incMetric(DeviceMetricsManager.BTN_HELP);
            RelativeLayout helpOverlayLayout = (RelativeLayout) findViewById(R.id.homeOverlayRelativeLayout);
            showingOverlay = true;
            helpOverlayLayout.setVisibility(View.VISIBLE);
            setTitle(getString(R.string.app_name) + " Help");
            helpOverlayLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideHelp();
                }
            });

            TextView helpText1 = (TextView) findViewById(R.id.homeOverlayTxt1);
            helpText1.setTypeface(handFont);
            TextView helpText2 = (TextView) findViewById(R.id.homeOverlayTxt2);
            helpText2.setTypeface(handFont);

            TextView whatAreSunJoulesButton = (TextView) findViewById(R.id.homeOverlayTxt3);
            whatAreSunJoulesButton.setTypeface(handFont);
            whatAreSunJoulesButton.setText(Html.fromHtml("<u>SunJoules</u><br/>"));
            whatAreSunJoulesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mbm == null) mbm = new MatchBoxModel();
                    mbm.incMetric(DeviceMetricsManager.BTN_WHATSJ);
                    LayoutInflater factory1 = LayoutInflater.from(self);
                    final View textEntryView1 = factory1.inflate(R.layout.dialog_what_are_sun_joules, null);
                    TextView link = (TextView) textEntryView1.findViewById(R.id.sjtxt05);
                    link.setText(Html.fromHtml("Subject to change - for more info see <a href=\"http://rechoice.org\">rechoice.org</a>"));
                    link.setMovementMethod(LinkMovementMethod.getInstance());
                    AlertDialog.Builder alert1 = new AlertDialog.Builder(self, R.style.NoPadding);
                    //alert1.setTitle(" ");  //need to have some text in title for icon to appear
                    alert1.setPositiveButton("OK", null);
                    //AlertDialog dialog = alert1.create();
                    alert1.setView(textEntryView1);
                    //alert1.setIcon(R.drawable.sj_header);
                    alert1.show();
                }
            });
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception displaying help overlay: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void showDialog() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null) {
                    progressDialog.show();
                }
            }
        });
    }

    private void setDialogText(final String text) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null) {
                    progressDialog.setMessage(text);
                }
            }
        });
    }

    private void hideDialog() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null) {
                    if (progressDialog.isShowing()) progressDialog.dismiss();
                }
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        try {
            if (keyCode == KeyEvent.KEYCODE_MENU) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
                return true;
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception inside onKeyUp: "+e.getClass()+": "+e.getMessage());
        }
        return super.onKeyUp(keyCode, event);
    }

	class SlideitemListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			selectNavDrawerItem(position);
            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) mDrawerLayout.closeDrawer(Gravity.LEFT);
		}
	}

    protected void showSunportRefillTimer() {
        try {
            networkRequestPending = true;

            // Before committing, show alert dialog with progress bar and then commit fragment via updateDisplay()
            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle(getString(R.string.app_name));
            progressDialog.setMessage("Contacting CloudSolar...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);

            showDialog();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        int timer = 0;
                        while (networkRequestPending) {
                            Thread.sleep(50);
                            timer++;
                            if (timer > 60000/50) {
                                showToast("The servers are not responding quickly.\nPlease try again from a location with better network access.");
                                hideDialog();
                                return;
                            }
                        }
                        hideDialog();

                    } catch (InterruptedException e) {
                        VeeyahLogger.i(TAG, "SunPort reload timer interrupted: "+e.toString());
                    }
                }
            }).start();
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception displaying sunport reload timer dialog: "+e.getClass()+": "+e.getMessage());
        }
    }
	
	public void selectNavDrawerItem(int position) {
		final int pos = position;
		final Handler handler = new Handler();
		
		// Before committing, show alert dialog with progress bar and then commit fragment via updateDisplay()
		/*progressDialog = new ProgressDialog(this);
		progressDialog.setTitle(getString(R.string.app_name));
		progressDialog.setMessage("Loading...");
		progressDialog.setIndeterminate(true);
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.setCancelable(false);

        showDialog();*/

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {

                    if (pos == NAV_DRAWER_ITEM_PHONE) { //main page MySummary Request
                        if (mySummary == null) {
                            /*networkRequestPending = true;
                            MySummaryManager mySummaryManager = new MySummaryManager();
                            mySummaryManager.getUserSummary(mbm.getDeviceID(), webServices, self);*/

                            networkRequestPending = false;
                        }
                        showingHome = false;
                    } else if (pos == NAV_DRAWER_ITEM_SETTINGS) { //settings page
                        networkRequestPending = false;
                        if ((System.currentTimeMillis() - lastSettingCheck) > SETTINGS_CHECK_TIMEOUT) {
                            lastSettingCheck = System.currentTimeMillis();
                            DisplaySettingsManager displaySettings = new DisplaySettingsManager();
                            displaySettings.displaySettings(mbm.getDeviceID(), new WebServiceManager(), self);
                        }
                        showingHome = false;
                    } else if (pos == NAV_DRAWER_ITEM_HOME) {
                        showingHome = false;
                    } else if (pos == NAV_DRAWER_PLUG_SOLAR) {
                        networkRequestPending = false;
                        /*if (plugFrag != null) {
                            if (plugFrag.currentPort != null) {
                                if ((System.currentTimeMillis() - plugFrag.currentPort.getEnergyTime()) < VeeyahBattery.SUNPORT_HOLD_TIME) {
                                    networkRequestPending = false;
                                    updateDisplay(NAV_DRAWER_PLUG_SOLAR);
                                    plugFrag.refreshGUI();
                                }
                            }
                        }*/
                        showingHome = false;
                        Intent intent = new Intent();
                        intent.setClass(ViaSolar.getContext(), VeeyahBattery.class);
                        intent.setAction(VeeyahBattery.SCAN_BLE);
                        startService(intent);
                    } else {
                        networkRequestPending = false;
                    }

                    /*int timer = 0;
			        while (networkRequestPending) {
			        	Thread.sleep(50);
                        timer++;
                        if (timer > 60000/50) {
                            showToast("The servers are not responding quickly.\nPlease try again from a location with better network access.");
                            updateDisplay(NAV_DRAWER_ITEM_HOME);
                            hideDialog();
                            showingHome = true;
                            //VeeyahLogger.i(TAG, "select Nav drawer Home_View animation");
                            //phoneFrag.animateRingView();
                            return;
                        }
			        }*/

                    if (ignoreLastRequest) { //set ignoreLastRequest to true if you don't want the chosen screen to load (for example, no BLE devices detected)
                        ignoreLastRequest = false;
                        hideDialog();
                        return;
                    }
					
					handler.post(new Runnable() {
						@Override
						public void run() {
							// Dismiss the progress dialog
							updateDisplay(pos);
						}
					});
                    hideDialog();
					
				} catch (Exception e) {
					VeeyahLogger.i(TAG, "Exception in fragment change timer: "+e.getClass()+": "+e.getMessage());
				}				
			}
		}).start();
	}

    protected void showToast(final String msg) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(
                            self,
                            msg,
                            Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception showing toast: "+e.getClass()+": "+e.getMessage());
        }
    }

    public void refreshView(Object data) {
        if (data == null) {
            VeeyahLogger.w(TAG, "Null data Received!");
            return;
        }

        try {
            if (GetSunJoulesResponse.class.equals(data.getClass())) {
                if (phoneFrag == null) phoneFrag = new Phone_Fragment();
                VeeyahLogger.i(TAG, "refreshView Home_View animation");
                phoneFrag.animateRingView();
                showToast("Thanks for being green!");

                //refresh VeeyahBattery to update notification
                Intent loadData = new Intent(getApplicationContext(), VeeyahBattery.class);
                loadData.setAction(VeeyahBattery.REFRESH);
                startService(loadData);
            } else if (MySummaryResponse.class.equals(data.getClass())) {
                if (phoneFrag == null) phoneFrag = new Phone_Fragment();
                phoneFrag.updateHomeView((MySummaryResponse) data);
            } else if (DisplaySettingsResponse.class.equals(data.getClass())) {
                if (settingFrag == null) settingFrag = new Setting_Fragment();
                settingFrag.setFields();
            }
            networkRequestPending = false;
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception refreshing view: "+e.getClass()+": "+e.getMessage());
        }

    }

    public void showError(String msg) {
        //TODO: goto offline mode
        networkRequestPending = false;
    }

    final static int NAV_DRAWER_ITEM_SETTINGS = 0;
    final static int NAV_DRAWER_ITEM_SHARE = 1;
    final static int NAV_DRAWER_ITEM_HOME = 2;
    final static int NAV_DRAWER_ITEM_PHONE = 3;
    //final static int NAV_DRAWER_ABOUT = 4;
    final static int NAV_DRAWER_PLUG_SOLAR = 4;
	//final static int NAV_DRAWER_ITEM_USAGE_DETAILS = 6;
	//final static int NAV_DRAWER_ITEM_FEEDBACK = 7;
	//final static int NAV_DRAWER_ITEM_ABOUT = 4;

	public void updateDisplay(int position) {
        try {
            // Update and commit to the fragment
            switch (position) {
                case NAV_DRAWER_ITEM_PHONE:
                    // Main
                    if (phoneFrag == null) phoneFrag = new Phone_Fragment();
                    if (fragment == phoneFrag) return;
                    if (mbm == null) mbm = new MatchBoxModel();
                    mbm.incMetric(DeviceMetricsManager.BTN_MAIN);
                    fragment = phoneFrag;
                    break;
                case NAV_DRAWER_ITEM_SHARE:
                    // Share app
                    share();
                    break;
                case NAV_DRAWER_ITEM_SETTINGS:
                    // Settings
                    if (settingFrag == null) settingFrag = new Setting_Fragment();
                    if (fragment == settingFrag) return;
                    if (mbm == null) mbm = new MatchBoxModel();
                    mbm.incMetric(DeviceMetricsManager.BTN_SETTINGS);
                    fragment = settingFrag;
                    break;
                case NAV_DRAWER_PLUG_SOLAR:
                    //Plug
                    if (plugFrag == null) plugFrag = new Plug_Fragment();
                    if (fragment == plugFrag) return;
                    if (mbm == null) mbm = new MatchBoxModel();
                    fragment = plugFrag;
                    break;
                /*case NAV_DRAWER_ABOUT:
                    // about
                    Intent firstPage = new Intent(getApplicationContext(), FirstActivity.class);
                    startActivity(firstPage);
                    finish();
                    return;*/
                default: //home
                    if (homeFrame == null) homeFrame = (RelativeLayout) findViewById(R.id.scv);
                    if (!homeFrame.isShown()) {
                        homeFrame.setVisibility(View.VISIBLE);
                    }
                    if (fragment != null) {
                        android.support.v4.app.FragmentManager
                                fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction().remove(fragment);
                        fragment = null;
                    }
                    return;
            }

            transitionFragment(position);

        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception updating display: "+e.getClass()+": "+e.getMessage());
        }

	}

    private void transitionFragment(int position) {
        try {
            if (fragment != null) {
                android.support.v4.app.FragmentManager
                        fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
                // update selected item and title
                if (position == NAV_DRAWER_ITEM_PHONE || position == NAV_DRAWER_ITEM_HOME || position == NAV_DRAWER_ITEM_SETTINGS) {
                    setTitle(menutitles[position]);
                }
                if (homeFrame == null) homeFrame = (RelativeLayout) findViewById(R.id.scv);
                if (homeFrame.isShown()) homeFrame.setVisibility(View.GONE);
            } else {
                // error in creating fragment
                VeeyahLogger.e(TAG, "Error in creating fragment");
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception updating fragment: "+e.getClass()+": "+e.getMessage());
        }
    }

    public void clickPlugIcon() {
        try {
            selectNavDrawerItem(Home_View.NAV_DRAWER_PLUG_SOLAR);
            //showToast("Coming soon. . .");
            /*if (mbm.getRegisteredPorts().isEmpty()) {
                registerPort();
            } else {
                selectNavDrawerItem(Home_View.NAV_DRAWER_PLUG_SOLAR);
            }*/
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception clicking plug icon: "+e.getClass()+": "+e.getMessage());
        }
    }

    public void feedback() {
        // Generate feedback email
        try {
            if (mbm == null) mbm = new MatchBoxModel();
            mbm.incMetric(DeviceMetricsManager.BTN_COMMENT);
            Intent feedbackEmailIntent = new Intent(Intent.ACTION_SEND);
            feedbackEmailIntent.setType("message/rfc822");
            feedbackEmailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"fun@veeyah.com"});
            feedbackEmailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name) + " App Feedback");
            feedbackEmailIntent.putExtra(Intent.EXTRA_TEXT, "Version: "+VeeyahConstants.VERSION_NUMBER+"\n<<put feedback here>>");
            startActivity(Intent.createChooser(feedbackEmailIntent, "Send us your feedback with. . ."));
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception sending feedback: "+e.getClass()+": "+e.getMessage());
        }
    }

    public void share() {
        // Share app
        try {
            if (mbm == null) mbm = new MatchBoxModel();
            mbm.incMetric(DeviceMetricsManager.BTN_SHARE);
            new Thread() {
                public void run() {
                    try {
                        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        //using EXTRA_SUBJECT forces MMS
                        //shareIntent.putExtra(Intent.EXTRA_SUBJECT, ctx.getString(R.string.app_name) + " App");
                        shareIntent.putExtra(Intent.EXTRA_TEXT, "My phone chooses solar energy from the smart grid!  Get the free app here: " + mbm.getShareLink());
                        mbm.incMetric(DeviceMetricsManager.APP_SHARES);
                        startActivity(Intent.createChooser(shareIntent, "Share this app using..."));
                    } catch (Exception e) {
                        VeeyahLogger.w(TAG, "Exception at inner thread sharing app: "+e.getClass()+": "+e.getMessage());
                    }
                }
            }.start();
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception sharing app: "+e.getClass()+": "+e.getMessage());
        }
    }

    public void registerPort() {
        final Intent registerPort = new Intent(self,
                RegisterPort.class);
        startActivity(registerPort);
        finish();
    }

    private void updateMWh() {
        try {
            if (mwhText == null || mwhTargetText == null) return;
            if (mbm == null) mbm = new MatchBoxModel();
            int total = mbm.getMWh();
            int target = mbm.getMWhTarget();
            String date = mbm.getMWhUpdateDate();
            if (total < 1 || target < 1 || "".equals(date)) return;

            String mwhStr = "Community Impact: "+total+" MWh\nas of "+date;
            String targetStr = "Next Goal: "+target+" MWh";

            mwhText.setText(mwhStr);
            mwhTargetText.setText(targetStr);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception updating MWh: "+e.getClass()+": "+e.getMessage());
        }
    }

    /*public void showUsageDetails() {
        //TODO: need to determine whether usage details are for phone or port, which port
        try {
            if (detailsFrag == null) detailsFrag = new Details_Fragment();
            if (mbm == null) mbm = new MatchBoxModel();
            mbm.incMetric(DeviceMetricsManager.BTN_DETAIL);
            fragment = detailsFrag;
            transitionFragment(-1);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception showing usage details: "+e.getClass()+": "+e.getMessage());
        }
    }*/

	@Override
	public void setTitle(CharSequence title) {
        try {
            mTitle = title;
            ActionBar actionBar = getSupportActionBar();
            if (actionBar == null) throw new ViaSolarException("Action bar is null!");
            actionBar.setTitle(mTitle);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception setting title: "+e.getClass()+": "+e.getMessage());
        }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.splash_screen, menu);
		return true;
	}

	 public boolean onOptionsItemSelected(MenuItem item) {
	  // toggle nav drawer on selecting action bar app icon/title
	          if (mDrawerToggle.onOptionsItemSelected(item)) {
	                     return true;
	              }
	             // Handle action bar actions click
	             switch (item.getItemId()) {
	             /*case R.id.action_settings:
                     return true;*/
	                    default :
	                                 return super.onOptionsItemSelected(item);
	                }
	 }

	/***
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		mDrawerLayout.isDrawerOpen(mDrawerList);
		//menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		if (mDrawerToggle != null) mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggles
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

    protected void setHelpLongClickListener(View view, final String helpString) {
        // Set the help string for this View
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showToast(helpString);
                return true;
            }
        });
    }

	@Override
	public void onBackPressed() {
		// On Back Button pressed, go back to Main or close
        try {
            if (mDrawerLayout == null) throw new ViaSolarException("Menu is null!");
            if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                mDrawerLayout.closeDrawer(mDrawerList);
                return;
            }
            if (showingOverlay) {
                if (plugFrag != null) plugFrag.hideHelp();
                if (phoneFrag != null) phoneFrag.hideHelp();
                hideHelp();
                return;
            }
            if (networkRequestPending) {
                selectNavDrawerItem(NAV_DRAWER_ITEM_HOME);
                networkRequestPending = false;
                return;
            }
            if (!showingHome) {
                selectNavDrawerItem(NAV_DRAWER_ITEM_HOME);
                return;
            }
            exitApp();
            /*if (!"Main".equals(actionBar.getTitle())) {
                selectNavDrawerItem(NAV_DRAWER_ITEM_HOME);
            } else {
                RelativeLayout helpOverlayLayout = (RelativeLayout) findViewById(R.id.helpOverlayRelativeLayout);
                if (helpOverlayLayout != null) {
                    if (helpOverlayLayout.getVisibility() == View.VISIBLE) {
                        helpOverlayLayout.setVisibility(View.GONE);
                    } else {
                        exitApp();
                    }
                } else {
                    exitApp();
                }
            }*/
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception on back pressed: "+e.getClass()+": "+e.getMessage());
        }
	}

    private void exitApp() {
        VeeyahLogger.i(TAG, "Exit app");
        Intent intent = new Intent(getApplicationContext(), Home_View.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);
        startActivity(intent);
    }
	
}
