package com.veeyah.measure;

import com.veeyah.measure.VeeyahBattery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootListener extends BroadcastReceiver {
	public void onReceive(Context context, Intent intent) {
		if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
			Intent myStarterIntent = new Intent(context, VeeyahBattery.class);
			myStarterIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			myStarterIntent.setAction(Intent.ACTION_BOOT_COMPLETED);
			context.startService(myStarterIntent);
		}
	}
}
