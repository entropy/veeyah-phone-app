package com.veeyah.measure;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

public class DrawView extends View {
	Paint paint = new Paint();
	Path pat = new Path();

	public DrawView(Context context) {
		super(context);
		setFocusable(true);

		// context.clearRect(70,1160,1600,450);
		// pat.addRect(50,160,250,1600,Path.Direction.CW);
	}

	public DrawView(Context con, AttributeSet atts) {
		super(con, atts);
	}

	@Override
	public void onDraw(Canvas canvas) {
		int i = 0;
		Bitmap firstUserBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.sample_user);

		Rect firstUserSource = new Rect(0, 0, 100, 100);

		Rect firstUserBitmapRect = new Rect(25, 220,125, 320);
		canvas.drawBitmap(firstUserBitmap, firstUserSource,
				firstUserBitmapRect, null);

		// Bitmap firstUserBitmap = BitmapFactory.decodeResource(getResources(),
		// R.drawable.eample_user_icon);
		//
		// Rect firstUserSource = new Rect(0, 0, 160, 160);

		Rect secondUserBitmapRect = new Rect(128, 150, 228, 250);
		canvas.drawBitmap(firstUserBitmap, firstUserSource,
				secondUserBitmapRect, null);

		Rect fourthUserBitmapRect = new Rect(373, 105, 473, 205);
		canvas.drawBitmap(firstUserBitmap, firstUserSource,
				fourthUserBitmapRect, null);

		Rect fifthUserBitmapRect = new Rect(490, 150, 590, 250);
		canvas.drawBitmap(firstUserBitmap, firstUserSource,
				fifthUserBitmapRect, null);

		Rect sixUserBitmapRect = new Rect(590, 220, 690, 320);
		canvas.drawBitmap(firstUserBitmap, firstUserSource, sixUserBitmapRect,
				null);

		Bitmap sideUserBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.sample_user);

		Rect sideUserSource = new Rect(0, 0, 160, 160);

		Rect sideUserBitmapRect = new Rect(240, 105, 350, 205);
		canvas.drawBitmap(sideUserBitmap, sideUserSource, sideUserBitmapRect,
				null);
		RectF rectF = new RectF(23, 218,127, 322);
		Paint p1 = new Paint();
		// smooths
		p1.setAntiAlias(true);
		p1.setColor(Color.RED);
		p1.setStyle(Paint.Style.STROKE);
		p1.setStrokeWidth(5);
		canvas.drawArc(rectF, 0, 360, false, p1);

		RectF rectF2 = new RectF(126, 148, 230, 252);

		canvas.drawArc(rectF2, 0, 360, false, p1);
		RectF rectF3 = new RectF(238, 103, 352, 207);

		canvas.drawArc(rectF3, 0, 360, false, p1);

		RectF rectF4 = new RectF(371, 103, 475, 207);

		canvas.drawArc(rectF4, 0, 360, false, p1);
		
		RectF rectF5 = new RectF(488, 148, 592, 252);

		canvas.drawArc(rectF5, 0, 360, false, p1);
		
		RectF rectF6 = new RectF(588, 218, 692, 322);

		canvas.drawArc(rectF6, 0, 360, false, p1);

	}
}