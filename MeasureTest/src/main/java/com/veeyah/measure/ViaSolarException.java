package com.veeyah.measure;

public class ViaSolarException extends Exception {

    private String msg;
    static final long serialVersionUID = 22389;

    public ViaSolarException(String message) {
        super();
        this.msg = message;
    }

    public String getMessage() {
        return msg;
    }
}
