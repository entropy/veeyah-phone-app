package com.veeyah.measure.manager;

import android.util.Log;
import android.util.Xml;

import com.veeyah.measure.MatchBoxModel;
import com.veeyah.measure.VeeyahConstants;
import com.veeyah.utility.VeeyahMetric;
import org.xmlpull.v1.XmlSerializer;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.util.Iterator;

public class DeviceMetricsManager {

    private final static String DEVICE_METRICS_REQUEST = "DeviceMetrics";
    public final static String PHONE_CAPACITY = "phone.capacity";
    public final static String VIDEO_PREFIX = "res.video";
    public final static String VIDEO_CURRENT_SLOT = "res.video.slot.current";
    public final static String APP_LAUNCH = "app.launch";
    public final static String APP_NOTIFICATION_LAUNCH = "app.notificationLaunch";
    public final static String APP_SHARES = "app.shares";
    public final static String SCR_LOGIN = "scr.login";
    public final static String SCR_MAIN = "scr.main";
    public final static String SCR_INFO = "scr.info";
    public final static String SCR_SETTINGS = "scr.settings";
    public final static String BTN_REGISTER = "btn.register";
    public final static String BTN_MAIN = "btn.main";
    public final static String BTN_RELOAD = "btn.reload";
    public final static String BTN_HELP = "btn.help";
    public final static String BTN_WHATSJ = "btn.whatsj";
    public final static String BTN_COMMENT = "btn.comment";
    public final static String BTN_SHARE = "btn.share";
    public final static String BTN_DETAIL = "btn.detail";
    public final static String BTN_SETTINGS = "btn.settings";
    public final static String BTN_UPDATE = "btn.update";
    public final static String CHK_TERMS = "chk.terms";
    public final static String CHK_STICKY = "chk.sticky";

    public void sendMetricsReport(WebServiceManager wsm, VideoFileManager vfm) {
        try {
            MatchBoxModel mbm = new MatchBoxModel();
            Iterator<VeeyahMetric> metrics = mbm.getDeviceMetrics(vfm).iterator();
            if (!metrics.hasNext()) return;
            String deviceID = mbm.getDeviceID();
            if (deviceID == null || "".equals(deviceID)) {
                deviceID = VeeyahConstants.DEFAULT_DEVICE_ID;
            }
            XmlSerializer serializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            try {

                serializer.setOutput(writer);
                serializer.startDocument("UTF-8", true);
                serializer.startTag("", "soapenv:Envelope");
                serializer.attribute("", "xmlns:soapenv",
                        "http://schemas.xmlsoap.org/soap/envelope/");
                serializer.attribute("", "xmlns:vr",
                        "http://veeyah.com/VeeyahReport");

                serializer.startTag("", "soapenv:Header");
                serializer.endTag("", "soapenv:Header");
                serializer.startTag("", "soapenv:Body");
                serializer.startTag("", "vr:DeviceMetrics");
                serializer.startTag("", "RequestTime");
                serializer.text(getTime());
                serializer.endTag("", "RequestTime");
                serializer.startTag("", "DeviceID");
                serializer.text(deviceID);
                serializer.endTag("", "DeviceID");
                while (metrics.hasNext()) {
                    VeeyahMetric metric = metrics.next();
                    serializer.startTag("", "VeeyahMetric");
                    serializer.startTag("", "KeyName");
                    serializer.text(metric.getName());
                    serializer.endTag("", "KeyName");
                    serializer.startTag("", "Quantity");
                    serializer.text(metric.getValue()+"");
                    serializer.endTag("", "Quantity");
                    serializer.endTag("", "VeeyahMetric");
                }
                serializer.endTag("", "vr:DeviceMetrics");
                serializer.endTag("", "soapenv:Body");
                serializer.endTag("", "soapenv:Envelope");
                serializer.endDocument();
                String msg = writer.toString();
                Log.i("Request", msg);
                WebServiceRequest wsr = new WebServiceRequest(DEVICE_METRICS_REQUEST, VeeyahConstants.WEBSERVICE_DOMAIN+"/VeeyahReport", msg, new ErrorReportManager());
                wsm.makeWebServiceRequest(wsr, true, true);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } catch (Exception e) {
            Log.i("DeviceMetricsManager", "Exception sending DeviceMetrics: " + e.getClass()+ ": " + e.getMessage());
        }
    }

    private static String getTime() {
        java.util.Date date = new java.util.Date();
        return ((new Timestamp(date.getTime())).toString());
    }

    public void onServiceComplete(String msg) {
        //do nothing
    }

    public void onServiceFault(String msg) {
        Log.e("DeviceMetricsManager", "Fault reporting metrics.  The message is: "+msg);
    }
}
