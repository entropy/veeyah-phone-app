package com.veeyah.measure.manager;

public interface WebServiceReceiver {

    public void onServiceComplete(String msg);
    public void onServiceFault(String msg);

}
