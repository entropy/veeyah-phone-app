package com.veeyah.measure.manager;

import java.io.StringWriter;
import java.sql.Timestamp;
import org.xmlpull.v1.XmlSerializer;
import android.util.Xml;
import com.veeyah.matchbox.MySummary;
import com.veeyah.matchbox.MySummaryResponse;
import com.veeyah.measure.VeeyahConstants;
import com.veeyah.measure.VeeyahLogger;

public class MySummaryManager implements WebServiceReceiver {

    final String TAG = "[Veeyah MySummaryManager "+VeeyahConstants.VERSION_NUMBER+"]";
    final static String MY_SUMMARY_REQUEST = "MySummary";
    //public static String MY_SUMMARY_NEWS = "Your phone is powered via solar!";
    private VeeyahGUI callback;

	public void getUserSummary(String deviceID, WebServiceManager wsm, VeeyahGUI callback) {
        this.callback = callback;
		MySummary mySummary = new MySummary();
		mySummary.setDeviceID(deviceID);
		getMySummary(wsm, deviceID);
	}

	protected void getMySummary(WebServiceManager wsm, String deviceID) {
		try {
			XmlSerializer serializer = Xml.newSerializer();
			StringWriter writer = new StringWriter();
			try {

				serializer.setOutput(writer);
				serializer.startDocument("UTF-8", true);
				serializer.startTag("", "soapenv:Envelope");
				serializer.attribute("", "xmlns:soapenv",
						"http://schemas.xmlsoap.org/soap/envelope/");
				serializer.attribute("", "xmlns:mat",
						"http://veeyah.com/MatchBox");

				serializer.startTag("", "soapenv:Header");
				serializer.endTag("", "soapenv:Header");

				serializer.startTag("", "soapenv:Body");
				serializer.startTag("", "mat:MySummary");
				serializer.startTag("", "RequestTime");
				serializer.text(getTime());
				serializer.endTag("", "RequestTime");
				serializer.startTag("", "DeviceID");
				serializer.text(deviceID);
				serializer.endTag("", "DeviceID");
				serializer.endTag("", "mat:MySummary");
				serializer.endTag("", "soapenv:Body");
				serializer.endTag("", "soapenv:Envelope");
				serializer.endDocument();
                String msg = writer.toString();
				VeeyahLogger.i("Request", msg);
                WebServiceRequest wsr = new WebServiceRequest(MY_SUMMARY_REQUEST, VeeyahConstants.WEBSERVICE_DOMAIN+"/MatchBox", msg, this);
                wsm.makeWebServiceRequest(wsr, true, true);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		} catch (Exception e) {
			VeeyahLogger.i("Veeyah", "Exception getting MySummary: " + e.getClass()+ ": " + e.getMessage());
		}
	}

	private String getTime() {
		java.util.Date date = new java.util.Date();
		return ((new Timestamp(date.getTime())).toString());
	}

    public void onServiceComplete(String msg) {
        try {
            MySummaryResponse msr = new MySummaryResponse();
            try {
                String respStr = WebServiceManager.parseForSingleElt("SunJouleBalance", msg);
                if (!"".equals(respStr)) msr.setSunJouleBalance(Integer.parseInt(respStr));
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Failed to parse sunJouleBalance: " + e.getClass() + ": " + e.getMessage());
                msr.setSunJouleBalance(-1);
            }

            msr.getNews().addAll(WebServiceManager.parseForElt("News", msg));
            //VeeyahLogger.i("Veeyah MySummaryResponse Parser", "The news is "+msr.getNews().get(0));

            callback.refreshView(msr);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception onServiceComplete: "+e.getClass()+": "+e.getMessage());
        }
    }

    public void onServiceFault(String msg) {
        callback.showError("Could not reach the Veeyah server.");
    }

}
