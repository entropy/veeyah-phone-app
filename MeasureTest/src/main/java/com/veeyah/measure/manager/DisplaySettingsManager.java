package com.veeyah.measure.manager;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlSerializer;
import android.util.Xml;
import com.veeyah.measure.MatchBoxModel;
import com.veeyah.measure.VeeyahConstants;
import com.veeyah.measure.VeeyahLogger;
import com.veeyah.auth.DisplaySettingsResponse;
import com.veeyah.auth.VeeyahSetting;

public class DisplaySettingsManager implements WebServiceReceiver {

    final static String DISPLAY_SETTINGS_REQUEST = "DisplaySettings";
    private VeeyahGUI callback;

    public void displaySettings(String deviceID, WebServiceManager wsm, VeeyahGUI callback) {
        this.callback = callback;
        doDisplaySettings(deviceID, wsm);
    }

    private void doDisplaySettings(String deviceID, WebServiceManager wsm) {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            serializer.setOutput(writer);

            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "soapenv:Envelope");
            serializer.attribute("", "xmlns:soapenv",
                    "http://schemas.xmlsoap.org/soap/envelope/");
            serializer.attribute("", "xmlns:veey",
                    "http://veeyah.com/VeeyahAuth");
            serializer.startTag("", "soapenv:Header");
            serializer.endTag("", "soapenv:Header");

            serializer.startTag("", "soapenv:Body");
            serializer.startTag("", "veey:DisplaySettings");
            serializer.startTag("", "RequestTime");
            serializer.text("" + WebServiceManager.getCurrentTimeStamp());
            serializer.endTag("", "RequestTime");
            serializer.startTag("", "SessionID");
            serializer.endTag("", "SessionID");
            serializer.startTag("", "DeviceID");
            serializer.text("" + deviceID);
            serializer.endTag("", "DeviceID");

            serializer.endTag("", "veey:DisplaySettings");
            serializer.endTag("", "soapenv:Body");
            serializer.endTag("", "soapenv:Envelope");
            serializer.endDocument();
            String msg = writer.toString();
            VeeyahLogger.i("Request", msg);
            WebServiceRequest wsr = new WebServiceRequest(DISPLAY_SETTINGS_REQUEST, VeeyahConstants.WEBSERVICE_DOMAIN+"/VeeyahAuth", msg, this);
            wsm.makeWebServiceRequest(wsr, true, true);
        } catch (Exception e) {
            VeeyahLogger.w("Veeyah DisplaySettingsManager", "Exception updating settings: "+e.getClass()+": "+e.getMessage());
        }
    }

    public void onServiceComplete(String msg) {
        DisplaySettingsResponse dsr = new DisplaySettingsResponse();
        try {
            MatchBoxModel mbm = new MatchBoxModel();
            VeeyahLogger.i("Veeyah DisplaySettingsManager", "Response: "+msg);
            List<String> rawList = WebServiceManager.parseForElt("Setting", msg);
            for (String inner : rawList) {
                VeeyahSetting setting = new VeeyahSetting();
                setting.setSettingName(WebServiceManager.parseForSingleElt("SettingName", inner));
                setting.setSettingValue(WebServiceManager.parseForSingleElt("SettingValue", inner));
                dsr.getSetting().add(setting);
                if (VeeyahConstants.CUST_NAME.equals(setting.getSettingName())) {
                    mbm.setUserName(setting.getSettingValue());
                    //VeeyahLogger.i("Veeyah DisplaySettingsManager", "Set user name to: "+setting.getSettingValue());
                } else if (VeeyahConstants.CUST_EMAIL.equals(setting.getSettingName())) {
                    mbm.setUserEmail(setting.getSettingValue());
                    //VeeyahLogger.i("Veeyah DisplaySettingsManager", "Set user email to: "+setting.getSettingValue());
                } else {
                    VeeyahLogger.i("Veeyah DisplaySettingsManager", "Unknown setting: "+setting.getSettingName());
                }
            }
        } catch (Exception e) {
            VeeyahLogger.w("Veeyah DisplaySettingsResponse Parser", "Failed to parse response: "+e.getClass()+": "+e.getMessage());
            callback.showError("Could not parse response from the Veeyah server.");
            return;
        }

        callback.refreshView(dsr);
    }

    public void onServiceFault(String msg) {
        callback.showError("Could not register with the Veeyah server.");
    }

}
