package com.veeyah.measure.manager;

public interface VeeyahGUI {
    public void refreshView(Object data);
    public void showError(String msg);
}
