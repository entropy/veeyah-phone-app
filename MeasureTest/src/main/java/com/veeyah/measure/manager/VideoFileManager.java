package com.veeyah.measure.manager;

import android.app.DownloadManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.util.Log;

import com.veeyah.measure.VeeyahConstants;
import com.veeyah.measure.VeeyahLogger;
import com.veeyah.measure.ViaSolar;
import com.veeyah.measure.ViaSolarException;
import com.veeyah.utility.VeeyahMetric;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

public class VideoFileManager extends ContentProvider implements Parcelable {

    public static final Uri CONTENT_URI_BASE =
            Uri.parse("content://com.veeyah.measure.manager.VideoFileManager.files/");

    private static final String VIDEO_MIME_TYPE = "video/mp4";

    private SharedPreferences sh = null;
    private final String TAG = "[ Veeyah VideoFileManager "+VeeyahConstants.VERSION_NUMBER+"]";
    private final int MAX_VID_STORAGE = 10;
    private final String VIDEO_URL = "vURL";
    private final String VIDEO_PATH = "vPath";
    private final String VIDEO_LOADED = "vLoaded";
    private final String TIMES_PLAYED = "vPlayed";
    private final String VIDEO_SLOT = "vSlot";

    public final String NO_VIDEO = "vNone";
    public final int MIN_VID_STORAGE = 3;

    private String[] vidURLs;
    private String[] vidPaths;
    private boolean[] vidLoaded;
    private int[] timesPlayed;
    private boolean isWaitingExternal = false;
    private boolean isWaiting = false;
    private int currentSlot = 0;
    private DownloadManager currentDownload;
    private long enqueue = -1;

    public VideoFileManager() {
        sh = PreferenceManager.getDefaultSharedPreferences(ViaSolar.getContext());
        vidURLs = new String[MAX_VID_STORAGE];
        vidPaths = new String[MAX_VID_STORAGE];
        vidLoaded = new boolean[MAX_VID_STORAGE];
        timesPlayed = new int[MAX_VID_STORAGE];
        currentSlot = sh.getInt(VIDEO_SLOT, 0);
        for (int i=0; i<MAX_VID_STORAGE; i++) {
            vidURLs[i] = sh.getString(VIDEO_URL+i, "");
            vidPaths[i] = sh.getString(VIDEO_PATH+i, "");
            //fix change in paths
            //TODO: remove this after it has run at least once on all devices
            if (vidPaths[i].contains("/")) vidPaths[i] = "vid"+i;
            vidLoaded[i] = sh.getBoolean(VIDEO_LOADED+i, false);
            timesPlayed[i] =sh.getInt(TIMES_PLAYED+i, 0);
        }
    }

    public int countUnwatchedVideos() {
        //returns the number of loaded videos that have not been watched
        int unwatched = 0;
        for (int i=(MAX_VID_STORAGE-1); i>-1; i--) {
            if (vidLoaded[i] && timesPlayed[i] < 1) unwatched++;
        }
        //VeeyahLogger.i(TAG, "Unwatched videos: "+unwatched);
        return(unwatched);
    }

    public int countLoadedVideos() {
        int loaded = 0;
        for (int i=(MAX_VID_STORAGE-1); i>-1; i--) {
            if (vidLoaded[i]) loaded++;
        }
        VeeyahLogger.i(TAG, "Loaded videos: "+loaded);
        return(loaded);
    }

    public void startWaiting() { isWaitingExternal = true; }
    public void stopWaiting() { isWaitingExternal = false; }
    public boolean isWaiting() {
        return(isWaitingExternal);
    }

    /*public long getEnqueue() {
        return(enqueue);
    }

    public String getCurrentFileName() throws ViaSolarException {
        if (!isWaiting) throw new ViaSolarException("The video file manager is not expecting a new file right now!");
        return("vid"+currentSlot);
    }*/

    public void downloadSuccessful() {
        VeeyahLogger.i(TAG, "Video "+currentSlot+" loaded successfully");
        vidLoaded[currentSlot] = true;
        int oldSlot = currentSlot;
        currentSlot++;
        if (currentSlot >= MAX_VID_STORAGE) currentSlot = 0;
        if (sh != null) {
            SharedPreferences.Editor e = sh.edit();
            e.putInt(VIDEO_SLOT, currentSlot);
            e.putBoolean(VIDEO_LOADED+oldSlot, true);
            e.commit();
        }
        isWaiting = false;
        isWaitingExternal = false;
        VeeyahLogger.i(TAG, "Finished loading video, new slot: "+currentSlot);
    }

    public void downloadError() {
        isWaiting = false;
    }

    public void downloadNewVideo(URL url) throws ViaSolarException {
        if (url == null) throw new ViaSolarException("The video file manager received a null video URL.");
        if (!isWaitingExternal) throw new ViaSolarException("The video file manager was started without telling it to wait.");
        if (isWaiting) throw new ViaSolarException("The video file manager is already trying to download another video: "+vidURLs[currentSlot]);
        if (!WebServiceManager.isConnectingToInternet()) return;

        try {
            String urlStr = url.toString();
            for (int i=0; i<MAX_VID_STORAGE; i++) {
                if (urlStr.equals(vidURLs[i])) return;  //already have this video
            }
            setVideoInfo(currentSlot, url.toString(), "vid" + currentSlot, false, 0);
            isWaiting = true;
            currentDownload = (DownloadManager) new DownloadManager().execute(vidURLs[currentSlot]);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception starting download for new video '"+url.toString()+"': "+e.getClass()+": "+e.getMessage());
        }
    }

    private class DownloadManager extends AsyncTask<String, Integer, String> {
        protected void onProgressUpdate(Integer... progress) {
            //VeeyahLogger.i(TAG, "Download "+currentSlot+" progress: "+progress[0]);
        }

        protected void onPostExecute(String result) {
            try {
                if (vidURLs[currentSlot].equals(result)) {
                    downloadSuccessful();
                } else {
                    downloadError();
                }
            } catch (Exception e) {
                VeeyahLogger.w(TAG, "Exception onPostExecute of download video: "+e.getClass()+": "+e.getMessage());
            }
        }

        protected String doInBackground(String... i) {
            try {
                URL url = new URL(vidURLs[currentSlot]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    VeeyahLogger.w(TAG, "Server returned HTTP " + connection.getResponseCode()+ " " + connection.getResponseMessage());
                    return("");
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                // download the file
                InputStream input = connection.getInputStream();
                FileOutputStream output = ViaSolar.getContext().openFileOutput(vidPaths[currentSlot], Context.MODE_PRIVATE);
                //FileOutputStream output = new FileOutputStream(vidPaths[currentSlot]);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
                VeeyahLogger.i(TAG, "Wrote video: "+vidURLs[currentSlot]);
                return(vidURLs[currentSlot]);
            } catch (Exception e) {
                String errStr = "Exception contacting server for '"+vidURLs[currentSlot]+"': " + e.getClass() + ": " + e.getMessage();
                VeeyahLogger.w(TAG, errStr);
                return("");
            }

        }
    }

    private void setVideoInfo(int slot, String url, String path, boolean loaded, int played) {
        vidURLs[slot] = url;
        vidPaths[slot] = path;
        vidLoaded[slot] = loaded;
        timesPlayed[slot] = played;
        if (sh != null) {
            SharedPreferences.Editor e = sh.edit();
            e.putString(VIDEO_URL+slot, url);
            e.putString(VIDEO_PATH+slot, path);
            e.putBoolean(VIDEO_LOADED + slot, loaded);
            e.putInt(TIMES_PLAYED+slot, played);
            e.commit();
        }
    }

    private void incrementTimesPlayed(int slot) {
        timesPlayed[slot]++;
        if (sh != null) {
            SharedPreferences.Editor e = sh.edit();
            e.putInt(TIMES_PLAYED+slot, timesPlayed[slot]);
            e.commit();
        }
    }

    public String watchUnwatchedVideo() {
        for (int i=(MAX_VID_STORAGE-1); i>-1; i--) {
            if (vidLoaded[i] && timesPlayed[i] < 1) {
                incrementTimesPlayed(i);
                return(CONTENT_URI_BASE + Uri.encode(vidPaths[i]));
            }
        }
        return(NO_VIDEO);
    }

    public String watchAnyVideo() {
        int counter = -1;
        for (int i = 0; i < MAX_VID_STORAGE; i++) {
            if (vidLoaded[i]) {
                counter = i;
                break;
            }
        }
        if (counter == -1) return (NO_VIDEO);
        int vid = (int) (Math.random() * counter);
        incrementTimesPlayed(vid);
        return(CONTENT_URI_BASE + Uri.encode(vidPaths[vid]));
    }

    public String watchLocalVideo(String url) {
        if (url == null) return(NO_VIDEO);
        for (int i=(MAX_VID_STORAGE-1); i>-1; i--) {
            if (vidLoaded[i] && url.equals(vidURLs[i])) {
                incrementTimesPlayed(i);
                return(CONTENT_URI_BASE + Uri.encode(vidPaths[i]));
            }
        }
        return(NO_VIDEO);
    }

    public VideoFileManager(Parcel in) {
        this();
        in.readBundle();
    }

    public void writeToParcel(Parcel out, int flags) {
        Bundle b = new Bundle();
        b.putStringArray(VIDEO_URL, vidURLs);
        b.putStringArray(VIDEO_PATH, vidPaths);
        b.putBooleanArray(VIDEO_LOADED, vidLoaded);
        b.putIntArray(TIMES_PLAYED, timesPlayed);
        out.writeBundle(b);
    }

    public static final Parcelable.Creator<VideoFileManager> CREATOR = new Parcelable.Creator<VideoFileManager>() {
        public VideoFileManager createFromParcel(Parcel in) {
            return new VideoFileManager(in);
        }

        public VideoFileManager[] newArray(int size) {
            return new VideoFileManager[size];
        }
    };

    public int describeContents() {
        return (""+vidURLs.hashCode()+vidPaths.hashCode()+vidLoaded.hashCode()).hashCode();
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    public ArrayList<VeeyahMetric> getMetrics() {
        ArrayList<VeeyahMetric> output = new ArrayList<VeeyahMetric>();
        try {
            output.add(new VeeyahMetric(DeviceMetricsManager.VIDEO_CURRENT_SLOT, sh.getInt(VIDEO_SLOT, 0)));
            for (int i = 0; i < MAX_VID_STORAGE; i++) {
                output.add(new VeeyahMetric(DeviceMetricsManager.VIDEO_PREFIX+"."+i+"."+sh.getString(VIDEO_URL, "none"), sh.getInt(TIMES_PLAYED + i, 0)));
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception printing metrics: "+e.getClass()+": "+e.getMessage());
        }
        return(output);
    }

    @Override
    public String toString() {
        String out = "";
        out += "Current Slot: " + sh.getInt(VIDEO_SLOT, 0) + "\n";
        for (int i = 0; i < MAX_VID_STORAGE; i++) {
            out += "URL[" + i + "]: " + sh.getString(VIDEO_URL + i, "") + "\n";
            out += "Path[" + i + "]: " + sh.getString(VIDEO_PATH + i, "") + "\n";
            out += "Loaded[" + i + "]: " + sh.getBoolean(VIDEO_LOADED + i, false) + "\n";
            out += "timesPlayed[" + i + "]: " + sh.getInt(TIMES_PLAYED + i, 0) + "\n";
        }
        return (out);
    }

    @Override
    public String getType(final Uri uri) {
        return VIDEO_MIME_TYPE;
    }

    @Override
    public ParcelFileDescriptor openFile(final Uri uri, final String mode)
            throws FileNotFoundException {

        try {
            File f = ViaSolar.getContext().getFileStreamPath(uri.getPath().substring(1));
            return (ParcelFileDescriptor.open(f,
                    ParcelFileDescriptor.MODE_READ_ONLY));
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception opening file '"+uri.getPath().substring(1)+"': "+e.getClass()+": "+e.getMessage());
        }

        throw new FileNotFoundException(uri.getPath());
    }

    @Override
    public int delete(final Uri uri, final String selection, final String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Uri insert(final Uri uri, final ContentValues values) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Cursor query(final Uri uri, final String[] projection, final String selection, final String[] selectionArgs, final String sortOrder) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int update(final Uri uri, final ContentValues values, final String selection, final String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }
}
