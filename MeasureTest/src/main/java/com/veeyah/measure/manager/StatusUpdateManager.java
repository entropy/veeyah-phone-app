package com.veeyah.measure.manager;

import java.io.StringWriter;
import java.sql.Timestamp;

import org.xmlpull.v1.XmlSerializer;

import android.util.Log;
import android.util.Xml;

import com.veeyah.measure.MatchBoxModel;
import com.veeyah.measure.VeeyahConstants;
import com.veeyah.measure.VeeyahLogger;

public class StatusUpdateManager implements WebServiceReceiver {

    final static String STATUS_UPDATE_REQUEST = "StatusUpdate";
    final static String TAG = "[Veeyah StatusUpdateManager "+VeeyahConstants.VERSION_NUMBER+"]";

    public static void sendStatusUpdate(WebServiceManager wsm, MatchBoxModel mbm, long minSinceUpdate, int delta, int sjBurned) {
        try {
            //TODO: update matchboxmodel to handle status update data fields
            String deviceID = mbm.getDeviceID();
            XmlSerializer serializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            try {

                serializer.setOutput(writer);
                serializer.startDocument("UTF-8", true);
                serializer.startTag("", "soapenv:Envelope");
                serializer.attribute("", "xmlns:soapenv",
                        "http://schemas.xmlsoap.org/soap/envelope/");
                serializer.attribute("", "xmlns:vr",
                        "http://veeyah.com/VeeyahReport");

                serializer.startTag("", "soapenv:Header");
                serializer.endTag("", "soapenv:Header");

                serializer.startTag("", "soapenv:Body");
                serializer.startTag("", "vr:StatusUpdate");
                serializer.startTag("", "RequestTime");
                serializer.text(getTime());
                serializer.endTag("", "RequestTime");
                serializer.startTag("", "DeviceID");
                serializer.text(deviceID);
                serializer.endTag("", "DeviceID");
                serializer.startTag("", "MinutesSinceLastUpdate");
                serializer.text(minSinceUpdate+"");
                serializer.endTag("", "MinutesSinceLastUpdate");
                serializer.startTag("", "SunJoulesBurned");
                serializer.text(sjBurned+"");
                serializer.endTag("", "SunJoulesBurned");
                serializer.startTag("", "DownwardBatteryChange");
                serializer.text(delta+"");
                serializer.endTag("", "DownwardBatteryChange");
                serializer.startTag("", "BatteryCapacityWatthours");
                serializer.text(mbm.getCapacity()+"");
                serializer.endTag("", "BatteryCapacityWatthours");
                serializer.endTag("", "vr:StatusUpdate");
                serializer.endTag("", "soapenv:Body");
                serializer.endTag("", "soapenv:Envelope");
                serializer.endDocument();
                String msg = writer.toString();
                Log.i("Request", msg);
                WebServiceRequest wsr = new WebServiceRequest(STATUS_UPDATE_REQUEST, VeeyahConstants.WEBSERVICE_DOMAIN+"/VeeyahReport", msg, new StatusUpdateManager());
                wsm.makeWebServiceRequest(wsr, true, true);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception sending StatusUpdate: " + e.getClass()+ ": " + e.getMessage());
        }
    }

    private static String getTime() {
        java.util.Date date = new java.util.Date();
        return ((new Timestamp(date.getTime())).toString());
    }

    public void onServiceComplete(String msg) {
        try {
            MatchBoxModel mbm = new MatchBoxModel();
            VeeyahLogger.i(TAG, "Got status update reponse: "+WebServiceManager.parseForSingleElt("TotalMWhRetired", msg));
            mbm.setTotalMWh(Integer.parseInt(WebServiceManager.parseForSingleElt("TotalMWhRetired", msg)));
            mbm.setTargetMWh(Integer.parseInt(WebServiceManager.parseForSingleElt("NextTargetMWh", msg)));
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Failed to parse total MWh impact: "+e.getClass()+": "+e.getMessage());
            return;
        }
    }

    public void onServiceFault(String msg) {
        VeeyahLogger.e(TAG, "Fault updating status.  The message is: " + msg);
    }

}
