package com.veeyah.measure.manager;

import java.io.StringWriter;
import org.xmlpull.v1.XmlSerializer;
import android.util.Xml;
import com.veeyah.matchbox.GetSunJoulesResponse;
import com.veeyah.measure.MatchBoxModel;
import com.veeyah.measure.SunPort;
import com.veeyah.measure.VeeyahConstants;
import com.veeyah.measure.VeeyahLogger;

public class GetSunJoulesManager implements WebServiceReceiver {

    final String TAG = "[Veeyah GetSunJoulesManager "+VeeyahConstants.VERSION_NUMBER+"]";
    final static String GET_SUNJOULES_REQUEST = "GetSunJoules";
    private VeeyahGUI callback;
    private boolean cache = false; //should we cache these sunjoules or make them available?  false => available
    private SunPort sp = null;

    public GetSunJoulesManager() {

    }

    public GetSunJoulesManager(SunPort sp) {
        this.sp = sp;
    }

    public void getSunJoules(String deviceID, int amount, WebServiceManager wsm, VeeyahGUI callback, boolean cache) {
        this.callback = callback;
        this.cache = cache;
        if (VeeyahConstants.DEFAULT_DEVICE_ID.equals(deviceID)) {
            callback.showError("You cannot get SunJoules for an unregistered device.");
            return;
        }
        requestSunJoules(deviceID, amount, wsm);
    }

    private void requestSunJoules(String deviceID, int amount, WebServiceManager wsm) {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            serializer.setOutput(writer);
            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "soapenv:Envelope");
            serializer.attribute("", "xmlns:soapenv",
                    "http://schemas.xmlsoap.org/soap/envelope/");
            serializer.attribute("", "xmlns:mat",
                    "http://veeyah.com/MatchBox");

            serializer.startTag("", "soapenv:Header");
            serializer.endTag("", "soapenv:Header");

            serializer.startTag("", "soapenv:Body");
            serializer.startTag("", "mat:GetSunJoules");
            serializer.startTag("", "RequestTime");
            serializer.text(WebServiceManager.getCurrentTimeStamp());
            serializer.endTag("", "RequestTime");
            serializer.startTag("", "DeviceID");
            serializer.text(deviceID);
            serializer.endTag("", "DeviceID");
            serializer.startTag("", "Amount");
            serializer.text(amount + "");
            serializer.endTag("", "Amount");
            serializer.endTag("", "mat:GetSunJoules");
            serializer.endTag("", "soapenv:Body");
            serializer.endTag("", "soapenv:Envelope");
            serializer.endDocument();
            String msg = writer.toString();
            VeeyahLogger.i(TAG, "Request: "+msg);
            WebServiceRequest wsr = new WebServiceRequest(GET_SUNJOULES_REQUEST, VeeyahConstants.WEBSERVICE_DOMAIN+"/MatchBox", msg, this);
            wsm.makeWebServiceRequest(wsr, false, false);
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception getting sunjoules: "+e.getClass()+": "+e.getMessage());
        }
    }

    public void onServiceComplete(String msg) {
        GetSunJoulesResponse response = new GetSunJoulesResponse();
        try {
            response.setAmount(Integer.parseInt(WebServiceManager.parseForSingleElt("Amount", msg)));
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Failed to parse sunJouleBalance: "+e.getClass()+": "+e.getMessage());
            callback.showError("Could not parse response from the Veeyah server.");
            return;
        }

        try {
            MatchBoxModel mbm = new MatchBoxModel();
            if (sp == null) {
                if (cache) {
                    mbm.setCachedSJ(mbm.getCachedSJ() + (response.getAmount() * 100));
                } else {
                    if (mbm.getAvailableSJ() < 1) mbm.setStartTime(mbm.printTime());
                    mbm.setAvailableSJ(mbm.getAvailableSJ() + (response.getAmount() * 100));
                }
            } else {
                sp.setPendingFill(response.getAmount());
                callback.refreshView(sp);
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception saving new SunJoules: "+e.getClass()+": "+e.getMessage());
        }

        callback.refreshView(response);
    }

    public void onServiceFault(String msg) {
        callback.showError("Could not get SunJoules from the Veeyah server.");
    }

}
