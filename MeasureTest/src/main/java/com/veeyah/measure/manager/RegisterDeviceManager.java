package com.veeyah.measure.manager;

import java.io.StringWriter;
import org.xmlpull.v1.XmlSerializer;
import android.util.Xml;

import com.veeyah.measure.MatchBoxJSONSerializer;
import com.veeyah.measure.MatchBoxModel;
import com.veeyah.measure.VeeyahConstants;
import com.veeyah.measure.VeeyahLogger;
import com.veeyah.measure.ViaSolar;
import com.veeyah.auth.RegisterDeviceResponse;
import com.veeyah.auth.RegisterType;
import com.veeyah.measure.ViaSolarException;

public class RegisterDeviceManager implements WebServiceReceiver {

    final static String REGISTER_DEVICE_REQUEST = "RegisterDevice";
    private VeeyahGUI callback;
    private String userName, userEmail;

    public void registerDevice(RegisterType deviceInfo, WebServiceManager wsm, VeeyahGUI callback) {
        this.callback = callback;
        updateDevice(wsm, deviceInfo);
    }

    private void updateDevice(WebServiceManager wsm, RegisterType deviceInfo) {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        userName = deviceInfo.getName();
        userEmail = deviceInfo.getEmail();
        try {
            if (serializer == null) throw new ViaSolarException("Serializer is null");
            if (writer == null) throw new ViaSolarException("Writer is null");
            serializer.setOutput(writer);

            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "soapenv:Envelope");
            serializer.attribute("", "xmlns:soapenv",
                    "http://schemas.xmlsoap.org/soap/envelope/");
            serializer.attribute("", "xmlns:veey",
                    "http://veeyah.com/VeeyahAuth");
            serializer.startTag("", "soapenv:Header");
            serializer.endTag("", "soapenv:Header");
            serializer.startTag("", "soapenv:Body");
            serializer.startTag("", "veey:RegisterDevice");
            serializer.startTag("", "RequestTime");
            serializer.text(WebServiceManager.getCurrentTimeStamp());
            serializer.endTag("", "RequestTime");
            serializer.startTag("", "DeviceInfo");
            serializer.startTag("", "Email");
            if (deviceInfo == null) throw new ViaSolarException("deviceInfo is null");
            if (deviceInfo.getEmail() == null) throw new ViaSolarException("Email is null");
            serializer.text(deviceInfo.getEmail());
            serializer.endTag("", "Email");
            serializer.startTag("", "Name");
            if (deviceInfo.getName() == null) throw new ViaSolarException("Name is null");
            serializer.text(deviceInfo.getName());
            serializer.endTag("", "Name");
            serializer.startTag("", "Password");
            if (deviceInfo.getPassword() == null) throw new ViaSolarException("Password is null");
            serializer.text(deviceInfo.getPassword());
            serializer.endTag("", "Password");
            serializer.startTag("", "ClientGUID");
            if (deviceInfo.getClientGUID() == null) throw new ViaSolarException("GUID is null");
            serializer.text(deviceInfo.getClientGUID());
            serializer.endTag("", "ClientGUID");
            serializer.startTag("", "InviteCode");
            if (deviceInfo.getInviteCode() == null) throw new ViaSolarException("Invite is null");
            serializer.text(deviceInfo.getInviteCode());
            serializer.endTag("", "InviteCode");
            serializer.startTag("", "DeviceName");
            if (deviceInfo.getDeviceName() == null) throw new ViaSolarException("Device Name is null");
            serializer.text(deviceInfo.getDeviceName());
            serializer.endTag("", "DeviceName");
            serializer.startTag("", "DeviceType");
            if (deviceInfo.getDeviceType() == null) throw new ViaSolarException("Device Type is null");
            serializer.text(deviceInfo.getDeviceType());
            serializer.endTag("", "DeviceType");
            serializer.startTag("", "BatteryCapacity");
            if (deviceInfo.getBatteryCapacity() == null) throw new ViaSolarException("Battery Capacity is null");
            serializer.text("" + deviceInfo.getBatteryCapacity());
            serializer.endTag("", "BatteryCapacity");
            serializer.endTag("", "DeviceInfo");
            serializer.endTag("", "veey:RegisterDevice");
            serializer.endTag("", "soapenv:Body");
            serializer.endTag("", "soapenv:Envelope");
            serializer.endDocument();
            String msg = writer.toString();
            if (msg == null) throw new ViaSolarException("msg is null");
            //VeeyahLogger.i("Request", msg);
            WebServiceRequest wsr = new WebServiceRequest(REGISTER_DEVICE_REQUEST, VeeyahConstants.WEBSERVICE_DOMAIN+"/VeeyahAuth", msg, this);
            if (wsm == null) throw new ViaSolarException("wsm is null");
            wsm.makeWebServiceRequest(wsr, true, true);
        } catch (Exception e) {
            VeeyahLogger.w("Veeyah RegisterDeviceManager", "Exception registering device: "+e.getClass()+": "+e.getMessage());
            callback.showError("Sorry, your device could not be registered: "+e.getMessage());
        }
    }

    public void onServiceComplete(String msg) {
        RegisterDeviceResponse rdr = new RegisterDeviceResponse();
        try {
            rdr.setDeviceID(WebServiceManager.parseForSingleElt("DeviceID", msg));
            //TODO: enable this in the future, remember that BigInt does not allow blanks
            //rdr.batteryCapacity = new BigInteger(""+WebServiceManager.parseForSingleElt("BatteryCapacity", msg));
        } catch (Exception e) {
            VeeyahLogger.w("Veeyah RegisterDeviceResponse Parser", "Failed to parse sunJouleBalance: "+e.getClass()+": "+e.getMessage());
            callback.showError("Could not parse response from the Veeyah server.");
            return;
        }

        try {
            MatchBoxModel mbm = new MatchBoxModel();
            mbm.setDeviceID(rdr.getDeviceID());
            mbm.setUserName(userName);
            mbm.setUserEmail(userEmail);
            MatchBoxJSONSerializer.saveMbInfo(mbm, ViaSolar.getContext());
        } catch (Exception e) {
            VeeyahLogger.w("Veeyah RegisterDeviceManager", "Exception saving results from device registration: "+e.getClass()+": "+e.getMessage());
        }

        callback.refreshView(rdr);
    }

    public void onServiceFault(String msg) {
        callback.showError("Could not register with the Veeyah server.");
    }

}
