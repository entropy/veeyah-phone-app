package com.veeyah.measure.manager;

import java.io.StringWriter;
import java.util.List;
import org.xmlpull.v1.XmlSerializer;
import android.util.Xml;
import com.veeyah.measure.MatchBoxModel;
import com.veeyah.measure.VeeyahConstants;
import com.veeyah.measure.VeeyahLogger;
import com.veeyah.auth.VeeyahSetting;

public class ChangeSettingManager implements WebServiceReceiver {

    final static String CHANGE_SETTING_REQUEST = "ChangeSetting";
    private VeeyahGUI callback;
    private String userName, userEmail;

    public void changeSetting(String deviceID, String userName, String userEmail, WebServiceManager wsm, VeeyahGUI callback) {
        this.callback = callback;
        this.userName = userName;
        this.userEmail = userEmail;
        doChangeSetting(deviceID, wsm);
    }

    private void doChangeSetting(String deviceID, WebServiceManager wsm) {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            serializer.setOutput(writer);

            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "soapenv:Envelope");
            serializer.attribute("", "xmlns:soapenv",
                    "http://schemas.xmlsoap.org/soap/envelope/");
            serializer.attribute("", "xmlns:veey",
                    "http://veeyah.com/VeeyahAuth");
            serializer.startTag("", "soapenv:Header");
            serializer.endTag("", "soapenv:Header");

            serializer.startTag("", "soapenv:Body");
            serializer.startTag("", "veey:ChangeSetting");
            serializer.startTag("", "RequestTime");
            serializer.text("" + WebServiceManager.getCurrentTimeStamp());
            serializer.endTag("", "RequestTime");
            serializer.startTag("", "SessionID");
            serializer.text("");
            serializer.endTag("", "SessionID");
            serializer.startTag("", "DeviceID");
            serializer.text("" + deviceID);
            serializer.endTag("", "DeviceID");
            serializer.startTag("", "Setting");
            serializer.startTag("", "SettingName");
            serializer.text("" + "CUSTOMER NAME");
            serializer.endTag("", "SettingName");
            serializer.startTag("", "SettingValue");
            serializer.text(userName);
            serializer.endTag("", "SettingValue");
            serializer.endTag("", "Setting");
            serializer.startTag("", "Setting");
            serializer.startTag("", "SettingName");
            serializer.text("" + "CUSTOMER EMAIL");
            serializer.endTag("", "SettingName");
            serializer.startTag("", "SettingValue");
            serializer.text(userEmail);
            serializer.endTag("", "SettingValue");
            serializer.endTag("", "Setting");
            serializer.endTag("", "veey:ChangeSetting");
            serializer.endTag("", "soapenv:Body");
            serializer.endTag("", "soapenv:Envelope");
            serializer.endDocument();
            String msg = writer.toString();
            VeeyahLogger.i("Request", msg);
            WebServiceRequest wsr = new WebServiceRequest(CHANGE_SETTING_REQUEST, VeeyahConstants.WEBSERVICE_DOMAIN+"/VeeyahAuth", msg, this);
            wsm.makeWebServiceRequest(wsr, true, true);
        } catch (Exception e) {
            VeeyahLogger.w("Veeyah RegisterDeviceManager", "Exception updating settings: "+e.getClass()+": "+e.getMessage());
        }
    }

    public void onServiceComplete(String msg) {
        MatchBoxModel mbm = new MatchBoxModel();

        try {
            List<String> rawList = WebServiceManager.parseForElt("VeeyahSetting", msg);
            for (String inner : rawList) {
                VeeyahSetting setting = new VeeyahSetting();
                setting.setSettingName(WebServiceManager.parseForSingleElt("SettingName", inner));
                setting.setSettingValue(WebServiceManager.parseForSingleElt("SettingValue", inner));
                if (VeeyahConstants.CUST_NAME.equals(setting.getSettingName())) {
                    mbm.setUserName(setting.getSettingValue());
                } else if (VeeyahConstants.CUST_EMAIL.equals(setting.getSettingName())) {
                    mbm.setUserEmail(setting.getSettingValue());
                } else {
                    VeeyahLogger.i("Veeyah ChangeSettingManager", "Unknown setting: "+setting.getSettingName());
                }
            }
        } catch (Exception e) {
            VeeyahLogger.w("Veeyah ChangeSettingResponse Parser", "Failed to parse response: "+e.getClass()+": "+e.getMessage());
            callback.showError("Could not parse response from the Veeyah server.");
            return;
        }

        callback.refreshView(mbm);
    }

    public void onServiceFault(String msg) {
        callback.showError("Could not register with the Veeyah server.");
    }

}
