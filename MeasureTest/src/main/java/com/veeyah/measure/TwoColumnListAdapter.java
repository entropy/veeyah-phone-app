package com.veeyah.measure;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

public class TwoColumnListAdapter extends BaseAdapter {

    Context context;
    List<TwoColumnRowItem> rowItem;
    View.OnClickListener listenLeft = null;
    View.OnClickListener listenRight = null;

    TwoColumnListAdapter(Context context, List<TwoColumnRowItem> rowItem) {
        this.context = context;
        this.rowItem = rowItem;
    }

    private class ViewHolder {
        TextView left;
        Button right;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.two_column_list, null);
            holder = new ViewHolder();
            holder.left = (TextView) convertView.findViewById(R.id.leftText);
            if (listenLeft != null) holder.left.setOnClickListener(listenLeft);
            holder.right = (Button) convertView.findViewById(R.id.rightButton);
            if (listenRight != null) holder.right.setOnClickListener(listenRight);

            TwoColumnRowItem row_pos = rowItem.get(position);
            // setting the image resource and title
            holder.left.setText(position+": "+row_pos.getLeftText());
            holder.right.setText(row_pos.getRightText());
            convertView.setTag(holder);
        }
        return convertView;
    }

    public void setLeftClickListener(View.OnClickListener left) {
        this.listenLeft = left;
    }

    public void setRightClickListener(View.OnClickListener right) {
        this.listenRight = right;
    }

    @Override
    public int getCount() {
        return rowItem.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItem.indexOf(getItem(position));
    }

}
