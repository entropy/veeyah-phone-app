package com.veeyah.measure;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.veeyah.measure.manager.ChangeSettingManager;
import com.veeyah.measure.manager.RegisterSunPortManager;
import com.veeyah.measure.manager.VeeyahGUI;
import com.veeyah.measure.manager.WebServiceManager;


@SuppressLint("NewApi")
public class RegisterPort extends Activity implements VeeyahGUI {
    private final String TAG = "[ Veeyah RegisterPort "+VeeyahConstants.VERSION_NUMBER+"]";

    public MatchBoxModel mbm;
    RegisterPort self;
    private ProgressDialog progressDialog = null;
    LinearLayout background = null;
    private boolean registered = false;
    private BroadcastReceiver listen;
    private String unitID, email;
    private SunPort newPort = null;
    public Handler handler = new Handler();

    public void refreshView(Object data) {
        if (data == null) {
            VeeyahLogger.w(TAG, "Null data Received!");
            return;
        }

        try {
            if (data.getClass().equals(String.class)) {
                String msg = (String) data;
                showToast(msg);
                if (newPort != null) {
                    Intent spIntent = new Intent();
                    spIntent.setClass(ViaSolar.getContext(), VeeyahBattery.class);
                    spIntent.setAction(VeeyahBattery.CONNECT_BLE);
                    spIntent.putExtra(VeeyahBattery.BLE_ADDRESS, newPort.getAddress());
                    startService(spIntent);
                    goHome(true);
                } else {
                    goHome(false);
                }
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception redirecting to Home_View: "+e.getClass()+": "+e.getMessage());
        }

    }

    public void showError(String msg) {
        showToast("There was a network problem registering your SunPort. Please try again later.");
        goHome(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        VeeyahLogger.i(TAG, "onResume()");
        registerBroadcastReceiver();
    }

    private void checkIntent() {
        try {

        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception checking intent: "+e.getClass()+": "+e.getMessage());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            self = this;
            super.onCreate(savedInstanceState);

            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.activity_register);

            mbm = new MatchBoxModel();

            background = (LinearLayout) findViewById(R.id.scv);
            getBackgroundScreen(background);

            TextView learnMore = (TextView) findViewById(R.id.registerTextHeader);
            learnMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showToast("Loading plug.solar website...");
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                    browserIntent.setData(Uri.parse("http://plug.solar/"));
                    startActivity(browserIntent);
                }
            });


            String userEmail = mbm.getUserEmail();
            if (!"".equals(userEmail) && !VeeyahConstants.DEFAULT_USER_EMAIL.equals(userEmail)) {
                this.email = userEmail;
                TextView mailTextView = (TextView) findViewById(R.id.mailTextView);
                mailTextView.setText(this.email);
            }

            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle(getString(R.string.app_name));
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);

            registerBroadcastReceiver();

            Button regBtn = (Button) findViewById(R.id.registerPortBtn);
            regBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!registered) {
                        setDialogText("Checking your info. . .");
                        showDialog();
                        registerProcess();
                    } else {
                        goHome(false);
                    }
                }
            });

            Button cancelBtn = (Button) findViewById(R.id.cancelBtn);
            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goHome(false);
                }
            });

        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception onCreate: "+e.getClass()+": "+e.getMessage());
        }
    }

    private void showDialog() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                progressDialog.show();
            }
        });
    }

    private void setDialogText(final String text) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                progressDialog.setMessage(text);
            }
        });
    }

    private void hideDialog() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (progressDialog.isShowing()) progressDialog.dismiss();
            }
        });
    }

    private void registerBroadcastReceiver() {
        listen = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {

                    VeeyahLogger.i(TAG, "Got broadcast: "+intent.getAction());
                    if (VeeyahBattery.REFRESH.equals(intent.getAction())) {
                            VeeyahLogger.i(TAG, "got refresh");
                    } else if (VeeyahBattery.SCAN_BLE.equals(intent.getAction())) {
                        try {
                            SunPort sp = intent.getParcelableExtra(VeeyahBattery.CURRENT_SUNPORT);
                            if (sp == null) {
                                hideDialog();
                                showToast("No SunPorts were detected nearby.  Please plug your SunPort in and try again.");
                            } else if (sp.getRawAddress().contains(unitID.toUpperCase())) {
                                showToast("Found your SunPort!  Registering it. . .");
                                setDialogText("Found your SunPort!  Registering it. . .");
                                newPort = sp;

                                RegisterSunPortManager regPort = new RegisterSunPortManager(newPort);
                                regPort.registerSunPort(sp.getRawAddress(), new WebServiceManager(), self);
                            } else {
                                VeeyahLogger.i(TAG, "Detected SunPort MAC is: "+sp.getRawAddress());
                                showToast("Detected a SunPort nearby, but the unitID does not match!\nPlease check your unit ID and try again!");
                                hideDialog();
                            }
                        } catch (Exception e) {
                            VeeyahLogger.w(TAG, "Exception registering SunPort after Scan BLE response: "+e.getClass()+": "+e.getMessage());
                        }
                    } else {
                        VeeyahLogger.i(TAG, "Received unknown broadcast: "+intent.getAction());
                    }
                } catch (Exception e) {
                    VeeyahLogger.w(TAG,
                            "Exception receiving local broadcast '"
                                    + intent.getAction()
                                    + "' in MainActivity: " + e.getClass()
                                    + ": " + e.getMessage()
                    );
                }
            }
        };

        VeeyahBattery.registerLocalBroadcast(listen, this);
        VeeyahLogger.i(TAG, "Broadcast Receiver is listening. . .");
    }

    private void registerProcess() {
        try {
            EditText mailText = (EditText) findViewById(R.id.mailTextView);
            Editable email = mailText.getText();
            if (email != null && email.length() > 0) {

                EditText unitIDText = (EditText) findViewById(R.id.unitIdText);
                Editable unitID = unitIDText.getText();

                if (unitID != null && unitID.length() == 4) {

                    if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                        // Check for invalid email characters (whitespace, etc)
                        //showToast("Please plug your SunPort in nearby. . .");
                        setDialogText("Please plug your SunPort in nearby. . .");
                        this.unitID = unitID.toString();
                        this.email = email.toString();
                        if (!this.email.equals(mbm.getUserEmail())) {
                            ChangeSettingManager changeSet = new ChangeSettingManager();
                            mbm.setUserEmail(this.email);
                            changeSet.changeSetting(mbm.getDeviceID(), mbm.getUserName(), mbm.getUserEmail(), new WebServiceManager(), self);
                        }
                        Intent intent = new Intent();
                        intent.setClass(ViaSolar.getContext(), VeeyahBattery.class);
                        intent.setAction(VeeyahBattery.SCAN_BLE);
                        intent.putExtra(VeeyahBattery.UNIT_ID, this.unitID);
                        startService(intent);
                    } else {
                        showToast("Please enter a valid email address to continue.");
                        hideDialog();
                    }
                } else {
                    showToast("Please enter a valid unit ID to continue.");
                    hideDialog();
                }
            } else {
                showToast("Please enter a valid email address to continue.");
                hideDialog();
            }
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception registering device onClick of Go: " + e.getClass() + ": " + e.getMessage());
            showToast("Sorry, there was a problem registering your device.  Please contact customersupport@veeyah.com");
            hideDialog();
        }
    }

    @Override
    public void onDestroy() {
        VeeyahLogger.i(TAG, "onDestroy called");
        hideDialog();
        super.onDestroy();
        VeeyahLogger.i(TAG, "onDestroy finished");
    }

    public void getBackgroundScreen(LinearLayout layout) {

        try {

            /*Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("H");
            String strDate = sdf.format(c.getTime());
            int hours = Integer.parseInt(strDate);
            final ImageView dayNight = (ImageView) findViewById(R.id.daynight);

            if (isBetween(hours, 6, 8)) {
                layout.setBackgroundResource(R.drawable.sky_bg_04);
                dayNight.setImageResource(R.drawable.sun);
            } else if (isBetween(hours, 8, 12)) {
                layout.setBackgroundResource(R.drawable.sky_bg_04);
                dayNight.setImageResource(R.drawable.sun);

            } else if (isBetween(hours, 12, 16)) {

                layout.setBackgroundResource(R.drawable.sky_bg);
                dayNight.setImageResource(R.drawable.sun);

            } else if (isBetween(hours, 16, 20)) {

                layout.setBackgroundResource(R.drawable.sky_bg);
                dayNight.setImageResource(R.drawable.sun);

            } else if (isBetween(hours, 20, 24)) {
                dayNight.setImageResource(R.drawable.moon);
                layout.setBackgroundResource(R.drawable.sky_bg_04);

            } else if (isBetween(hours, 0, 6)) {
                dayNight.setImageResource(R.drawable.moon);

                layout.setBackgroundResource(R.drawable.sky_bg_04);

            }*/
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception setting background: "+e.getClass()+": "+e.getMessage());
        }
    }

    public static boolean isBetween(int x, int lower, int upper) {
        return lower < x && x <= upper;
    }

    private void goHome(boolean showPlug) {
        final Intent startMainActivity = new Intent(self,
                Home_View.class);
        if (showPlug) {
            startMainActivity.putExtra(VeeyahBattery.CURRENT_SUNPORT, newPort);
            startMainActivity.setType(VeeyahBattery.CONNECT_BLE);
        }
        startActivity(startMainActivity);
        hideDialog();
        finish();
        return;
    }

    protected void showToast(final String msg) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(
                            self,
                            msg,
                            Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            VeeyahLogger.w(TAG, "Exception showing toast: "+e.getClass()+": "+e.getMessage());
        }
    }
}

