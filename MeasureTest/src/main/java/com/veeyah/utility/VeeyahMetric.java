package com.veeyah.utility;

public class VeeyahMetric {

    private String name;
    private int value;

    public VeeyahMetric(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return(name);
    }

    public int getValue() {
        return(value);
    }
}
