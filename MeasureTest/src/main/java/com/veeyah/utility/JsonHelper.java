package com.veeyah.utility;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class JsonHelper {
	public static final String TAG = "[ JSON ]";
	
	public static String getItemFromTag(String json, String tag) {
		JSONObject jsonObj;
		try {
			jsonObj = new JSONObject(json);
			return jsonObj.getString(tag);
		} catch (JSONException e) {
			Log.e(TAG, e.toString());
			return "";
		}
	}
	
	public static String getBitlyUrl(String json) { 
		String shortUrl = "";
		JSONObject jsonObj;
		try {
			jsonObj = new JSONObject(json);
			JSONObject obj = (JSONObject) jsonObj.get("data");
			shortUrl = (String) obj.get("url");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return shortUrl;
	}
}
