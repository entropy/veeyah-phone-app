package com.veeyah.utility;

import java.util.Calendar;
public class Astro {

	public static boolean isNight() {
		Calendar now = Calendar.getInstance();
		
		Calendar sunrise = Calendar.getInstance();
		Calendar sunset = Calendar.getInstance();

		sunrise.set(Calendar.HOUR_OF_DAY, 7);
		sunrise.set(Calendar.MINUTE, 0);
		sunrise.set(Calendar.SECOND, 0);
		sunrise.set(Calendar.MILLISECOND, 0);
		
		sunset.set(Calendar.HOUR_OF_DAY, 19);
		sunset.set(Calendar.MINUTE, 0);
		sunset.set(Calendar.SECOND, 0);
		sunset.set(Calendar.MILLISECOND, 0);
		
		if (now.after(sunrise) && now.before(sunset)) {
			// Day
			return false;
		} else {
			// Night
			return true;
		}
	}
}
