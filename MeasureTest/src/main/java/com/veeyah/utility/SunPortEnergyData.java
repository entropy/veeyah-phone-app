package com.veeyah.utility;

import java.util.Date;

/**
 * Created by netripy on 12/15/2014.
 */

public class SunPortEnergyData {

    private Date dateTime;
    private int energySJ;
    private int energyPercent;

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date value) {
        this.dateTime = value;
    }

    public int getEnergySJ() {
        return energySJ;
    }

    public void setEnergySJ(int value) {
        this.energySJ = value;
    }

    public int getEnergyPercent() {
        return energyPercent;
    }

    public void setEnergyPercent(int value) {
        this.energyPercent = value;
    }

}

