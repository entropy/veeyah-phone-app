package com.veeyah.utility;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;



public class CustomListAdapter extends ArrayAdapter<String> {
	Context context;
	private ArrayList<String> list;
	int layoutId;
	Holder holder;
	public View view;
	//public int currPosition = 0;

	public CustomListAdapter(Context context, int textViewResourceId,
			ArrayList<String> list) {
		super(context, android.R.layout.simple_list_item_1, list);
		this.context = context;
		this.list = list;
		layoutId = textViewResourceId;

	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public String getItem(int position) {
		return list.get(position);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		RelativeLayout layout;

		if (convertView == null) {

			layout = (RelativeLayout) View.inflate(context, layoutId, null);

			holder = new Holder();
			
//			holder.title = (TextView) layout.findViewById(R.id.txtNewsSource);
			layout.setTag(holder);

		} else {
			
			layout = (RelativeLayout) convertView;
			view = layout;
			holder = (Holder) layout.getTag();
		}
		Canvas c = new Canvas();
		RectF lineRect = new RectF(0, 0,90, 50);
		Paint p1 = new Paint();
		// smooths
		p1.setAntiAlias(true);
		p1.setColor(Color.DKGRAY);
		p1.setStyle(Paint.Style.STROKE);
		p1.setStrokeWidth(1);
		c.drawArc(lineRect, 0, 360, false, p1);
		//String newsSource = getItem(position);
//		holder.title.setText(newsSource);
		//VeeyahLogger.v("Test", "lo frm newsadpater");

		return layout;
	}

	private class Holder {
		public TextView title;

	}
	/*public int getCurrentPosition(){
		return currPosition;
	}*/
}
