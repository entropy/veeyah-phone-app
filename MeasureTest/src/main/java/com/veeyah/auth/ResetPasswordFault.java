
package com.veeyah.auth;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResetPasswordFault" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
public class ResetPasswordFault {

    protected String resetPasswordFault;

    /**
     * Gets the value of the resetPasswordFault property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResetPasswordFault() {
        return resetPasswordFault;
    }

    /**
     * Sets the value of the resetPasswordFault property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResetPasswordFault(String value) {
        this.resetPasswordFault = value;
    }

}
