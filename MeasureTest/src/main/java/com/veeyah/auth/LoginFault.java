
package com.veeyah.auth;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LoginFault" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
public class LoginFault {

    protected String loginFault;

    /**
     * Gets the value of the loginFault property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoginFault() {
        return loginFault;
    }

    /**
     * Sets the value of the loginFault property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoginFault(String value) {
        this.loginFault = value;
    }

}
