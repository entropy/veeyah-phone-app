
package com.veeyah.auth;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CheckSessionFault" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
public class CheckSessionFault {

    protected String checkSessionFault;

    /**
     * Gets the value of the checkSessionFault property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckSessionFault() {
        return checkSessionFault;
    }

    /**
     * Sets the value of the checkSessionFault property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckSessionFault(String value) {
        this.checkSessionFault = value;
    }

}
