
package com.veeyah.auth;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the VeeyahAuthService package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: VeeyahAuthService
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link SignUpResponse }
     * 
     */
    public SignUpResponse createSignUpResponse() {
        return new SignUpResponse();
    }

    /**
     * Create an instance of {@link DisplaySettingsFault }
     * 
     */
    public DisplaySettingsFault createDisplaySettingsFault() {
        return new DisplaySettingsFault();
    }

    /**
     * Create an instance of {@link LogoutResponse }
     * 
     */
    public LogoutResponse createLogoutResponse() {
        return new LogoutResponse();
    }

    /**
     * Create an instance of {@link SignUpFault }
     * 
     */
    public SignUpFault createSignUpFault() {
        return new SignUpFault();
    }

    /**
     * Create an instance of {@link RegisterDeviceFault }
     * 
     */
    public RegisterDeviceFault createRegisterDeviceFault() {
        return new RegisterDeviceFault();
    }

    /**
     * Create an instance of {@link CheckSession }
     * 
     */
    public CheckSession createCheckSession() {
        return new CheckSession();
    }

    /**
     * Create an instance of {@link WebFingerprint }
     * 
     */
    public WebFingerprint createWebFingerprint() {
        return new WebFingerprint();
    }

    /**
     * Create an instance of {@link Logout }
     * 
     */
    public Logout createLogout() {
        return new Logout();
    }

    /**
     * Create an instance of {@link SignUp }
     * 
     */
    public SignUp createSignUp() {
        return new SignUp();
    }

    /**
     * Create an instance of {@link CheckSessionFault }
     * 
     */
    public CheckSessionFault createCheckSessionFault() {
        return new CheckSessionFault();
    }

    /**
     * Create an instance of {@link CheckSessionResponse }
     * 
     */
    public CheckSessionResponse createCheckSessionResponse() {
        return new CheckSessionResponse();
    }

    /**
     * Create an instance of {@link ResetPasswordResponse }
     * 
     */
    public ResetPasswordResponse createResetPasswordResponse() {
        return new ResetPasswordResponse();
    }

    /**
     * Create an instance of {@link RegisterDeviceResponse }
     * 
     */
    public RegisterDeviceResponse createRegisterDeviceResponse() {
        return new RegisterDeviceResponse();
    }

    /**
     * Create an instance of {@link FacebookLogin }
     * 
     */
    public FacebookLogin createFacebookLogin() {
        return new FacebookLogin();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link LoginFault }
     * 
     */
    public LoginFault createLoginFault() {
        return new LoginFault();
    }

    /**
     * Create an instance of {@link ChangeSettingResponse }
     * 
     */
    public ChangeSettingResponse createChangeSettingResponse() {
        return new ChangeSettingResponse();
    }

    /**
     * Create an instance of {@link VeeyahSetting }
     * 
     */
    public VeeyahSetting createVeeyahSetting() {
        return new VeeyahSetting();
    }

    /**
     * Create an instance of {@link ChangePassword }
     * 
     */
    public ChangePassword createChangePassword() {
        return new ChangePassword();
    }

    /**
     * Create an instance of {@link DisplaySettings }
     * 
     */
    public DisplaySettings createDisplaySettings() {
        return new DisplaySettings();
    }

    /**
     * Create an instance of {@link ChangeSetting }
     * 
     */
    public ChangeSetting createChangeSetting() {
        return new ChangeSetting();
    }

    /**
     * Create an instance of {@link DisplaySettingsResponse }
     * 
     */
    public DisplaySettingsResponse createDisplaySettingsResponse() {
        return new DisplaySettingsResponse();
    }

    /**
     * Create an instance of {@link FacebookLoginFault }
     * 
     */
    public FacebookLoginFault createFacebookLoginFault() {
        return new FacebookLoginFault();
    }

    /**
     * Create an instance of {@link FacebookLoginResponse }
     * 
     */
    public FacebookLoginResponse createFacebookLoginResponse() {
        return new FacebookLoginResponse();
    }

    /**
     * Create an instance of {@link LogoutFault }
     * 
     */
    public LogoutFault createLogoutFault() {
        return new LogoutFault();
    }

    /**
     * Create an instance of {@link ChangePasswordFault }
     * 
     */
    public ChangePasswordFault createChangePasswordFault() {
        return new ChangePasswordFault();
    }

    /**
     * Create an instance of {@link ResetPassword }
     * 
     */
    public ResetPassword createResetPassword() {
        return new ResetPassword();
    }

    /**
     * Create an instance of {@link RegisterDevice }
     * 
     */
    public RegisterDevice createRegisterDevice() {
        return new RegisterDevice();
    }

    /**
     * Create an instance of {@link RegisterType }
     * 
     */
    public RegisterType createRegisterType() {
        return new RegisterType();
    }

    /**
     * Create an instance of {@link ResetPasswordFault }
     * 
     */
    public ResetPasswordFault createResetPasswordFault() {
        return new ResetPasswordFault();
    }

    /**
     * Create an instance of {@link ChangeSettingFault }
     * 
     */
    public ChangeSettingFault createChangeSettingFault() {
        return new ChangeSettingFault();
    }

    /**
     * Create an instance of {@link ChangePasswordResponse }
     * 
     */
    public ChangePasswordResponse createChangePasswordResponse() {
        return new ChangePasswordResponse();
    }

}
