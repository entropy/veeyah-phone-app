
package com.veeyah.auth;


/**
 * <p>Java class for WebFingerprint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WebFingerprint">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClientIP" type="{http://veeyah.com/VeeyahAuth}IPType"/>
 *         &lt;element name="UserAgent" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HttpAcceptsHeader" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
public class WebFingerprint {

    protected String clientIP;
    protected String userAgent;
    protected String httpAcceptsHeader;

    /**
     * Gets the value of the clientIP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientIP() {
        return clientIP;
    }

    /**
     * Sets the value of the clientIP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientIP(String value) {
        this.clientIP = value;
    }

    /**
     * Gets the value of the userAgent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserAgent() {
        return userAgent;
    }

    /**
     * Sets the value of the userAgent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserAgent(String value) {
        this.userAgent = value;
    }

    /**
     * Gets the value of the httpAcceptsHeader property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHttpAcceptsHeader() {
        return httpAcceptsHeader;
    }

    /**
     * Sets the value of the httpAcceptsHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHttpAcceptsHeader(String value) {
        this.httpAcceptsHeader = value;
    }

}
