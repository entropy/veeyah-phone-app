package com.veeyah.measuretest;

import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings.Secure;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;

public final class MatchBoxService_Impl {

    private VeeyahBattery vb;
    private AsyncTask currentTask;

    class GetJoulesTask extends AsyncTask<String, Integer, String> {

        protected void onProgressUpdate(Integer... progress) {
            //setProgressPercent(progress[0]);
        }

        protected void onPostExecute(String result) {
            int dex = result.indexOf("<Amount>");
            if (dex < 0) {
                Log.w("Veeyah", "Amount not found?");
                return;
            }
            String sjStr = result.substring(dex+8);
            dex = sjStr.indexOf("<");
            sjStr = sjStr.substring(0, dex);
            try {
                int sj = Integer.parseInt(sjStr);
                vb.addSunJoules(sj);
            } catch (Exception e) {
                Log.w("Veeyah", "Exception parsing amount: "+e.getClass()+": "+e.getMessage());
            }
        }

        protected String doInBackground(String... i) {
            return(doWebService(i[0].getBytes()));
        }
    }

    class RegisterTask extends AsyncTask<String, Integer, String> {

        protected void onProgressUpdate(Integer... progress) {
            //setProgressPercent(progress[0]);
        }

        protected void onPostExecute(String result) {
            int dex = result.indexOf("<DeviceID>");
            if (dex < 0) {
                Log.w("WARN: No Device ID returned", result);
                return;
            }

            String idStr = result.substring(dex+10);
            dex = idStr.indexOf("<");
            idStr = idStr.substring(0, dex);
            Log.i("Device ID returned:", idStr);
            vb.registrationSuccess(idStr);
        }

        protected String doInBackground(String... i) {
            return(doWebService(i[0].getBytes()));
        }
    }

    protected String doWebService(byte[] postData) {
        try {
            URL url = new URL("http://veeyah.com:8080/MatchBox/");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Length", Integer.toString(postData.length));
            conn.setRequestProperty("Content-Type", "text/xml");
            conn.setRequestProperty("SOAPAction", "\"http://veeyah.com/MatchBox\"");
            conn.setUseCaches(false);
            conn.setDoOutput(true);

            OutputStream out = conn.getOutputStream();
            out.write(postData);
            out.close();
            try {
                InputStream in = new BufferedInputStream(conn.getInputStream());
                StringBuilder total = new StringBuilder();
                byte[] bytes = new byte[1000];
                int numRead;
                while ((numRead = in.read(bytes)) >= 0) {
                    total.append(new String(bytes, 0, numRead));
                }
                return(total.toString());
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            return("Exception contacting server: "+e.getClass()+": "+e.getMessage());
        }
    }

    private String getTime() {
        java.util.Date date= new java.util.Date();
        return((new Timestamp(date.getTime())).toString());
    }

    protected void getSunJoules(int amount, VeeyahBattery vb, String deviceID) {
        if (deviceID == null || "".equals(deviceID)) {
            Log.i("Veeyah Service", "Device ID is null.");
            return;
        }
        if (!checkTaskAvailability()) return;
        Log.i("Veeyah", "Requesting "+amount+" SunJoules.");
        try {
            this.vb = vb;
            String msg = "<?xml version=\"1.0\" ?>\n" +
                    "<S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                    "\t<S:Body>\n" +
                    "\t\t<ns2:GetSunJoules xmlns:ns2=\"http://veeyah.com/MatchBox\">\n" +
                    "\t\t\t<RequestTime>"+getTime()+"</RequestTime>\n" +
                    "\t\t\t<DeviceID>"+deviceID+"</DeviceID>\n" +
                    "\t\t\t<Amount>"+amount+"</Amount>\n" +
                    "\t\t</ns2:GetSunJoules>\n" +
                    "\t</S:Body>\n" +
                    "</S:Envelope>";
            currentTask = new GetJoulesTask().execute(msg);
        } catch (Exception e) {
            Log.i("Veeyah", "Exception getting Sun Joules: "+e.getClass()+": "+e.getMessage());
        }
    }

    /*protected void updateMetrics(MainActivity c) {
        try {
            this.mc = c;
            String msg = "<?xml version=\"1.0\" ?>\n" +
                    "<S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                    "\t<S:Body>\n" +
                    "\t\t<ns2:UpdateMetrics xmlns:ns2=\"http://veeyah.com/MatchBox\">\n" +
                    "\t\t\t<RequestTime>"+getTime()+"</RequestTime>\n" +
                    "\t\t\t<DeviceID>deviceID9012345678901234</DeviceID>\n" +
                    "\t\t\t<Metrics>Metrics</Metrics>\n" +
                    "\t\t</ns2:UpdateMetrics>\n" +
                    "\t</S:Body>\n" +
                    "</S:Envelope>";
            new MatchBoxTask().execute(msg);
        } catch (Exception e) {
            Toast.makeText(c, "Exception updating metrics: "+e.getClass()+": "+e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }*/



    private boolean checkTaskAvailability() {
        if (currentTask == null) return(true);
        if (currentTask.getStatus().equals(AsyncTask.Status.FINISHED)) return(true);
        Log.i("Veeyah Service", "Asynch task is too busy.");
        return(false);
    }

    protected void doRegister(VeeyahBattery vb, String name, String email, String password) {
        this.vb = vb;
        if (!checkTaskAvailability()) return;
        try {
            String guid = Secure.getString(vb.getContentResolver(), Secure.ANDROID_ID);
            String msg = "<?xml version=\"1.0\" ?>\n" +
                    "<S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                    "\t<S:Body>\n" +
                    "\t\t<ns2:RegisterDevice xmlns:ns2=\"http://veeyah.com/MatchBox\">\n" +
                    "\t\t\t<RequestTime>"+getTime()+"</RequestTime>\n" +
                    "\t\t\t<DeviceInfo>\n" +
                    "\t\t\t\t<Email>"+email+"</Email>\n" +
                    "\t\t\t\t<Name>"+name+"</Name>\n" +
                    "\t\t\t\t<Password>"+password+"</Password>\n" +
                    "\t\t\t\t<ClientGUID>"+guid+"</ClientGUID>\n" +
                    "\t\t\t\t<DeviceName>Droid</DeviceName>\n" +
                    "\t\t\t\t<DeviceType>"+ Build.MANUFACTURER+": "+Build.MODEL+"</DeviceType>\n" +
                    "\t\t\t\t<InviteCode>0KAwbgfzG00CNk6XX02qejsf</InviteCode>\n" +
                    "\t\t\t</DeviceInfo>\n" +
                    "\t\t</ns2:RegisterDevice>\n" +
                    "\t</S:Body>\n" +
                    "</S:Envelope>";
            currentTask = new RegisterTask().execute(msg);
        } catch (Exception e) {
            Log.w("Veeyah", "Exception registering device: "+e.getClass()+": "+e.getMessage());
        }
    }
}
