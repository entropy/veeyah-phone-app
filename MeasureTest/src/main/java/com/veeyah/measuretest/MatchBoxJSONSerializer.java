package com.veeyah.measuretest;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

public final class MatchBoxJSONSerializer {

    private static final String fileName = "mbLog";

    public static void saveMbInfo(MatchBoxModel mbm, Context myContext) {
        JSONArray array = new JSONArray();
        try {
            array.put(0, mbm.getStartTime());
            array.put(1, mbm.getLastPoint());
            array.put(2, mbm.getCapacity());
            array.put(3, mbm.getTotCharge());
            array.put(4, mbm.getTotDischarge());
            array.put(5, mbm.getAvailableSJ());
            array.put(6, mbm.getUsedSJ());
            array.put(7, mbm.getIdleTime());
            array.put(8, mbm.getDeviceID());
            array.put(9, mbm.getLastTime());
        } catch (Exception e) {
            Log.w("Veeyah", "Exception building JSONArray: " + e.getClass() + ": " + e.getMessage());
        }
        Writer w = null;
        try {
            OutputStream out = myContext.openFileOutput(fileName, Context.MODE_PRIVATE);
            w = new OutputStreamWriter(out);
            w.write(array.toString());
        } catch (Exception e) {
            Log.w("Veeyah", "Exception writing to file: "+e.getClass()+": "+e.getMessage());
        } finally {
            try {
                if (w != null) w.close();
            } catch (Exception e) {
                Log.w("Veeyah", "Exception closing file: "+e.getClass()+": "+e.getMessage());
            }
        }
    }

    //if this method returns null, this means the device should be registered
    public static MatchBoxModel loadMbInfo(Context myContext) {
        MatchBoxModel mbm = new MatchBoxModel();
        BufferedReader br = null;
        try {
            if (myContext == null) {
                Log.w("Veeyah", "MatchBoxJSONSerializer.loadMbInfo: Null context!");
                return(mbm);
            }
            InputStream in = myContext.openFileInput(fileName);
            br = new BufferedReader(new InputStreamReader(in));
            StringBuilder json = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                json.append(line);
            }
            JSONArray array = (JSONArray) new JSONTokener(json.toString()).nextValue();
            if (array.length() != 10) {
                Log.w("Veeyah", "Invalid JSON array ("+array.length()+")!");
            } else {
                mbm.setStartTime(array.getString(0));
                mbm.setlastPoint(array.getLong(1));
                if (mbm.getCapacity() != array.getInt(2)) {
                 Log.i("Veeyah", "Capacity changed from "+array.getInt(2)+" to "+mbm.getCapacity());
                }
                mbm.setTotCharge(array.getInt(3));
                mbm.setTotDischarge(array.getInt(4));
                mbm.setAvailableSJ(array.getLong(5));
                mbm.setUsedSJ(array.getLong(6));
                mbm.setIdleTime(array.getLong(7));
                mbm.setDeviceID(array.getString(8));
                mbm.setLastTime(array.getString(9));
            }
            if (mbm.getDeviceID() == null || "".equals(mbm.getDeviceID())) return(null);
        } catch (FileNotFoundException fnfe) {
            //caller should register device
            return(null);
        } catch (Exception e) {
            Log.w("Veeyah", "Exception loading file: "+e.getClass()+": "+e.getMessage());
        } finally {
            try {
                if (br != null) br.close();
            } catch (Exception e) {
                Log.w("Veeyah", "Exception closing input file: "+e.getClass()+": "+e.getMessage());
            }
        }
        return(mbm);
    }
}
